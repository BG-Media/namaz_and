package com.bugingroup.namazmuftiyat;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.SectionIndexer;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bugingroup.namazmuftiyat.utils.Requests;
import com.bugingroup.namazmuftiyat.utils.volley.AppController;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindString;
import butterknife.ButterKnife;

import static com.bugingroup.namazmuftiyat.MainActivity.whatPage;

/**
 * Created by Nurik on 30.05.2017.
 */

public class ActivityTaharat extends AppCompatActivity {

    @BindString(R.string.taharat) String taharat;
    @BindString(R.string.namaz_book) String namaz_book;
    @BindString(R.string.azan) String azan;
    @BindString(R.string.sura_dua) String sura_dua;
    @BindString(R.string.sura) String sura;
    @BindString(R.string.dua) String dua;
    public static ArrayList<HashMap<String,String>> taharatList = new ArrayList<>();
    Requests r = new Requests();

    ListView bookListView;
    public static SimpleAdapter bookAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_common_list);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(taharat);
        bookListView = (ListView) findViewById(R.id.list);


        bookAdapter = new SimpleAdapter(this,taharatList,R.layout.common_item,
                new String[]{"title"},new int[]{R.id.common_item_text});
        bookListView.setAdapter(bookAdapter);


        if(whatPage.equals("taharat")){
            taharatList.clear();
            getSupportActionBar().setTitle(taharat);
            r.getSub_categories(this,taharatList,"24",bookAdapter);
        }else if(whatPage.equals("book")){
            taharatList.clear();
            getSupportActionBar().setTitle(namaz_book);
            r.getSub_categories(this,taharatList,"1",bookAdapter);
        }else if(whatPage.equals("azan")){
            taharatList.clear();
            getSupportActionBar().setTitle(azan);
            r.getSub_categories(this,taharatList,"9",bookAdapter);
        }else if(whatPage.equals("sura_dua")){
            taharatList.clear();
            getSupportActionBar().setTitle(sura_dua);
            HashMap<String, String> hashMap = new HashMap<>();
            hashMap.put("id", "1");
            hashMap.put("title", sura);
            taharatList.add(hashMap);
            hashMap = new HashMap<>();
            hashMap.put("id", "2");
            hashMap.put("title",dua);
            taharatList.add(hashMap);
            bookAdapter.notifyDataSetChanged();
        }


      bookListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
            if(whatPage.equals("taharat") && i==2){
                startActivity(new Intent(ActivityTaharat.this,BeineliDaret.class).putExtra("title",taharatList.get(i).get("title")).putExtra("id",taharatList.get(i).get("id")));
            } else
            startActivity(new Intent(ActivityTaharat.this,SubCategory.class).putExtra("title",taharatList.get(i).get("title")).putExtra("id",taharatList.get(i).get("id")));
        }
    });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


}
