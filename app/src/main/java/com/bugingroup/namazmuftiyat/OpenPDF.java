package com.bugingroup.namazmuftiyat;

import android.app.ProgressDialog;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.OpenableColumns;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import com.github.barteksc.pdfviewer.PDFView;
import com.github.barteksc.pdfviewer.scroll.DefaultScrollHandle;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Nurik on 16.05.2017.
 */

public class OpenPDF extends AppCompatActivity {


    @BindView(R.id.pdfview)
    PDFView pdfview;
    ProgressDialog p;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pdf);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("  ");
        p = new ProgressDialog(OpenPDF.this);
        p.setTitle("");
        p.setMessage(getResources().getString(R.string.download));
        p.setCancelable(false);
        p.show();
        Log.d("ASASS","aaaaaa1");
        new RetrievePDFStream().execute(getIntent().getStringExtra("pdf"));
        Log.d("ASASS","aaaaaa2");

    }


    private class RetrievePDFStream extends AsyncTask<String,Void, InputStream>
    {

        @Override
        protected InputStream doInBackground(String... string) {
            String root = Environment.getExternalStorageDirectory().toString();
            int count;
            InputStream inputstream = null;
            try{
                URL url = new URL(string[0]);
                HttpURLConnection urlcon = (HttpURLConnection) url.openConnection();
                Log.d("ASASS",urlcon.getResponseMessage());
                if(urlcon.getResponseCode()==200){
                    inputstream = new BufferedInputStream(urlcon.getInputStream());
                }

                OutputStream output = new FileOutputStream(root+"/NamazTime.pdf");
                byte data[] = new byte[1024];

                long total = 0;
                while ((count = inputstream.read(data)) != -1) {
                    total += count;

                    // writing data to file
                    output.write(data, 0, count);

                }

                // flushing output
                output.flush();

                // closing streams
                output.close();
                inputstream.close();

            }
            catch (IOException e){
                p.dismiss();
                return null;
            }
            return inputstream;

        }

        @Override
        protected void onPostExecute(InputStream inputStream) {
            Log.d("ASASS","aaaaaa");
            String root = Environment.getExternalStorageDirectory().toString();
            displayFromUri(Uri.fromFile(new File(root+"/NamazTime.pdf")));
            Toast.makeText(OpenPDF.this, getString(R.string.download_success), Toast.LENGTH_SHORT).show();
            p.dismiss();

        }
    }

    private void displayFromUri(Uri uri) {

        pdfview.fromUri(uri)
                .enableAnnotationRendering(true)
                .scrollHandle(new DefaultScrollHandle(this))
                .load();
    }

    public String getFileName(Uri uri) {
        String result = null;
        if (uri.getScheme().equals("content")) {
            Cursor cursor = getContentResolver().query(uri, null, null, null, null);
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
        if (result == null) {
            result = uri.getLastPathSegment();
        }
        return result;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


}
