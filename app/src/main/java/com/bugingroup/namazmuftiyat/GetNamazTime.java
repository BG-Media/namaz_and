package com.bugingroup.namazmuftiyat;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;

import com.bugingroup.namazmuftiyat.Fragment.Fragment1;
import com.bugingroup.namazmuftiyat.Fragment.Fragment2;
import com.bugingroup.namazmuftiyat.Fragment.Fragment3;
import com.bugingroup.namazmuftiyat.utils.NonSwipeableViewPager;

import butterknife.BindColor;
import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by nurbaqyt on 15.05.17.
 */

public class GetNamazTime extends AppCompatActivity {

    @BindString(R.string.setting)
    String setting;

    @BindView(R.id.bottom_reset)
    LinearLayout bottom_reset;

    @BindView(R.id.bottom)
    LinearLayout bottom;

    @BindView(R.id.viewpager)
    NonSwipeableViewPager viewPager;
    int i=0;
    @BindView(R.id.view1) View view1;
    @BindView(R.id.view2) View view2;
    @BindView(R.id.view3) View view3;

    @BindColor(R.color.colorAccent) int colorAccent;
    @BindColor(R.color.gray) int gray;

    public static double LAT, LON;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_namaz);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(setting);

        FragmentManager fm = getSupportFragmentManager();
        MyFragmentPagerAdapter pagerAdapter = new MyFragmentPagerAdapter(fm);
        viewPager.setPagingEnabled(false);
        viewPager.setAdapter(pagerAdapter);
        setPager();

    }

    @OnClick(R.id.next)
    void NextClick(){
        if(i!=2) {
            i++;
            setPager();
        }

    }
    @OnClick(R.id.back)
    void BackClick(){
        if(i!=0) {
            i--;
           setPager();
        }
    }

    @OnClick(R.id.reset)
    void ResetClick(){
       finish();
        finish();
        startActivity(getIntent());
    }

    public void setPager(){
        viewPager.reMeasureCurrentPage(i);
        viewPager.setCurrentItem(i);
        switch (i){
            case 0:
                view1.setBackgroundColor(colorAccent);
                view2.setBackgroundColor(gray);
                view3.setBackgroundColor(gray);
                bottom.setVisibility(View.VISIBLE);
                bottom_reset.setVisibility(View.GONE);
                break;
            case 1:
                view1.setBackgroundColor(gray);
                view2.setBackgroundColor(colorAccent);
                view3.setBackgroundColor(gray);
                bottom.setVisibility(View.VISIBLE);
                bottom_reset.setVisibility(View.GONE);
                break;
            case 2:
                view1.setBackgroundColor(gray);
                view2.setBackgroundColor(gray);
                view3.setBackgroundColor(colorAccent);
                bottom.setVisibility(View.GONE);
                bottom_reset.setVisibility(View.VISIBLE);
                break;

        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    public class MyFragmentPagerAdapter extends FragmentPagerAdapter {

        final int PAGE_COUNT = 3;

        /** Constructor of the class */
        public MyFragmentPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        /** This method will be invoked when a page is requested to create */
        @Override
        public Fragment getItem(int arg0) {

            switch(arg0){
                case 0:
                    return new Fragment1();
                case 1:
                    return new Fragment2();
                case 2:
                    return new Fragment3();
                default:
                    return null;
            }
        }

        /** Returns the number of pages */
        @Override
        public int getCount() {
            return PAGE_COUNT;
        }
    }


}
