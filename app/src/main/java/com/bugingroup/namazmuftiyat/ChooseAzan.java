package com.bugingroup.namazmuftiyat;

import android.app.AlertDialog;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.widget.Toast.LENGTH_SHORT;
import static com.bugingroup.namazmuftiyat.MainActivity.addingTimeArr;
import static com.bugingroup.namazmuftiyat.SplashScreen.PREFS_NAME;
import static com.bugingroup.namazmuftiyat.TimeSettings.whatSettings;
import static com.bugingroup.namazmuftiyat.utils.CalculatePrayTime.UValues;

public class ChooseAzan extends AppCompatActivity {

    ListView azanListView;
    ArrayAdapter adapter;
    String[] titlesAzanArr = new String[]{"abdulbasit_abdussamad","ahmed_al_nufays","ali_ahmed_mulla","mishari_rashid","mustafa_ismail","mustafa_ozjan"};

    MediaPlayer mp;

    int selected = 0;

    final int[] resID = {R.raw.abdulbasit_abdussamad, R.raw.ahmed_al_nufays, R.raw.ali_ahmed_mulla, R.raw.mishari_rashid, R.raw.mustafa_ismail, R.raw.mustafa_ozjan};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_azan);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        azanListView = (ListView) findViewById(R.id.choose_azan_list);
        adapter = new ArrayAdapter(ChooseAzan.this,R.layout.item_city,R.id.city,titlesAzanArr);
        azanListView.setAdapter(adapter);
        mp = new MediaPlayer();

            azanListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    azanListView.setItemChecked(i, true);
                    mp.reset();// stops any current playing song
                    mp = MediaPlayer.create(getApplicationContext(), resID[i]);
                    mp.start();
                    selected=i;
                }
            });

    }

    @OnClick(R.id.save)
    void SaveClick(){
        SharedPreferences  settings = getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.putInt("azan", selected);
        editor.commit();
        finish();
    }

    @Override

    public void onDestroy() {

        super.onDestroy();

        mp.release();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


}
