package com.bugingroup.namazmuftiyat;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.bugingroup.namazmuftiyat.Alarm.Alarm;
import com.bugingroup.namazmuftiyat.Alarm.AlarmListAdapter;
import com.bugingroup.namazmuftiyat.Alarm.EditAlarm;

import java.io.IOException;
import java.util.GregorianCalendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.content.ContentValues.TAG;

/**
 * Created by nurbaqyt on 31.07.17.
 */

public class AlarmActivity extends Activity {
    private AlarmListAdapter mAlarmListAdapter;

 int mHour,mMinute;
    @BindView(R.id.list)
    ListView mAlarmList;
    @BindView(R.id.number)
    EditText number;

    MediaPlayer mediaPlayer;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alarm);
        ButterKnife.bind(this);

        mAlarmListAdapter = new AlarmListAdapter(AlarmActivity.this);
        mAlarmList.setAdapter(mAlarmListAdapter);

        Toast.makeText(this, mAlarmListAdapter.getCount()+"..", Toast.LENGTH_SHORT).show();

    }

    public void onResume()
    {
        super.onResume();
        Log.i(TAG, "AlarmMe.onResume()");
        mAlarmListAdapter.updateAlarms();
    }


    @OnClick(R.id.add)
    void AddClick(){

        Toast.makeText(this, "AAAAA", Toast.LENGTH_SHORT).show();
        GregorianCalendar mCalendar;
        mCalendar = new GregorianCalendar(2017, 7, 1, mHour, mMinute);

        Log.d("Aaaaaaaaaa",Long.parseLong(number.getText().toString())+"--"+number.getText().toString());
        Alarm mAlarm = new Alarm(AlarmActivity.this);
        mAlarm.setId(1);
        mAlarm.setType(Integer.parseInt(number.getText().toString()));
        mAlarm.setTitle("Asr");
        mAlarm.setDate(mCalendar.getTimeInMillis());
        mAlarm.setDays(0);
        mAlarm.setEnabled(true);
        mAlarm.setOccurence(0);
        mAlarmListAdapter.add(mAlarm);
        mAlarmListAdapter.notifyDataSetChanged();
        mAlarmListAdapter.updateAlarms();

    }

    @OnClick(R.id.time)
     void onTimeClick()
    {
        showDialog(1);
    }
    @Override
    protected Dialog onCreateDialog(int id)
    {
       return new TimePickerDialog(this, mTimeSetListener, 0, 0, true);
    }


    @Override
    protected void onPrepareDialog(int id, Dialog dialog)
    {
       ((TimePickerDialog)dialog).updateTime(0, 0);
    }

    private TimePickerDialog.OnTimeSetListener mTimeSetListener = new TimePickerDialog.OnTimeSetListener()
    {
        public void onTimeSet(TimePicker view, int hourOfDay, int minute)
        {
            mHour = hourOfDay;
            mMinute = minute;

        }
    };
}
