package com.bugingroup.namazmuftiyat;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.bugingroup.namazmuftiyat.Widget.NamazWidget;
import com.bugingroup.namazmuftiyat.utils.volley.AppController;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.bugingroup.namazmuftiyat.MainActivity.currentCityName;
import static com.bugingroup.namazmuftiyat.MainActivity.lang;
import static com.bugingroup.namazmuftiyat.SplashScreen.PREFS_NAME;

/**
 * Created by nurbaqyt on 26.05.17.
 */

public class CityList extends AppCompatActivity {


    @BindView(R.id.list)
    ListView list;

    ArrayList<HashMap<String,String>> arrayList;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_city_list);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getCity();
    }


    public void getCity(){
        arrayList = new ArrayList<>();
        StringRequest jsonReq = new StringRequest (Request.Method.GET,
                "http://namaz.muftyat.kz/namaz_times/regions.json",new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if(response!=null){
                    try {
                        JSONArray jsonArray = new JSONArray(response);
                        for (int i=0;i<jsonArray.length();i++){
                            JSONObject jsonObject = (JSONObject) jsonArray.get(i);
                            if(jsonObject.getInt("parent_id")!=0) {
                                HashMap<String, String> hashMap = new HashMap<>();
                                hashMap.put("id", jsonObject.getString("id"));
                                hashMap .put("coords", jsonObject.getString("coords"));
                                hashMap.put("title",lang.equals("ru") ?jsonObject.getString("title_ru") :jsonObject.getString("title_kk"));
                                arrayList.add(hashMap);
                            }
                        }
                        ListAdapter adapter = new ListAdapter(CityList.this,arrayList);
                        list.setAdapter(adapter);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {


                Toast.makeText(CityList.this,getResources().getString(R.string.volley_error),Toast.LENGTH_SHORT).show();
                VolleyLog.d("Error: " + error.getMessage());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json");
                return headers;
            }

        };
        AppController.getInstance().addToRequestQueue(jsonReq);
    }



        public class ListAdapter extends BaseAdapter {

            ArrayList<HashMap<String, String>> arrayList;
            Activity activity;
            HashMap<String, String> m;
            public ListAdapter(Activity activity, ArrayList<HashMap<String, String>> arrayList) {
                this.activity = activity;
                this.arrayList = arrayList;
            }


            @Override
            public int getCount() {
                return arrayList.size();
            }

            @Override
            public Object getItem(int location) {
                return null;
            }

            @Override
            public long getItemId(int position) {
                return position;
            }


            class ViewHolder {
                TextView city;

            }

            @Override
            public View getView(final int position, View convertView, ViewGroup parent) {
                final ViewHolder viewHolder;
                if (convertView == null) {
                    LayoutInflater inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    convertView = inflater.inflate(R.layout.item_city, parent, false);
                    viewHolder = new ViewHolder();

                    viewHolder.city = (TextView) convertView.findViewById(R.id.city);

                    convertView.setTag(viewHolder);

                } else {
                    viewHolder = (ViewHolder) convertView.getTag();
                }
                m = arrayList.get(position);
                viewHolder.city.setText(m.get("title"));

                convertView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        m = arrayList.get(position);
                        SharedPreferences  settings = getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);

                        SharedPreferences.Editor editor = settings.edit();
                        editor.putString("city_id", m.get("id"));
                        editor.putString("city", m.get("title"));
                        editor.putString("coords", m.get("coords"));
                        editor.commit();

                        String[] separated = m.get("coords").split(",");
                        Log.d("LLLLL",separated[1]+"***"+separated[0]);
                        MainActivity.longitude =  Double.valueOf(separated[1].trim()).doubleValue();
                        MainActivity.latitude = Double.valueOf(separated[0].trim()).doubleValue();

                        currentCityName = m.get("title");

                        Intent intent = new Intent();
                        intent.putExtra("name", m.get("title"));
                        setResult(RESULT_OK, intent);
                        finish();
                    }
                });
                return convertView;
            }
        }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


}
