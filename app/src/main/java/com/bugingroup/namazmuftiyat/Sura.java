package com.bugingroup.namazmuftiyat;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.TextView;

/**
 * Created by nurbaqyt on 04.08.17.
 */

public class Sura extends AppCompatActivity {
    SuraAdapter adapter;
    ListView list;
    int pos;
    SharedPreferences settings;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.sura);
        this.pos = getIntent().getIntExtra("pos", 0);
        setTitle(getResources().getStringArray(R.array.kazakh)[this.pos]);
        int len = getResources().getStringArray(getResources().getIdentifier("kazakh_" + (this.pos + 1), "array", getPackageName())).length;
        this.list = (ListView) findViewById(R.id.list);
        ListView listView = this.list;
        boolean z = len > 25 || len <= 25;
        listView.setFastScrollEnabled(z);
        this.adapter = new SuraAdapter(this, this.pos);
        this.list.setAdapter(this.adapter);
    }

    public void onResume() {
        int index = this.list.getFirstVisiblePosition();
        this.list.setAdapter(new SuraAdapter(this, this.pos));
        this.list.setSelection(index);
        super.onResume();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.filter) {
            startActivity(new Intent(this, Langs.class));
        } else if(item.getItemId() == android.R.id.home) {
            super.onBackPressed();
        }
        return true;
    }
}
