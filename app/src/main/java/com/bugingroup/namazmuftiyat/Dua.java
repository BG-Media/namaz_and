package com.bugingroup.namazmuftiyat;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.bugingroup.namazmuftiyat.utils.volley.AppController;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by nurbaqyt on 15.08.17.
 */

public class Dua  extends AppCompatActivity {
    ArrayList<HashMap<String,String>> duaList = new ArrayList<>();

    ListAdapter adapter;

    @BindView(R.id.list)
    ListView list;
    String pos;
    SharedPreferences settings;
    Typeface scheherazade;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sura);
        ButterKnife.bind(this);
        scheherazade = Typeface.createFromAsset(getAssets(), "trado.ttf");
        pos = getIntent().getStringExtra("position");
        setTitle(getIntent().getStringExtra("name"));

        Log.d("ASAS",pos+";");
        adapter = new ListAdapter(Dua.this,duaList);
        list.setAdapter(adapter);
        getSingleDua(pos);

    }

    private void getSingleDua(String pos) {
        StringRequest jsonReq = new StringRequest (Request.Method.GET,
                "http://195.210.47.17/hidayat/api/v1/duga/id/"+pos,new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                if(response!=null){
                    Log.d("ddddd",response);

                    try {
                        JSONArray jsonArray = new JSONArray(response);
                        for (int i=0;i<jsonArray.length();i++) {
                            JSONObject jsonObject = (JSONObject) jsonArray.get(i);
                            HashMap<String, String> hashMap = new HashMap<>();
                            hashMap.put("id", jsonObject.getString("id"));
                            hashMap.put("title", jsonObject.getString("name")+"\n\n"+jsonObject.getString("body"));
                           duaList.add(hashMap);
                        }
                        adapter.notifyDataSetChanged();

                    } catch ( JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                Toast.makeText(Dua.this,Dua.this.getResources().getString(R.string.volley_error),Toast.LENGTH_SHORT).show();

                VolleyLog.d("Error: " + error.getMessage());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json");
                return headers;
            }
        };
        AppController.getInstance().addToRequestQueue(jsonReq);
    }


    public class ListAdapter extends BaseAdapter {

        ArrayList<HashMap<String, String>> arrayList;
        Activity activity;
        HashMap<String, String> m;
        public ListAdapter(Activity activity, ArrayList<HashMap<String, String>> arrayList) {
            this.activity = activity;
            this.arrayList = arrayList;
        }


        @Override
        public int getCount() {
            return arrayList.size();
        }

        @Override
        public Object getItem(int location) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }


        class ViewHolder {
            TextView city;

        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            final ViewHolder viewHolder;
            if (convertView == null) {
                LayoutInflater inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.item_city, parent, false);
                viewHolder = new ViewHolder();

                viewHolder.city = (TextView) convertView.findViewById(R.id.city);

                convertView.setTag(viewHolder);

            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }
            m = arrayList.get(position);
            viewHolder.city.setTypeface(scheherazade);
            viewHolder.city.setText(m.get("title"));

            return convertView;
        }
    }



}