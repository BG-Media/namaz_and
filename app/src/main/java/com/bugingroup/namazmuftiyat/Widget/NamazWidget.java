package com.bugingroup.namazmuftiyat.Widget;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.opengl.Visibility;
import android.util.Log;
import android.widget.RemoteViews;
import android.widget.Toast;

import com.bugingroup.namazmuftiyat.MainActivity;
import com.bugingroup.namazmuftiyat.NamazTime;
import com.bugingroup.namazmuftiyat.R;
import com.bugingroup.namazmuftiyat.utils.CalculatePrayTime;

import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Random;

import butterknife.BindArray;

import static android.view.View.GONE;
import static android.view.View.INVISIBLE;
import static android.view.View.VISIBLE;
import static com.bugingroup.namazmuftiyat.MainActivity.ASRM;
import static com.bugingroup.namazmuftiyat.MainActivity.HIGHET;
import static com.bugingroup.namazmuftiyat.MainActivity.M1;
import static com.bugingroup.namazmuftiyat.MainActivity.M2;
import static com.bugingroup.namazmuftiyat.MainActivity.M3;
import static com.bugingroup.namazmuftiyat.MainActivity.M4;
import static com.bugingroup.namazmuftiyat.MainActivity.M5;
import static com.bugingroup.namazmuftiyat.MainActivity.M6;
import static com.bugingroup.namazmuftiyat.MainActivity.METHOD;
import static com.bugingroup.namazmuftiyat.MainActivity.addingTimeArr;
import static com.bugingroup.namazmuftiyat.MainActivity.currentCityName;
import static com.bugingroup.namazmuftiyat.MainActivity.latitude;
import static com.bugingroup.namazmuftiyat.MainActivity.longitude;
import static com.bugingroup.namazmuftiyat.MainActivity.timezone;

/**
 * Created by nurbaqyt on 19.07.17.
 */

public class NamazWidget extends AppWidgetProvider {

    SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
    int YEAR,MONTH,DAY,dd;

    String [] namaz_name;

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager,
                         int[] appWidgetIds) {
        super.onUpdate(context, appWidgetManager, appWidgetIds);
        Calendar cal = Calendar.getInstance();
        Date now = new Date();
        cal.setTime(now);

        YEAR = cal.get(Calendar.YEAR);
        MONTH = cal.get(Calendar.MONTH)+1;
        DAY = cal.get(Calendar.DATE);
        dd = cal.get(Calendar.DATE);
        Log.d("MONTH",YEAR+"*"+MONTH+"*"+DAY);

        for (int i : appWidgetIds) {

        RemoteViews rv = new RemoteViews(context.getPackageName(),
                R.layout.namaz_widget);


        getTime(context,rv,YEAR,MONTH,DAY);

            Intent configIntent = new Intent(context, MainActivity.class);

        PendingIntent configPendingIntent = PendingIntent.getActivity(context, 0, configIntent, 0);

        rv.setOnClickPendingIntent(R.id.setting, configPendingIntent);
        appWidgetManager.updateAppWidget(i, rv);
    }
    }




    public void getTime(Context context,RemoteViews rv,int y, int m ,int d) {
        namaz_name = context.getResources().getStringArray(R.array.namaz_name);

        try {
            SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
            Date date = sdf.parse(d+"."+m+"."+y);
             sdf=new SimpleDateFormat("EEEE dd MMM yyyy");

            String s=sdf.format(date.getTime());

            rv.setTextViewText(R.id.date_time,s);

        } catch (ParseException e) {
        e.printStackTrace();
    }

        rv.setTextViewText(R.id.city,currentCityName);
        rv.setTextViewText(R.id.next_namaz,context.getResources().getString(R.string.next_namaz));


         int k=100;

        // Retrive lat, lng using location API

        CalculatePrayTime prayers = new CalculatePrayTime();
        prayers.setTimeFormat(prayers.Time24);
        prayers.setCalcMethod(METHOD);
        prayers.setAsrJuristic(ASRM);
        prayers.setAdjustHighLats(HIGHET);
        ArrayList<String> timeNames = new ArrayList<String>();
        timeNames.add(namaz_name[0]);
        timeNames.add(namaz_name[1]);
        timeNames.add(namaz_name[2]);
        timeNames.add(namaz_name[3]);
        timeNames.add(namaz_name[4]);
        timeNames.add(namaz_name[4]);
        timeNames.add(namaz_name[5]);
        prayers.setTimeNames(timeNames);
        int[] offsets  = new int[]{M1, M2, M3, M4,0, M5, M6};

        if(METHOD==8){
            if (latitude < 48) {
                offsets  = new int[]{M1, M2-3, M3+3, M4+3,0, M5+3, M6};
            } else {
                offsets  = new int[]{M1, M2-5, M3+5, M4+5,0, M5+5, M6};
            }
        }
//        if(latitude<48){
//            offsets = new int[]{0, -3, 3, 3, 0, 3, 0};
//        }else {
//            offsets = new int[]{0, -5, 5, 5, 0, 5, 0};
//        }

        prayers.tune(offsets);

        SimpleDateFormat df = new SimpleDateFormat("HH:mm");
        Calendar calculateCalendar = Calendar.getInstance();

        Calendar cal = Calendar.getInstance();
        Date now = new Date();
        cal.setTime(now);


        Log.d("HHH", y+"**"+m+"**"+d+"**"+latitude+"**"+longitude+"**"+timezone);
        ArrayList prayerTimes = prayers.getDatePrayerTimes(y,m,d, latitude, longitude, timezone);
        ArrayList prayerNames = prayers.getTimeNames();

        rv.setTextViewText(R.id.txt1,namaz_name[0]);
        rv.setTextViewText(R.id.txt2,namaz_name[1]);
        rv.setTextViewText(R.id.txt3,namaz_name[2]);
        rv.setTextViewText(R.id.txt4,namaz_name[3]);
        rv.setTextViewText(R.id.txt5,namaz_name[4]);
        rv.setTextViewText(R.id.txt6,namaz_name[5]);

        int j=0;
        for (int i = 0; i < prayerTimes.size(); i++) {
            if (i != 4) {
                Log.d("SSSS", prayerTimes.size() + "***" + i + "**" + prayerTimes.get(i));
                try {
                    Date dateForCalculation = df.parse(prayerTimes.get(i) + "");
                    calculateCalendar.setTime(dateForCalculation);

//                if((cal.get(Calendar.DAY_OF_MONTH) >= 22 && cal.get(Calendar.MONTH) == 5 && latitude >=48) ||
//                        (cal.get(Calendar.MONTH) ==6 && latitude >=48) ||
//                        (cal.get(Calendar.DAY_OF_MONTH) <= 22 && cal.get(Calendar.MONTH) == 7 && latitude >=48)){
//                    addingTimeArr[i] = addingTimeArr[i]+2;
//                }

                    calculateCalendar.add(Calendar.MINUTE, addingTimeArr[i]);
                    String newTime = df.format(calculateCalendar.getTime());
                    switch (i){
                        case 0:   rv.setTextViewText(R.id.fajir,newTime); break;
                        case 1:   rv.setTextViewText(R.id.voshod,newTime); break;
                        case 2:   rv.setTextViewText(R.id.zuhr,newTime); break;
                        case 3:   rv.setTextViewText(R.id.asr,newTime); break;
                        case 5:   rv.setTextViewText(R.id.magrib,newTime); break;
                        case 6:   rv.setTextViewText(R.id.isha,newTime); break;
                    }


                    int hours = new Time(System.currentTimeMillis()).getHours();
                    int minute = new Time(System.currentTimeMillis()).getMinutes();
                    String[] separated = newTime.split(":");
                    int h = Integer.valueOf(separated[0]);
                    int M = Integer.valueOf(separated[1]);

                    Log.d("HH", hours + "_" + h + "M:" + minute + "_" + M);
                    if (k == 100 && dd == d) {
                        if (h >= hours && M > minute) {
                            k = j;
                            rv.setTextViewText(R.id.name,prayerNames.get(i)+"");
                        } else if (h > hours && h != 0) {
                            rv.setTextViewText(R.id.name,prayerNames.get(i)+"");
                            k = j;
                        }

                    }

                } catch (ParseException e) {
                    e.printStackTrace();
                }
                j++;
            }
        }
        if(k != 100)
        switch (k){
            case 0:
                rv.setViewVisibility(R.id.fajir_view, VISIBLE);
                rv.setViewVisibility(R.id.voshod_view, INVISIBLE);
                rv.setViewVisibility(R.id.zuhr_view, INVISIBLE);
                rv.setViewVisibility(R.id.asr_view, INVISIBLE);
                rv.setViewVisibility(R.id.magrib_view, INVISIBLE);
                rv.setViewVisibility(R.id.isha_view, INVISIBLE);
                break;
            case 1:
                rv.setViewVisibility(R.id.fajir_view, INVISIBLE);
                rv.setViewVisibility(R.id.voshod_view, VISIBLE);
                rv.setViewVisibility(R.id.zuhr_view, INVISIBLE);
                rv.setViewVisibility(R.id.asr_view, INVISIBLE);
                rv.setViewVisibility(R.id.magrib_view, INVISIBLE);
                rv.setViewVisibility(R.id.isha_view, INVISIBLE);
                break;
            case 2:
                rv.setViewVisibility(R.id.fajir_view, INVISIBLE);
                rv.setViewVisibility(R.id.voshod_view, INVISIBLE);
                rv.setViewVisibility(R.id.zuhr_view, VISIBLE);
                rv.setViewVisibility(R.id.asr_view, INVISIBLE);
                rv.setViewVisibility(R.id.magrib_view, INVISIBLE);
                rv.setViewVisibility(R.id.isha_view, INVISIBLE);
                break;
            case 3:
                rv.setViewVisibility(R.id.fajir_view, INVISIBLE);
                rv.setViewVisibility(R.id.voshod_view, INVISIBLE);
                rv.setViewVisibility(R.id.zuhr_view, INVISIBLE);
                rv.setViewVisibility(R.id.asr_view, VISIBLE);
                rv.setViewVisibility(R.id.magrib_view, INVISIBLE);
                rv.setViewVisibility(R.id.isha_view, INVISIBLE);
                break;
            case 4:
                rv.setViewVisibility(R.id.fajir_view, INVISIBLE);
                rv.setViewVisibility(R.id.voshod_view, INVISIBLE);
                rv.setViewVisibility(R.id.zuhr_view, INVISIBLE);
                rv.setViewVisibility(R.id.asr_view, INVISIBLE);
                rv.setViewVisibility(R.id.magrib_view, VISIBLE);
                rv.setViewVisibility(R.id.isha_view, INVISIBLE);
                break;
            case 5:
                rv.setViewVisibility(R.id.fajir_view, INVISIBLE);
                rv.setViewVisibility(R.id.voshod_view, INVISIBLE);
                rv.setViewVisibility(R.id.zuhr_view, INVISIBLE);
                rv.setViewVisibility(R.id.asr_view, INVISIBLE);
                rv.setViewVisibility(R.id.magrib_view, INVISIBLE);
                rv.setViewVisibility(R.id.isha_view, VISIBLE);
                break;
            default:
                Toast.makeText(context, "aaaa", Toast.LENGTH_SHORT).show();
                break;
        }
    }


        @Override
    public void onReceive(Context context, Intent intent) {

            Calendar cal = Calendar.getInstance();
            Date now = new Date();
            cal.setTime(now);

            YEAR = cal.get(Calendar.YEAR);
            MONTH = cal.get(Calendar.MONTH)+1;
            DAY = cal.get(Calendar.DATE);
            dd = cal.get(Calendar.DATE);
            Log.d("MONTH",YEAR+"*"+MONTH+"*"+DAY);


                RemoteViews rv = new RemoteViews(context.getPackageName(),
                        R.layout.namaz_widget);


                getTime(context,rv,YEAR,MONTH,DAY);

                rv.setTextViewText(R.id.city,currentCityName);
                Intent configIntent = new Intent(context, MainActivity.class);

                PendingIntent configPendingIntent = PendingIntent.getActivity(context, 0, configIntent, 0);

                rv.setOnClickPendingIntent(R.id.setting, configPendingIntent);


            AppWidgetManager.getInstance(context).updateAppWidget(
                    new ComponentName(context, NamazWidget.class), rv);

        }


}