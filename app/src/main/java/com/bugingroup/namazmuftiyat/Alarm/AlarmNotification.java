/**************************************************************************
 *
 * Copyright (C) 2012-2015 Alex Taradov <alex@taradov.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *************************************************************************/

package com.bugingroup.namazmuftiyat.Alarm;

import android.app.Activity;
import android.app.Dialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bugingroup.namazmuftiyat.MainActivity;
import com.bugingroup.namazmuftiyat.R;
import com.bugingroup.namazmuftiyat.Widget.NamazWidget;
import com.bugingroup.namazmuftiyat.utils.DatabaseHelper;
import com.bugingroup.namazmuftiyat.utils.Debt;

import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import static com.bugingroup.namazmuftiyat.SplashScreen.PREFS_NAME;

public class AlarmNotification extends Activity
{
  private final String TAG = "AlarmMe";

  private Vibrator mVibrator;
  private final long[] mVibratePattern = { 0, 500, 500 };
  private boolean mVibrate;
  private long mPlayTime;
  private Timer mTimer = null;
  private Alarm mAlarm;
  private DateTime mDateTime;
  private TextView mTextView,debt_title_text;
  private PlayTimerTask mTimerTask;
  MediaPlayer mp;

  int selected = 0;

  final int[] resID = {R.raw.abdulbasit_abdussamad, R.raw.ahmed_al_nufays, R.raw.ali_ahmed_mulla, R.raw.mishari_rashid, R.raw.mustafa_ismail, R.raw.mustafa_ozjan};

  int [] alarm_of_onn;
  Uri defaultRingtoneUri;


  LinearLayout namaz, debt_l;

  DatabaseHelper db;
    Debt new_debt;

  String fajir,zuhr,asr,magrib,isha;
  int CC,DD;

  AlarmListAdapter mAlarmListAdapter;


  @Override
  protected void onCreate(Bundle bundle)
  {
    super.onCreate(bundle);
    Log.d("TTTTT", "AlarmNotification");
    SharedPreferences settings = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
    Locale locale = new Locale(settings.getString("language","kk"));
    Locale.setDefault(locale);
    Configuration config = new Configuration();
    config.locale = locale;
    getBaseContext().getResources().updateConfiguration(config,
            getBaseContext().getResources().getDisplayMetrics());

    getWindow().addFlags(
      WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON |
      WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED |
      WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);

    setContentView(R.layout.notification);
    Log.d("AAAAAAA,",settings.getInt("fajir_select", 3)+"");
    mAlarmListAdapter =  new AlarmListAdapter(AlarmNotification.this);

    alarm_of_onn = new int[]{settings.getInt("fajir_select", 3), settings.getInt("voshod_select", 3),settings.getInt("zuhr_select",3),settings.getInt("asr_select",3),
            3,settings.getInt("magrib_select",3), settings.getInt("isha_select",3)};
    defaultRingtoneUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
    db = new DatabaseHelper(getApplicationContext());

    mDateTime = new DateTime(this);
    namaz = (LinearLayout)findViewById(R.id.namaz);
    debt_l = (LinearLayout)findViewById(R.id.debt);

    mTextView = (TextView)findViewById(R.id.alarm_title_text);
    debt_title_text = (TextView)findViewById(R.id.debt_title_text);

    fajir = getResources().getString(R.string.notification_debt) + " " + getResources().getString(R.string.fajir);
    zuhr = getResources().getString(R.string.notification_debt) + " " + getResources().getString(R.string.zuhr);
    asr = getResources().getString(R.string.notification_debt) + " " + getResources().getString(R.string.asr);
    magrib = getResources().getString(R.string.notification_debt) + " " + getResources().getString(R.string.magrib);
    isha = getResources().getString(R.string.notification_debt) + " " + getResources().getString(R.string.isha);



    readPreferences();
    mp = new MediaPlayer();
    if (mVibrate)
      mVibrator = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
    try {
      Intent updateWidget = new Intent(AlarmNotification.this, NamazWidget.class); // Widget.class is your widget class
      updateWidget.setAction("update_widget");
      PendingIntent pending = PendingIntent.getBroadcast(AlarmNotification.this, 0, updateWidget, PendingIntent.FLAG_CANCEL_CURRENT);
      pending.send();
    } catch (PendingIntent.CanceledException e) {
      e.printStackTrace();
    }
    start(getIntent());
  }

  @Override
  protected void onDestroy()
  {
    super.onDestroy();
    Log.i(TAG, "AlarmNotification.onDestroy()");

    stop();
  }

  @Override
  protected void onNewIntent(Intent intent)
  {
    super.onNewIntent(intent);
    Log.i(TAG, "AlarmNotification.onNewIntent()");
    mAlarm = new Alarm(this);
    mAlarm.fromIntent(intent);

    addNotification(mAlarm);

    stop();
    start(intent);
  }

  private void start(Intent intent)
  {
    mAlarm = new Alarm(this);
    mAlarm.fromIntent(intent);

    if(mAlarm.is_debt()){
      namaz.setVisibility(View.GONE);
      debt_l.setVisibility(View.VISIBLE);
      debt_title_text.setText(mAlarm.getTitle());

      List<Debt> allToDos = db.getAlldebt();
      Debt debt = allToDos.get(0);

      Log.d("ASAS",mAlarm.getTitle()+"//"+zuhr);
      if(mAlarm.getTitle().equals(fajir)){
        new_debt = new Debt(debt.getFajr_count(),debt.getFajr_result()-1,debt.getZuhr_count(),debt.getZuhr_result(),debt.getAsr_count(),
                debt.getAsr_result(), debt.getMagrib_count(),debt.getMagrib_result(),debt.getIsha_count(),debt.getIsha_result());
        CC = debt.getFajr_count();
        DD = debt.getFajr_result();
      }else if(mAlarm.getTitle().equals(zuhr)){
        new_debt = new Debt(debt.getFajr_count(),debt.getFajr_result(),debt.getZuhr_count(),debt.getZuhr_result()-1,debt.getAsr_count(),
                debt.getAsr_result() ,debt.getMagrib_count(),debt.getMagrib_result(),debt.getIsha_count(),debt.getIsha_result());
        CC = debt.getZuhr_count();
        DD = debt.getZuhr_result();
      }else if(mAlarm.getTitle().equals(asr)) {
        new_debt = new Debt(debt.getFajr_count(),debt.getFajr_result(),debt.getZuhr_count(),debt.getZuhr_result(),debt.getAsr_count(),
                debt.getAsr_result()-1,debt.getMagrib_count(),debt.getMagrib_result(),debt.getIsha_count(),debt.getIsha_result());
        CC = debt.getAsr_count();
        DD = debt.getAsr_result();
      }else if(mAlarm.getTitle().equals(magrib)) {
        new_debt = new Debt(debt.getFajr_count(),debt.getFajr_result(),debt.getZuhr_count(),debt.getZuhr_result(),debt.getAsr_count(),
                debt.getAsr_result(),debt.getMagrib_count(),debt.getMagrib_result()-1,debt.getIsha_count(),debt.getIsha_result());
        CC = debt.getMagrib_count();
        DD = debt.getMagrib_result();
      }else if(mAlarm.getTitle().equals(isha)) {
        new_debt = new Debt(debt.getFajr_count(),debt.getFajr_result(),debt.getZuhr_count(),debt.getZuhr_result(),debt.getAsr_count(),
                debt.getAsr_result(),debt.getMagrib_count(),debt.getMagrib_result(),debt.getIsha_count(),debt.getIsha_result()-1);
        CC = debt.getIsha_count();
        DD = debt.getIsha_result();
      }
    }else {
      mTextView.setText(mAlarm.getTitle());
      namaz.setVisibility(View.VISIBLE);
      debt_l.setVisibility(View.GONE);
    }
    Log.i(TAG, "AlarmNotification.start('" + mAlarm.getTitle() + "')");






    Log.d(TAG,"AAAAAA"+mAlarm.getType());
    Log.d(TAG,"AAAAAA2"+alarm_of_onn[mAlarm.getType()]);

    if(alarm_of_onn[mAlarm.getType()]==2){
      try {
        mp.setDataSource(AlarmNotification.this, defaultRingtoneUri);
        mp.setAudioStreamType(AudioManager.STREAM_NOTIFICATION);
        mp.prepare();

      } catch (IllegalArgumentException   | IOException e) {
        e.printStackTrace();
      }
    }else if(alarm_of_onn[mAlarm.getType()]==1){
      mp.reset();// stops any current playing song
      mp = MediaPlayer.create(getApplicationContext(), resID[selected]);

    }



    mTimerTask = new PlayTimerTask();
    mTimer = new Timer();
    mTimer.schedule(mTimerTask, mPlayTime);
    mp.start();
    if (mVibrate)
      mVibrator.vibrate(mVibratePattern, 0);

  }

  private void stop()
  {
    Log.i(TAG, "AlarmNotification.stop()");

    mTimer.cancel();
    mp.stop();
    if (mVibrate)
      mVibrator.cancel();
  }

  public void onDismissClick(View view)
  {
    finish();
  }
  public void onYesClick(View view)
  {
    final Dialog dialoga = new Dialog(AlarmNotification.this);
    dialoga.requestWindowFeature(Window.FEATURE_NO_TITLE);
    dialoga.setContentView(R.layout.dialog_alarm_notifi);
    dialoga.setCancelable(false);
    dialoga.show();
    final TextView text = (TextView) dialoga.findViewById(R.id.text) ;
    Button ok_btn = (Button) dialoga.findViewById(R.id.ok_btn) ;
    text.setText(getResources().getString(R.string.notifi_yes_btn, CC+"",DD-1+""));
    ok_btn.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        dialoga.dismiss();
        mAlarmListAdapter.cancelAlarm(mAlarm);
        db.updateDebt(new_debt);
        finish();
      }
    });


  }


  public void onNoClick(View view)
  {
    final Dialog dialoga = new Dialog(AlarmNotification.this);
    dialoga.requestWindowFeature(Window.FEATURE_NO_TITLE);
    dialoga.setContentView(R.layout.dialog_alarm_notifi);
    dialoga.setCancelable(false);
    dialoga.show();
    final TextView text = (TextView) dialoga.findViewById(R.id.text) ;
    Button ok_btn = (Button) dialoga.findViewById(R.id.ok_btn) ;
    text.setText(getResources().getString(R.string.notifi_no_btn, CC+"",DD+""));
    ok_btn.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        dialoga.dismiss();
        mAlarmListAdapter.cancelAlarm(mAlarm);
        finish();
      }
    });



  }

  public void onrepeatClick(View view)
  {
    finish();
  }

  private void readPreferences()
  {
    SharedPreferences  settings = getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
    selected =  settings.getInt("azan",0);
    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
    mVibrate = prefs.getBoolean("vibrate_pref", true);
    mPlayTime = (long) Integer.parseInt(prefs.getString("alarm_play_time_pref", "30")) * 1000;
  }

  private void addNotification(Alarm alarm)
  {
    NotificationManager notificationManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
    Notification notification;
    PendingIntent activity;
    Intent intent;

    Log.i(TAG, "AlarmNotification.addNotification(" + alarm.getId() + ", '" + alarm.getTitle() + "', '" + mDateTime.formatDetails(alarm) + "')");

    intent = new Intent(this.getApplicationContext(), MainActivity.class);
    intent.setAction(Intent.ACTION_MAIN);
    intent.addCategory(Intent.CATEGORY_LAUNCHER);

    activity = PendingIntent.getActivity(this, (int)alarm.getId(), intent, PendingIntent.FLAG_UPDATE_CURRENT);

    NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
    notification = builder
        .setContentIntent(activity)
        .setSmallIcon(R.mipmap.ic_launcher)
        .setAutoCancel(true)
        .setContentTitle("Намаз")
        .setContentText(alarm.getTitle())
        .build();

    notificationManager.notify((int)alarm.getId(), notification);
  }

  @Override
  public void onBackPressed()
  {

    mp.stop();
    finish();
  }

  private class PlayTimerTask extends TimerTask
  {
    @Override
    public void run()
    {
      Log.i(TAG, "AlarmNotification.PalyTimerTask.run()");
      addNotification(mAlarm);
      finish();
    }
  }
}

