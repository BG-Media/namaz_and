
package com.bugingroup.namazmuftiyat.Alarm;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TimePicker;
import android.widget.Toast;

import com.bugingroup.namazmuftiyat.R;

import java.util.Calendar;
import java.util.GregorianCalendar;

import butterknife.BindArray;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
public class EditAlarm extends Activity
{
  private EditText mTitle;
  private Button mDateButton;
  private Button mTimeButton;

  private Alarm mAlarm;
  private DateTime mDateTime;

  private GregorianCalendar mCalendar;
  private int mYear;
  private int mMonth;
  private int mDay;
  private int mHour;
  private int mMinute;

  static final int DATE_DIALOG_ID = 0;
  static final int TIME_DIALOG_ID = 1;
  static final int DAYS_DIALOG_ID = 2;


  @BindView(R.id.folders)
  Spinner folders;

  @Override
  public void onCreate(Bundle bundle)
  {
    super.onCreate(bundle);
    setContentView(R.layout.edit);
    ButterKnife.bind(this);

    mTitle = (EditText)findViewById(R.id.title);
    mDateButton = (Button)findViewById(R.id.date_button);
    mTimeButton = (Button)findViewById(R.id.time_button);

    mAlarm = new Alarm(this);
    mAlarm.fromIntent(getIntent());

    mDateTime = new DateTime(this);

    mTitle.setText(mAlarm.getTitle());
    mTitle.addTextChangedListener(mTitleChangedListener);

    mCalendar = new GregorianCalendar();
    mCalendar.setTimeInMillis(mAlarm.getDate());
    mYear = mCalendar.get(Calendar.YEAR);
    mMonth = mCalendar.get(Calendar.MONTH);
    mDay = mCalendar.get(Calendar.DAY_OF_MONTH);
    mHour = mCalendar.get(Calendar.HOUR_OF_DAY);
    mMinute = mCalendar.get(Calendar.MINUTE);

    mAlarm.setOccurence(0);
    mAlarm.setEnabled(true);
    updateButtons();

  }

  @Override
  protected Dialog onCreateDialog(int id)
  {
    if (DATE_DIALOG_ID == id)
      return new DatePickerDialog(this, mDateSetListener, mYear, mMonth, mDay);
    else if (TIME_DIALOG_ID == id)
      return new TimePickerDialog(this, mTimeSetListener, mHour, mMinute, mDateTime.is24hClock());
    else if (DAYS_DIALOG_ID == id)
      return DaysPickerDialog();
    else
      return null;
  }

  @Override
  protected void onPrepareDialog(int id, Dialog dialog)
  {
    if (DATE_DIALOG_ID == id)
      ((DatePickerDialog)dialog).updateDate(mYear, mMonth, mDay);
    else if (TIME_DIALOG_ID == id)
      ((TimePickerDialog)dialog).updateTime(mHour, mMinute);
  }    

  public void onDateClick(View view)
  {
    if (Alarm.ONCE == mAlarm.getOccurence())
      showDialog(DATE_DIALOG_ID);
    else if (Alarm.WEEKLY == mAlarm.getOccurence())
      showDialog(DAYS_DIALOG_ID);
  }

  public void onTimeClick(View view)
  {
    showDialog(TIME_DIALOG_ID);
  }

  @OnClick(R.id.ok)
  void onDoneClick()
  {
    if(mTitle.getText().toString().length()>0 ) {
        Intent intent = new Intent();
        mAlarm.toIntent(intent);
        setResult(RESULT_OK, intent);
        finish();

    }else {
      Toast.makeText(this, "What you want?", Toast.LENGTH_SHORT).show();
    }

  }

  @OnClick(R.id.close)
 void onCancelClick()
  {
    setResult(RESULT_CANCELED, null);  
    finish();
  }

  private DatePickerDialog.OnDateSetListener mDateSetListener = new DatePickerDialog.OnDateSetListener()
  {
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth)
    {
      mYear = year;
      mMonth = monthOfYear;
      mDay = dayOfMonth;

      mCalendar = new GregorianCalendar(mYear, mMonth, mDay, mHour, mMinute);
      mAlarm.setDate(mCalendar.getTimeInMillis());

      updateButtons();
    }
  };

  private TimePickerDialog.OnTimeSetListener mTimeSetListener = new TimePickerDialog.OnTimeSetListener()
  {
    public void onTimeSet(TimePicker view, int hourOfDay, int minute)
    {
      mHour = hourOfDay;
      mMinute = minute;

      mCalendar = new GregorianCalendar(mYear, mMonth, mDay, mHour, mMinute);
      mAlarm.setDate(mCalendar.getTimeInMillis());

      updateButtons();
    }
  };

  private TextWatcher mTitleChangedListener = new TextWatcher()
  {
    public void afterTextChanged(Editable s)
    {
      mAlarm.setTitle(mTitle.getText().toString());
    }

    public void beforeTextChanged(CharSequence s, int start, int count, int after)
    {
    }

    public void onTextChanged(CharSequence s, int start, int before, int count)
    {
    }
  }; 


  private void updateButtons()
  {
    if (Alarm.ONCE == mAlarm.getOccurence())
      mDateButton.setText(mDateTime.formatDate(mAlarm));
    else if (Alarm.WEEKLY == mAlarm.getOccurence())
      mDateButton.setText(mDateTime.formatDays(mAlarm));
    mTimeButton.setText(mDateTime.formatTime(mAlarm));
  }

  private Dialog DaysPickerDialog()
  {
    AlertDialog.Builder builder;
    final boolean[] days = mDateTime.getDays(mAlarm);
    final String[] names = mDateTime.getFullDayNames();

    builder = new AlertDialog.Builder(this);

    builder.setTitle("Week days");

    builder.setMultiChoiceItems(names, days, new DialogInterface.OnMultiChoiceClickListener()
    {
      public void onClick(DialogInterface dialog, int whichButton, boolean isChecked)
      {
      }
    });

    builder.setPositiveButton("OK", new DialogInterface.OnClickListener()
    {
      public void onClick(DialogInterface dialog, int whichButton)
      {
        mDateTime.setDays(mAlarm, days);
        updateButtons();
      }
    });

    builder.setNegativeButton("Cancel", null);

    return builder.create();
  }





}

