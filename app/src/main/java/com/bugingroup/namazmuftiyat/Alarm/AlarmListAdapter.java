package com.bugingroup.namazmuftiyat.Alarm;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bugingroup.namazmuftiyat.R;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;


public class AlarmListAdapter extends BaseAdapter
{
  private final String TAG = "AlarmMe";

  private Context mContext;
  private DataSource mDataSource;
  private LayoutInflater mInflater;
  private DateTime mDateTime;
  private int mColorOutdated;
  private int mColorActive;
  private AlarmManager mAlarmManager;
  private static ArrayList<Alarm> mList = null;

  public AlarmListAdapter(Context context)
  {
     mList = new ArrayList<Alarm>();
    mContext = context;
    mDataSource = DataSource.getInstance(context);
    for (int i=0;i<mDataSource.size();i++){
      Alarm alarm = mDataSource.get(i);
       mList.add(alarm);

    }
    Log.d("mList", mList+"");
    Log.i(TAG, "AlarmListAdapter.create()");

    mInflater = LayoutInflater.from(context);
    mDateTime = new DateTime(context);

    mColorOutdated = mContext.getResources().getColor(R.color.gray);
    mColorActive = mContext.getResources().getColor(R.color.black);

    mAlarmManager = (AlarmManager)context.getSystemService(mContext.ALARM_SERVICE);

    dataSetChanged();
  }

  public void save()
  {
    mDataSource.save();
  }

  public void update(Alarm alarm)
  {
    mDataSource.update(alarm);
    dataSetChanged();
  }

  public void updateAlarms()
  {
    Log.i(TAG, "AlarmListAdapter.updateAlarms()");
    for (int i = 0; i < mDataSource.size(); i++)
      mDataSource.update(mDataSource.get(i));
    dataSetChanged();
  }

  public void add(Alarm alarm)
  {
    mDataSource.add(alarm);
    dataSetChanged();
  }

  public void delete(int index)
  {
    cancelAlarm(mDataSource.get(index));
    mDataSource.remove(index);
    dataSetChanged();
  }

  public boolean has_alarm(int index)
  {
    cancelAlarm(mDataSource.get(index));
    mDataSource.remove(index);
    dataSetChanged();
    return true;
  }

  public void clear()
  {
    for (int i = 0; i < mDataSource.size(); i++)
      cancelAlarm(mDataSource.get(i));

    if(mDataSource.size()>0)
    mDataSource.clean();
    dataSetChanged();
  }

  public void onSettingsUpdated()
  {
    mDateTime.update();
    dataSetChanged();
  }

  public int getCount()
  {
    return mList.size();
  }

  public Alarm getItem(int position)
  {
    return mList.get(position);
  }

  public long getItemId(int position)
  {
    return position;
  }

  public View getView(final int position, View convertView, ViewGroup parent)
  {
    ViewHolder holder;
    Alarm alarm = mList.get(position);

    if (convertView == null)
    {
      convertView = mInflater.inflate(R.layout.list_item, null);

      holder = new ViewHolder();
      holder.title = (TextView)convertView.findViewById(R.id.item_title);
      holder.details = (TextView)convertView.findViewById(R.id.item_details);
      holder.delete = (ImageView) convertView.findViewById(R.id.delete);
      holder.item_true = (ImageView) convertView.findViewById(R.id.item_true);

      convertView.setTag(holder);
    }
    else
    {
      holder = (ViewHolder)convertView.getTag();
    }


      holder.title.setText(alarm.getTitle());
      holder.details.setText(mDateTime.formatDetails(alarm) + (alarm.getEnabled() ? "" : " [disabled]"));

      if (alarm.getOutdated()) {
//        delete(position);
        holder.title.setTextColor(mColorOutdated);
        holder.item_true.setVisibility(View.VISIBLE);
      } else {
        holder.title.setTextColor(mColorActive);
        holder.item_true.setVisibility(View.GONE);
      }



    holder.delete.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        mList.remove(position);
        cancelAlarm(mDataSource.get(position));
        mDataSource.remove(position);
        dataSetChanged();
        Toast.makeText(mContext, "a"+position, Toast.LENGTH_SHORT).show();
      }
    });


    return convertView;
  }

  private void dataSetChanged()
  {
    for (int i = 0; i < mDataSource.size(); i++)
      setAlarm(mDataSource.get(i));

    notifyDataSetChanged();
  }

  private void setAlarm(Alarm alarm)
  {
    PendingIntent sender;
    Intent intent;

    Log.d("TTTTT", alarm.getEnabled()+"");
    Log.d("TTTTTT", alarm.getOutdated()+"");

    if (alarm.getEnabled() && !alarm.getOutdated())
    {
      intent = new Intent(mContext, AlarmReceiver.class);
      alarm.toIntent(intent);
      sender = PendingIntent.getBroadcast(mContext, (int)alarm.getId(), intent, PendingIntent.FLAG_UPDATE_CURRENT);
      if(alarm.is_debt() && alarm.getRepeat_t()!=0)
      mAlarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, alarm.getDate(), TimeUnit.MINUTES.toMillis(alarm.getRepeat_t()), sender);
      else
        mAlarmManager.setExact(AlarmManager.RTC_WAKEUP, alarm.getDate(), sender);

      Log.i(TAG, "AlarmListAdapter.setAlarm(" + alarm.getId() + ", '" + alarm.getTitle() + "', " + alarm.getDate()+")");
    }
  }

  public void cancelAlarm(Alarm alarm)
  {
    PendingIntent sender;
    Intent intent;
    intent = new Intent(mContext, AlarmReceiver.class);
    sender = PendingIntent.getBroadcast(mContext, (int)alarm.getId(), intent, PendingIntent.FLAG_UPDATE_CURRENT);
    mAlarmManager.cancel(sender);
  }

  static class ViewHolder
  {
    TextView title;
    TextView details;
    ImageView delete;
    ImageView item_true;
  }
}

