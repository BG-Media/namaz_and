package com.bugingroup.namazmuftiyat;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.SparseBooleanArray;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListView;

/**
 * Created by nurbaqyt on 04.08.17.
 */

public class Langs extends AppCompatActivity {
    SharedPreferences.Editor editor;
    ListView list;
    SharedPreferences settings;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.activity_common_list);
        this.list = (ListView) findViewById(R.id.list);
        this.list.setChoiceMode(2);
        this.list.setAdapter(ArrayAdapter.createFromResource(this, R.array.langs_list,android.R.layout.simple_list_item_multiple_choice));
        this.settings = getSharedPreferences(SplashScreen.PREFS_NAME, 0);
        this.editor = this.settings.edit();
        for (int i = 0; i < 4; i++) {
            this.list.setItemChecked(i, this.settings.getBoolean("ch" + i, true));
        }
    }

    public void onBackPressed() {
        SparseBooleanArray pos = this.list.getCheckedItemPositions();
        for (int i = 0; i < 4; i++) {
            this.editor.putBoolean("ch" + i, pos.get(i)).commit();
        }
        super.onBackPressed();
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return true;
    }
}