package com.bugingroup.namazmuftiyat;

import android.app.Dialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bugingroup.namazmuftiyat.Widget.NamazWidget;
import com.bugingroup.namazmuftiyat.utils.DatabaseHelper;
import com.bugingroup.namazmuftiyat.utils.TrackGPS;
import com.bugingroup.namazmuftiyat.utils.volley.AppController;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import butterknife.BindArray;
import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.bugingroup.namazmuftiyat.MainActivity.ASRM;
import static com.bugingroup.namazmuftiyat.MainActivity.HIGHET;
import static com.bugingroup.namazmuftiyat.MainActivity.M1;
import static com.bugingroup.namazmuftiyat.MainActivity.M2;
import static com.bugingroup.namazmuftiyat.MainActivity.M3;
import static com.bugingroup.namazmuftiyat.MainActivity.M4;
import static com.bugingroup.namazmuftiyat.MainActivity.M5;
import static com.bugingroup.namazmuftiyat.MainActivity.M6;
import static com.bugingroup.namazmuftiyat.MainActivity.METHOD;
import static com.bugingroup.namazmuftiyat.MainActivity.currentCityName;
import static com.bugingroup.namazmuftiyat.MainActivity.lang;
import static com.bugingroup.namazmuftiyat.MainActivity.latitude;
import static com.bugingroup.namazmuftiyat.MainActivity.longitude;
import static com.bugingroup.namazmuftiyat.SplashScreen.PREFS_NAME;

/**
 * Created by Nurik on 28.04.2017.
 */

public class ActivitySetting extends AppCompatActivity {

    @BindString(R.string.setting)
    String setting;

    @BindView(R.id.city)
    TextView city;
    @BindView(R.id.language)
    TextView language;
    SharedPreferences settings;
    String CITY_id;
    private TrackGPS gps;

    @BindView(R.id.fajir) TextView fajir;
    @BindView(R.id.voshod) TextView voshod;
    @BindView(R.id.zuhr) TextView zuhr;
    @BindView(R.id.asr) TextView asr;
    @BindView(R.id.magrib) TextView magrib;
    @BindView(R.id.isha) TextView isha;

    @BindString(R.string.language) String languages;
    @BindString(R.string.azan) String azan;
    @BindString(R.string.standart) String standart;
    @BindString(R.string.enable) String enable;
    @BindString(R.string.gps_title) String gps_title;
    @BindString(R.string.gps_message) String gps_message;
    @BindString(R.string.select_type) String select;
    @BindArray(R.array.namaz_name)
    String [] namaz_name;

//    @BindView(R.id.debt_switch)
//    Switch debt_switch;

    @BindView(R.id.method)
    Spinner method;

    @BindArray(R.array.method_array)
    String[] method_array;

    @BindView(R.id.method_asr)
    Spinner method_asr;

    @BindArray(R.array.asr_array)
    String[] asr_array;

    @BindView(R.id.higherLatitudes)
    Spinner higherLatitudes;

    @BindArray(R.array.higher_array)
    String[] higher_array;

    @BindView(R.id.korrection_fajir)
    Spinner korrection_fajir;
    @BindView(R.id.korrection_voshod)
    Spinner korrection_voshod;

    @BindView(R.id.korrection_zuhr)
    Spinner korrection_zuhr;

    @BindView(R.id.korrection_asr)
    Spinner korrection_asr;

    @BindView(R.id.korrection_magrib)
    Spinner korrection_magrib;

    @BindView(R.id.korrection_isha)
    Spinner korrection_isha;

    DatabaseHelper databaseHelper = new DatabaseHelper(ActivitySetting.this);
    double timezone;

    int [] Minute = new int[61];
    String [] minute = new String[61];


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(setting);

        for (int i=0;i<61;i++){
            Minute[i] = i-30;
            minute[i] = i-30 + " мин.";
        }

        settings = getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        CITY_id = settings.getString("city_id","");
        switch (settings.getString("language","")){
            case "ru": language.setText("Русский");break;
            case "kk":
            default:  language.setText("Қазақша");break;
        }

        city.setText(settings.getString("city",select));
//         debt_switch.setChecked(settings.getBoolean("debt_notification",false));
         switch (settings.getInt("fajir_select",3)){
             case 1: fajir.setText(azan);break;
             case 2: fajir.setText(standart);break;
             case 3: fajir.setText(enable);break;
         }
         switch (settings.getInt("voshod_select",3)){
             case 1: voshod.setText(azan);break;
             case 2: voshod.setText(standart);break;
             case 3: voshod.setText(enable);break;
         }
         switch (settings.getInt("zuhr_select",3)){
             case 1: zuhr.setText(azan);break;
             case 2: zuhr.setText(standart);break;
             case 3: zuhr.setText(enable);break;
         }
         switch (settings.getInt("asr_select",3)){
             case 1: asr.setText(azan);break;
             case 2: asr.setText(standart);break;
             case 3: asr.setText(enable);break;
         }
         switch (settings.getInt("magrib_select",3)){
             case 1: magrib.setText(azan);break;
             case 2: magrib.setText(standart);break;
             case 3: magrib.setText(enable);break;
         }
         switch (settings.getInt("isha_select",3)){
             case 1: isha.setText(azan);break;
             case 2: isha.setText(standart);break;
             case 3: isha.setText(enable);break;
         }
        gps = new TrackGPS(ActivitySetting.this);




        ArrayAdapter<String> methodArrayAdapter = new ArrayAdapter<String>(ActivitySetting.this, android.R.layout.simple_spinner_item, method_array);
        methodArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        method.setAdapter(methodArrayAdapter);
        switch (METHOD){
            case 3:method.setSelection(1);break;
            case 2:method.setSelection(2);break;
            case 4:method.setSelection(3);break;
            case 1:method.setSelection(4);break;
            case 8: default: method.setSelection(0);break;
        }

        ArrayAdapter<String> asrArrayAdapter = new ArrayAdapter<String>(ActivitySetting.this, android.R.layout.simple_spinner_item, asr_array);
        asrArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        method_asr.setAdapter(asrArrayAdapter);
        switch (ASRM){
            case 1 : default: method_asr.setSelection(0);
            case 0 : method_asr.setSelection(1);
        }

        ArrayAdapter<String> higherArrayAdapter = new ArrayAdapter<String>(ActivitySetting.this, android.R.layout.simple_spinner_item, higher_array);
        higherArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        higherLatitudes.setAdapter(higherArrayAdapter);
        switch (HIGHET){
            case 3: default: higherLatitudes.setSelection(0); break;
            case 1: higherLatitudes.setSelection(1); break;
            case 2: higherLatitudes.setSelection(2); break;
            case 0: higherLatitudes.setSelection(3); break;
        }

        ArrayAdapter<String> fajirArrayAdapter = new ArrayAdapter<String>(ActivitySetting.this, android.R.layout.simple_spinner_item, minute);
        fajirArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        korrection_fajir.setAdapter(fajirArrayAdapter);
        korrection_fajir.setSelection(30+M1);

        ArrayAdapter<String> voshodArrayAdapter = new ArrayAdapter<String>(ActivitySetting.this, android.R.layout.simple_spinner_item, minute);
        voshodArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        korrection_voshod.setAdapter(fajirArrayAdapter);
        korrection_voshod.setSelection(30+M2);

        ArrayAdapter<String> zuhrArrayAdapter = new ArrayAdapter<String>(ActivitySetting.this, android.R.layout.simple_spinner_item, minute);
        zuhrArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        korrection_zuhr.setAdapter(fajirArrayAdapter);
        korrection_zuhr.setSelection(30+M3);

        ArrayAdapter<String> aasrArrayAdapter = new ArrayAdapter<String>(ActivitySetting.this, android.R.layout.simple_spinner_item, minute);
        aasrArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        korrection_asr.setAdapter(fajirArrayAdapter);
        korrection_asr.setSelection(30+M4);

        ArrayAdapter<String> magribArrayAdapter = new ArrayAdapter<String>(ActivitySetting.this, android.R.layout.simple_spinner_item, minute);
        magribArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        korrection_magrib.setAdapter(fajirArrayAdapter);
        korrection_magrib.setSelection(30+M5);

        ArrayAdapter<String> ishaArrayAdapter = new ArrayAdapter<String>(ActivitySetting.this, android.R.layout.simple_spinner_item, minute);
        ishaArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        korrection_isha.setAdapter(fajirArrayAdapter);
        korrection_isha.setSelection(30+M6);




        /////////////////////////////////////////////////////
        method_asr.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0: ASRM = 1; break;
                    case 1: ASRM = 0; break;
                }
                SharedPreferences.Editor editor = settings.edit();
                editor.putInt("ASRM",ASRM);
                editor.commit();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        /////////////////////////////////////////////////////
        higherLatitudes.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0: HIGHET = 3; break;
                    case 1: HIGHET = 1; break;
                    case 2: HIGHET = 2; break;
                    case 3: HIGHET = 0; break;
                }
                SharedPreferences.Editor editor = settings.edit();
                editor.putInt("HIGHET",HIGHET);
                editor.commit();
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        /////////////////////////////////////////////////////
        korrection_fajir.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                M1 = Minute[position];
                SharedPreferences.Editor editor = settings.edit();
                editor.putInt("M1",M1);
                editor.commit();
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        /////////////////////////////////////////////////////
        korrection_voshod.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                M2 = Minute[position];

                SharedPreferences.Editor editor = settings.edit();
                editor.putInt("M2",M2);
                editor.commit();
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        /////////////////////////////////////////////////////
        korrection_zuhr.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                M3 = Minute[position];

                SharedPreferences.Editor editor = settings.edit();
                editor.putInt("M3",M3);
                editor.commit();
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        /////////////////////////////////////////////////////
        korrection_asr.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                M4 = Minute[position];

                SharedPreferences.Editor editor = settings.edit();
                editor.putInt("M4",M4);
                editor.commit();
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        /////////////////////////////////////////////////////
        korrection_magrib.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                M5 = Minute[position];

                SharedPreferences.Editor editor = settings.edit();
                editor.putInt("M5",M5);
                editor.commit();
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        /////////////////////////////////////////////////////
        korrection_isha.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                M6 = Minute[position];
                SharedPreferences.Editor editor = settings.edit();
                editor.putInt("M6",M6);
                editor.commit();
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        ////////////////////////////////////////////////////
        method.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0: METHOD = 8; Spinnerenabled(false); break;
                    case 1: METHOD = 3; Spinnerenabled(true); break;
                    case 2: METHOD = 2; Spinnerenabled(true); break;
                    case 3: METHOD = 4; Spinnerenabled(true); break;
                    case 4: METHOD = 1; Spinnerenabled(true); break;
                }
                SharedPreferences.Editor editor = settings.edit();
                editor.putInt("METHOD",METHOD);
                editor.commit();
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

    }

    public void Spinnerenabled(boolean b){
        if(!b) {
            method_asr.setSelection(0);
            higherLatitudes.setSelection(0);
            SharedPreferences.Editor editor = settings.edit();
            editor.putInt("ASRM",1);
            editor.putInt("HIGHET",3);
            editor.commit();

        }
        ((Spinner) method_asr).getSelectedView().setEnabled(b);
        method_asr.setEnabled(b);
        ((Spinner) higherLatitudes).getSelectedView().setEnabled(b);
        higherLatitudes.setEnabled(b);
    }


    @OnClick(R.id.language)
    void languageClick(){

        final Dialog dialog = new Dialog(ActivitySetting.this);
        dialog.setContentView(R.layout.dialog_language);
        dialog.setTitle(languages);

        RadioButton rb_kz = (RadioButton)dialog.findViewById(R.id.rb_kz);
        RadioButton rb_ru = (RadioButton)dialog.findViewById(R.id.rb_ru);
        switch (settings.getString("language","")) {
            case "ru":
                rb_ru.setChecked(true);
                break;
            case "kk":
            default:
                rb_kz.setChecked(true);
                break;
        }
        dialog.show();


        rb_kz.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked)
                    ChangeLanguage("kk");
                dialog.dismiss();

            }
        });
        rb_ru.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked)
                    ChangeLanguage("ru");
                dialog.dismiss();
            }
        });


        Button no =(Button) dialog.findViewById(R.id.no);
        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data == null) {return;}
        String name = data.getStringExtra("name");
        city.setText(name);
        GetTimeZone();

    }

    public void ChangeLanguage(String loc){


        SharedPreferences.Editor editor = settings.edit();
        editor.putString("language", loc);
        editor.commit();
        lang = loc;

        Locale locale = new Locale(loc);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config,
                getBaseContext().getResources().getDisplayMetrics());
        startActivity(new Intent(ActivitySetting.this,MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
        finish();
    }
    @OnClick(R.id.timing)
    void Timing(){
        startActivity(new Intent(ActivitySetting.this,GetNamazTime.class));
    }

    @OnClick(R.id.city)
    void City(){
        final Dialog dialog = new Dialog(ActivitySetting.this);
        dialog.setContentView(R.layout.dialog_city);
        dialog.setTitle(select);
        dialog.show();

        //GPS
        Button gps_choose = (Button)dialog.findViewById(R.id.gps_choose);
        gps_choose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if(gps.canGetLocation()){

                    longitude = gps.getLongitude();
                    latitude = gps .getLatitude();
                    GetTimeZone();
                } else {
                    gps.showSettingsAlert(gps_title,gps_message);
                }
                SharedPreferences.Editor editor = settings.edit();

                editor.putBoolean("is_gps", true);
                editor.putString("city_id", "0");
                editor.putString("coords", latitude+","+longitude);
                Geocoder gcd = new Geocoder(getApplicationContext(), Locale.getDefault());
                List<Address> addresses = null;
                try {
                    addresses = gcd.getFromLocation(latitude, longitude, 1);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                if (addresses != null) {
                    if (addresses.size() > 0) {
                        currentCityName = addresses.get(0).getLocality();

                        city.setText(addresses.get(0).getLocality());
                        editor.putString("city", addresses.get(0).getLocality());

                    }
                }
                editor.commit();

            }
        });
        //LIST
        Button list_choose = (Button)dialog.findViewById(R.id.list_choose);
        list_choose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = settings.edit();
                editor.putBoolean("is_gps", false);
                editor.commit();
                dialog.dismiss();
                startActivityForResult(new Intent(ActivitySetting.this,CityList.class),704);
            }
        });


    }

    private void GetTimeZone() {

        StringRequest jsonReq = new StringRequest (Request.Method.GET,
                "http://api.geonames.org/timezoneJSON?lat="+latitude+"&lng="+longitude+"&username=adiletmaks",new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("RRRRRR",response);
                if(response!=null){
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        timezone = jsonObject.getDouble("gmtOffset");
                        Log.d("TZTZTZTZT",timezone+"   ");
                        databaseHelper.setTimeZone(timezone+"", ActivitySetting.this);
                        try {
                            Intent updateWidget = new Intent(ActivitySetting.this, NamazWidget.class); // Widget.class is your widget class
                            updateWidget.setAction("update_widget");
                            PendingIntent pending = PendingIntent.getBroadcast(ActivitySetting.this, 0, updateWidget, PendingIntent.FLAG_CANCEL_CURRENT);
                            pending.send();
                        } catch (PendingIntent.CanceledException e) {
                            e.printStackTrace();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json");
                return headers;
            }

        };
        AppController.getInstance().addToRequestQueue(jsonReq);
    }


    @OnClick(R.id.azan)
    void AzanClick(){
        startActivity(new Intent(ActivitySetting.this,ChooseAzan.class));

    }

    @OnClick(R.id.fajir)
    void fajirClick() {
        final SharedPreferences.Editor editor = settings.edit();

        final Dialog dialog = new Dialog(ActivitySetting.this);
        dialog.setContentView(R.layout.dialog_select_azan);
        dialog.setTitle(namaz_name[0]);
        dialog.show();
        final RadioButton rb_azan = (RadioButton) dialog.findViewById(R.id.rb_azan);
        final RadioButton rb_standart = (RadioButton) dialog.findViewById(R.id.rb_standart);
        final RadioButton rb_enabled = (RadioButton) dialog.findViewById(R.id.rb_enabled);

        switch (settings.getInt("fajir_select", 3)) {
            case 1:
                rb_azan.setChecked(true);
                break;
            case 2:
                rb_standart.setChecked(true);
                break;
            case 3:
                rb_enabled.setChecked(true);
                break;
        }

        rb_azan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fajir.setText(azan);
                editor.putInt("fajir_select", 1);
                editor.commit();
                dialog.dismiss();
            }
        });

        rb_standart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fajir.setText(standart);
                editor.putInt("fajir_select", 2);
                editor.commit();
                dialog.dismiss();
            }
        });

        rb_enabled.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fajir.setText(enable);
                editor.putInt("fajir_select", 3);
                editor.commit();
                dialog.dismiss();
            }
        });
    }

    @OnClick(R.id.voshod)
    void voshodClick(){
        final SharedPreferences.Editor editor = settings.edit();

        final Dialog dialog = new Dialog(ActivitySetting.this);
        dialog.setContentView(R.layout.dialog_select_azan);
        dialog.setTitle(namaz_name[1]);
        dialog.show();
        final RadioButton rb_azan = (RadioButton) dialog.findViewById(R.id.rb_azan);
        final RadioButton rb_standart = (RadioButton) dialog.findViewById(R.id.rb_standart);
        final RadioButton rb_enabled = (RadioButton) dialog.findViewById(R.id.rb_enabled);

       switch (settings.getInt("voshod_select",3)){
           case 1:rb_azan.setChecked(true);break;
           case 2:rb_standart.setChecked(true);break;
           case 3:rb_enabled.setChecked(true);break;
       }

        rb_azan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                voshod.setText(azan);
                editor.putInt("voshod_select", 1);
                editor.commit();
                dialog.dismiss();
            }
        });

        rb_standart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                voshod.setText(standart);
                editor.putInt("voshod_select",2);
                editor.commit();
                dialog.dismiss();
            }
        });

        rb_enabled.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                voshod.setText(enable);
                editor.putInt("voshod_select", 3);
                editor.commit();
                dialog.dismiss();
            }
        });

    }


    @OnClick(R.id.zuhr)
    void zuhrClick(){
        final SharedPreferences.Editor editor = settings.edit();

        final Dialog dialog = new Dialog(ActivitySetting.this);
        dialog.setContentView(R.layout.dialog_select_azan);
        dialog.setTitle(namaz_name[2]);
        dialog.show();
        final RadioButton rb_azan = (RadioButton) dialog.findViewById(R.id.rb_azan);
        final RadioButton rb_standart = (RadioButton) dialog.findViewById(R.id.rb_standart);
        final RadioButton rb_enabled = (RadioButton) dialog.findViewById(R.id.rb_enabled);

       switch (settings.getInt("zuhr_select",3)){
           case 1:rb_azan.setChecked(true);break;
           case 2:rb_standart.setChecked(true);break;
           case 3:rb_enabled.setChecked(true);break;
       }

        rb_azan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                zuhr.setText(azan);
                editor.putInt("zuhr_select", 1);
                editor.commit();
                dialog.dismiss();
            }
        });

        rb_standart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                zuhr.setText(standart);
                editor.putInt("zuhr_select",2);
                editor.commit();
                dialog.dismiss();
            }
        });

        rb_enabled.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                zuhr.setText(enable);
                editor.putInt("zuhr_select", 3);
                editor.commit();
                dialog.dismiss();
            }
        });

    }

    @OnClick(R.id.asr)
    void asrClick(){
        final SharedPreferences.Editor editor = settings.edit();

        final Dialog dialog = new Dialog(ActivitySetting.this);
        dialog.setContentView(R.layout.dialog_select_azan);
        dialog.setTitle(namaz_name[2]);
        dialog.show();
        final RadioButton rb_azan = (RadioButton) dialog.findViewById(R.id.rb_azan);
        final RadioButton rb_standart = (RadioButton) dialog.findViewById(R.id.rb_standart);
        final RadioButton rb_enabled = (RadioButton) dialog.findViewById(R.id.rb_enabled);

       switch (settings.getInt("asr_select",3)){
           case 1:rb_azan.setChecked(true);break;
           case 2:rb_standart.setChecked(true);break;
           case 3:rb_enabled.setChecked(true);break;
       }

        rb_azan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                asr.setText(azan);
                editor.putInt("asr_select", 1);
                editor.commit();
                dialog.dismiss();
            }
        });

        rb_standart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                asr.setText(standart);
                editor.putInt("asr_select",2);
                editor.commit();
                dialog.dismiss();
            }
        });

        rb_enabled.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                asr.setText(enable);
                editor.putInt("asr_select", 3);
                editor.commit();
                dialog.dismiss();
            }
        });

    }


    @OnClick(R.id.magrib)
    void magribClick(){
        final SharedPreferences.Editor editor = settings.edit();

        final Dialog dialog = new Dialog(ActivitySetting.this);
        dialog.setContentView(R.layout.dialog_select_azan);
        dialog.setTitle(namaz_name[2]);
        dialog.show();
        final RadioButton rb_azan = (RadioButton) dialog.findViewById(R.id.rb_azan);
        final RadioButton rb_standart = (RadioButton) dialog.findViewById(R.id.rb_standart);
        final RadioButton rb_enabled = (RadioButton) dialog.findViewById(R.id.rb_enabled);

       switch (settings.getInt("magrib_select",3)){
           case 1:rb_azan.setChecked(true);break;
           case 2:rb_standart.setChecked(true);break;
           case 3:rb_enabled.setChecked(true);break;
       }

        rb_azan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                magrib.setText(azan);
                editor.putInt("magrib_select", 1);
                editor.commit();
                dialog.dismiss();
            }
        });

        rb_standart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                magrib.setText(standart);
                editor.putInt("magrib_select",2);
                editor.commit();
                dialog.dismiss();
            }
        });

        rb_enabled.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                magrib.setText(enable);
                editor.putInt("magrib_select", 3);
                editor.commit();
                dialog.dismiss();
            }
        });

    }


    @OnClick(R.id.isha)
    void ishaClick(){
        final SharedPreferences.Editor editor = settings.edit();

        final Dialog dialog = new Dialog(ActivitySetting.this);
        dialog.setContentView(R.layout.dialog_select_azan);
        dialog.setTitle(namaz_name[2]);
        dialog.show();
        final RadioButton rb_azan = (RadioButton) dialog.findViewById(R.id.rb_azan);
        final RadioButton rb_standart = (RadioButton) dialog.findViewById(R.id.rb_standart);
        final RadioButton rb_enabled = (RadioButton) dialog.findViewById(R.id.rb_enabled);

       switch (settings.getInt("isha_select",3)){
           case 1:rb_azan.setChecked(true);break;
           case 2:rb_standart.setChecked(true);break;
           case 3:rb_enabled.setChecked(true);break;
       }

        rb_azan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isha.setText(azan);
                editor.putInt("isha_select", 1);
                editor.commit();
                dialog.dismiss();
            }
        });

        rb_standart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isha.setText(standart);
                editor.putInt("isha_select",2);
                editor.commit();
                dialog.dismiss();
            }
        });

        rb_enabled.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isha.setText(enable);
                editor.putInt("isha_select", 3);
                editor.commit();
                dialog.dismiss();
            }
        });

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }




}
