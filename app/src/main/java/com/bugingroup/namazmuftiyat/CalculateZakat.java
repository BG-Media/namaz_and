package com.bugingroup.namazmuftiyat;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.opengl.ETC1;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bugingroup.namazmuftiyat.utils.DatabaseHelper;
import com.bugingroup.namazmuftiyat.utils.Zakat;
import com.google.android.youtube.player.YouTubePlayer;
import com.thefinestartist.ytpa.YouTubePlayerActivity;
import com.thefinestartist.ytpa.enums.Orientation;
import com.thefinestartist.ytpa.utils.YouTubeUrlParser;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


/**
 * Created by Nurik on 06.04.2017.
 */

public class CalculateZakat extends AppCompatActivity {

    @BindView(R.id.zoloto_tenge) EditText zoloto_tenge;

    @BindView(R.id.dnejni) EditText dnejni;
    @BindView(R.id.result_dnejni) TextView result_dnejni;
    @BindView(R.id.layout_dnejni) LinearLayout layout_dnejni;

    @BindView(R.id.zoloto) EditText zoloto;
    @BindView(R.id.result_zoloto) TextView result_zoloto;
    @BindView(R.id.layout_zoloto) LinearLayout layout_zoloto;

    @BindView(R.id.serebro) EditText serebro;
    @BindView(R.id.result_serebro) TextView result_serebro;
    @BindView(R.id.layout_serebro) LinearLayout layout_serebro;

    @BindView(R.id.verbliud) EditText verbliud;
    @BindView(R.id.result_verbliud) TextView result_verbliud;
    @BindView(R.id.layout_verbliud) LinearLayout layout_verbliud;

    @BindView(R.id.baran) EditText baran;
    @BindView(R.id.result_baran) TextView result_baran;
    @BindView(R.id.layout_baran) LinearLayout layout_baran;

    @BindView(R.id.loshad) EditText loshad;
    @BindView(R.id.loshad_cena) EditText loshad_cena;
    @BindView(R.id.result_loshad) TextView result_loshad;
    @BindView(R.id.layout_loshad) LinearLayout layout_loshad;

    @BindView(R.id.krc) EditText krc;
    @BindView(R.id.result_krc) TextView result_krc;
    @BindView(R.id.layout_krc) LinearLayout layout_krc;

    @BindView(R.id.predmet) EditText predmet;
    @BindView(R.id.result_predmet) TextView result_predmet;
    @BindView(R.id.layout_predmet) LinearLayout layout_predmet;

    @BindView(R.id.save) Button save_btn;
    TextView DiaMsg,DiaTitle;
    Dialog dialog;
    Button btnOk;

    DatabaseHelper db;
    int id=0;
    @BindString(R.string.dnejni_title)  String dnejni_title;
    @BindString(R.string.dnejni_info)  String dnejni_info;

    @BindString(R.string.zoloto_title)  String zoloto_title;
    @BindString(R.string.zoloto_info)   String zoloto_info;

    @BindString(R.string.serebro_title)  String serebro_title;
    @BindString(R.string.serebro_info)   String serebro_info;

    @BindString(R.string.verbliud_title)  String verbliud_title;
    @BindString(R.string.verbliud_info)   String verbliud_info;

    @BindString(R.string.baran_title)  String baran_title;
    @BindString(R.string.baran_info)   String baran_info;

    @BindString(R.string.loshad_title)  String loshad_title;
    @BindString(R.string.loshad_info)   String loshad_info;

    @BindString(R.string.krc_title)  String krc_title;
    @BindString(R.string.krc_info)   String krc_info;

    @BindString(R.string.predmet_title)  String predmet_title;
    @BindString(R.string.predmet_info)   String predmet_info;

    @BindString(R.string.cal_zakat)   String cal_zakat;
    @BindString(R.string.save)   String save;
    @BindString(R.string.are_you_save)   String are_you_save;
    @BindString(R.string.yes)   String yes;
    @BindString(R.string.no)   String no;
    @BindString(R.string.no_zakat)   String no_zakat;
    @BindString(R.string.a_baran)   String a_baran;

    @BindString(R.string.a_verbliud)   String a_verbliud;
    @BindString(R.string.verbliud_36)   String verbliud_36;
    @BindString(R.string.verbliud_46)   String verbliud_46;
    @BindString(R.string.verbliud_61)   String verbliud_61;
    @BindString(R.string.verbliud_76)   String verbliud_76;
    @BindString(R.string.verbliud_91)   String verbliud_91;
    @BindString(R.string.verbliud_121)   String verbliud_121;
    @BindString(R.string.verbliud_125)   String verbliud_125;
    @BindString(R.string.verbliud_130)   String verbliud_130;
    @BindString(R.string.verbliud_135)   String verbliud_135;
    @BindString(R.string.verbliud_140)   String verbliud_140;
    @BindString(R.string.verbliud_145)   String verbliud_145;
    @BindString(R.string.verbliud_150)   String verbliud_150;
    @BindString(R.string.verbliud_155)   String verbliud_155;
    @BindString(R.string.verbliud_160)   String verbliud_160;
    @BindString(R.string.verbliud_165)   String verbliud_165;
    @BindString(R.string.verbliud_175)   String verbliud_175;
    @BindString(R.string.verbliud_186)   String verbliud_186;
    @BindString(R.string.verbliud_196)   String verbliud_196;
    @BindString(R.string.krc30)   String krc30;
    @BindString(R.string.krc40)   String krc40;
    @BindString(R.string.krc60)   String krc60;
    @BindString(R.string.krc70)   String krc70;
    @BindString(R.string.krc80)   String krc80;
    @BindString(R.string.krc90)   String krc90;
    @BindString(R.string.krc100)   String krc100;
    @BindString(R.string.krc120)   String krc120;



    @BindString(R.string.valid_empty)
    String valid_empty;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zakat_calc);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(cal_zakat);

         dialog = new Dialog(CalculateZakat.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.info_dialog);
        DiaMsg = (TextView) dialog.findViewById(R.id.DiaMsg);
        DiaTitle = (TextView) dialog.findViewById(R.id.DiaTitle);
        btnOk = (Button) dialog.findViewById(R.id.btnOk);
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        db = new DatabaseHelper(getApplicationContext());

        id = getIntent().getIntExtra("id",0);
        if(id!=0){
            Zakat zakat  = db.getSingle(id);
            dnejni      .setText(zakat.getDnejni());
            zoloto      .setText(zakat.getZoloto());
            serebro     .setText(zakat.getSerebro());
            verbliud    .setText(zakat.getVerbliud());
            baran       .setText(zakat.getBaran());
            loshad      .setText(zakat.getLoshad());
            loshad_cena .setText(zakat.getLoshadc());
            krc         .setText(zakat.getKrc());
            predmet     .setText(zakat.getPredmet());
            ResultZakat();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @OnClick(R.id.result)
    void onResultClick(){
        ResultZakat();
    }

    @OnClick(R.id.btn_dnejni)
    void btn_dnejni(){
        DiaTitle.setText(dnejni_title);
        DiaMsg.setText(dnejni_info);
        dialog.show();
    }

    @OnClick(R.id.btn_zoloto)
    void btn_zoloto(){
        DiaTitle.setText(zoloto_title);
        DiaMsg.setText(zoloto_info);
        dialog.show();
    }

    @OnClick(R.id.btn_serebro)
    void btn_serebro(){
        DiaTitle.setText(serebro_title);
        DiaMsg.setText(serebro_info);
        dialog.show();
    }

    @OnClick(R.id.btn_verbliud)
    void btn_verbliud(){
        DiaTitle.setText(verbliud_title);
        DiaMsg.setText(verbliud_info);
        dialog.show();
    }

    @OnClick(R.id.btn_baran)
    void btn_baran(){
        DiaTitle.setText(baran_title);
        DiaMsg.setText(baran_info);
        dialog.show();
    }

    @OnClick(R.id.btn_loshad)
    void btn_loshad(){
        DiaTitle.setText(loshad_title);
        DiaMsg.setText(loshad_info);
        dialog.show();
    }

    @OnClick(R.id.btn_krc)
    void btn_krc(){
        DiaTitle.setText(krc_title);
        DiaMsg.setText(krc_info);
        dialog.show();
    }
    @OnClick(R.id.btn_predmet)
    void btn_predmet(){
        DiaTitle.setText(predmet_title);
        DiaMsg.setText(predmet_info);
        dialog.show();
    }

   @OnClick(R.id.pay)
    void pay(){
       final Dialog  dialoga = new Dialog(CalculateZakat.this);
       dialoga.requestWindowFeature(Window.FEATURE_NO_TITLE);
       dialoga.setContentView(R.layout.activity_help_pay);
       dialoga.show();
       final EditText price = (EditText) dialoga.findViewById(R.id.price) ;
       Button pp = (Button) dialoga.findViewById(R.id.pay) ;
       pp.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               if(price.getText().toString().length()>0) {
                   dialoga.dismiss();
                   startActivity(new Intent(CalculateZakat.this, ActivityPay.class).putExtra("price", price.getText().toString()));
               }
               else Toast.makeText(CalculateZakat.this, valid_empty, Toast.LENGTH_SHORT).show();
           }
       });

      // startActivity(new Intent(CalculateZakat.this,ActivityRekvizit.class));
    }

   @OnClick(R.id.save)
    void save(){
           showDialog(0);

      // startActivity(new Intent(CalculateZakat.this,ActivityRekvizit.class));
    }

    protected Dialog onCreateDialog(int id) {
        if (id == 0) {
            AlertDialog.Builder adb = new AlertDialog.Builder(this);
            // заголовок
            adb.setTitle(save);
            // сообщение
            adb.setMessage(are_you_save);

            // кнопка положительного ответа
            adb.setPositiveButton(yes, myClickListener);
            // кнопка отрицательного ответа
            adb.setNegativeButton(no, myClickListener);
            // создаем диалог
            return adb.create();
        }
        return super.onCreateDialog(id);
    }

    DialogInterface.OnClickListener myClickListener = new DialogInterface.OnClickListener() {
        public void onClick(DialogInterface dialog, int which) {
            switch (which) {
                // положительная кнопка
                case Dialog.BUTTON_POSITIVE:
                    String d = dnejni.getText().toString().length()>0?dnejni.getText().toString():"";
                    String z = zoloto.getText().toString().length()>0?zoloto.getText().toString():"";
                    String s = serebro.getText().toString().length()>0?serebro.getText().toString():"";
                    String v = verbliud.getText().toString().length()>0?verbliud.getText().toString():"";
                    String b = baran.getText().toString().length()>0?baran.getText().toString():"";
                    String l = loshad.getText().toString().length()>0?loshad.getText().toString():"";
                    String lc = loshad_cena.getText().toString().length()>0?loshad_cena.getText().toString():"";
                    String k = krc.getText().toString().length()>0?krc.getText().toString():"";
                    String p = predmet.getText().toString().length()>0?predmet.getText().toString():"";
                    Zakat zakat = new Zakat(d,z,s,v,b,l,lc,k,p);
                    db.createToDo(zakat);
                    db.closeDB();
                    startActivity(new Intent(CalculateZakat.this,ActivityRekvizit.class));
                    finish();
                    break;
                // негативная кнопка
                case Dialog.BUTTON_NEGATIVE:
                    finish();
                    break;

            }
        }
    };
    public void ResultZakat(){

        //DNEJNI ***************************
        if(dnejni.getText().length()>0){
            if(Float.valueOf(dnejni.getText().toString())>=1112735) {
                float r = (float) (Float.valueOf(dnejni.getText().toString())*0.025);
                result_dnejni.setText(r + " KZT");
            }else {
                result_dnejni.setText(no_zakat);
                result_dnejni.setTextSize(10);
            }
            layout_dnejni.setVisibility(View.VISIBLE);
        }else layout_dnejni.setVisibility(View.GONE);

        //ZOLOTO ***************************
        if(zoloto.getText().length()>0){
            if(Float.valueOf(zoloto.getText().toString())>=85) {
                float r = (float) (Float.valueOf(zoloto.getText().toString()) * Float.valueOf(zoloto.getText().toString()) * 0.025);
                result_zoloto.setText(r + " KZT");
            }else {
                result_zoloto.setText(no_zakat);
                result_zoloto.setTextSize(10);

            }
            layout_zoloto.setVisibility(View.VISIBLE);
        }else layout_zoloto.setVisibility(View.GONE);

        //SEREBRO ***************************
        if(serebro.getText().length()>0){
            if(Float.valueOf(serebro.getText().toString())>=595) {
                float r = (float) (Float.valueOf(serebro.getText().toString()) * 183 * 0.025);
                result_serebro.setText(r + " KZT");
            }else {
                result_serebro.setText(no_zakat);
                result_serebro.setTextSize(10);

            }
            layout_serebro.setVisibility(View.VISIBLE);
        }else layout_serebro.setVisibility(View.GONE);


        //VERBLIUD ***************************
        if(verbliud.getText().length()>0){
            int v = Integer.valueOf(verbliud.getText().toString());
            if(v<5) {
                result_verbliud.setText(no_zakat);
                result_verbliud.setTextSize(10);
            }else if(v>=5 && v<=9) {
                result_verbliud.setText("1 "+a_baran);
                result_verbliud.setTextSize(10);
            }else if(v>=10 && v<=14) {
                result_verbliud.setText("2 "+a_baran);
                result_verbliud.setTextSize(10);
            }else if(v>=15 && v<=19) {
                result_verbliud.setText("3 "+a_baran);
                result_verbliud.setTextSize(10);
            }else if(v>=20 && v<=24) {
                result_verbliud.setText("4 "+a_baran);
                result_verbliud.setTextSize(10);
            }else if(v>=25 && v<=35) {
                result_verbliud.setText(a_verbliud);
                result_verbliud.setTextSize(10);
            }else if(v>=36 && v<=45) {
                result_verbliud.setText(verbliud_36);
                result_verbliud.setTextSize(10);
            }else if(v>=46 && v<=60) {
                result_verbliud.setText(verbliud_46);
                result_verbliud.setTextSize(10);
            }else if(v>=61 && v<=75) {
                result_verbliud.setText(verbliud_61);
                result_verbliud.setTextSize(10);
            }else if(v>=76 && v<=90) {
                result_verbliud.setText(verbliud_76);
                result_verbliud.setTextSize(10);
            }else if(v>=91 && v<=120) {
                result_verbliud.setText(verbliud_91);
                result_verbliud.setTextSize(10);
            }else if(v>=121 && v<=124) {
                result_verbliud.setText(verbliud_121);
                result_verbliud.setTextSize(10);
            }else if(v>=125 && v<=129) {
                result_verbliud.setText(verbliud_125);
                result_verbliud.setTextSize(10);
            }else if(v>=130 && v<=134) {
                result_verbliud.setText(verbliud_130);
                result_verbliud.setTextSize(10);
            }else if(v>=135 && v<=139) {
                result_verbliud.setText(verbliud_135);
                result_verbliud.setTextSize(10);
            }else if(v>=140 && v<=144) {
                result_verbliud.setText(verbliud_140);
                result_verbliud.setTextSize(10);
            }else if(v>=145 && v<=149) {
                result_verbliud.setText(verbliud_145);
                result_verbliud.setTextSize(10);
            }else if(v>=150 && v<=154) {
                result_verbliud.setText(verbliud_150);
                result_verbliud.setTextSize(10);
            }else if(v>=155 && v<=159) {
                result_verbliud.setText(verbliud_155);
                result_verbliud.setTextSize(10);
            }else if(v>=160 && v<=164) {
                result_verbliud.setText(verbliud_160);
                result_verbliud.setTextSize(10);
            }else if(v>=165 && v<=169) {
                result_verbliud.setText(verbliud_165);
                result_verbliud.setTextSize(10);
            }else if(v>=170 && v<=174) {
                result_verbliud.setText(verbliud_165);
                result_verbliud.setTextSize(10);
            }else if(v>=175 && v<=185) {
                result_verbliud.setText(verbliud_175);
                result_verbliud.setTextSize(10);
            }else if(v>=186 && v<=195) {
                result_verbliud.setText(verbliud_186);
                result_verbliud.setTextSize(10);
            }else if(v>=196 && v<=200) {
                result_verbliud.setText(verbliud_196);
                result_verbliud.setTextSize(10);
            }
            layout_verbliud.setVisibility(View.VISIBLE);
        }else layout_verbliud.setVisibility(View.GONE);


        //BARAN ***************************
        if(baran.getText().length()>0){
            int v = Integer.valueOf(baran.getText().toString());
            if(v<40) {
                result_baran.setText(no_zakat);
                result_baran.setTextSize(10);
            }else if(v>=40 && v<=120) {
                result_baran.setText("1 "+a_baran);
                result_baran.setTextSize(10);
            }else if(v>=121 && v<=200) {
                result_baran.setText("2 "+a_baran);
                result_baran.setTextSize(10);
            }else if(v>=201 && v<=399) {
                result_baran.setText("3 "+a_baran);
                result_baran.setTextSize(10);
            }else if(v>=400 && v<=499) {
                result_baran.setText("4 "+a_baran);
                result_baran.setTextSize(10);
            }else if(v>=500) {
                int r = v/100;
                result_baran.setText(r+" "+a_baran);
                result_baran.setTextSize(10);
            }
            layout_baran.setVisibility(View.VISIBLE);
        }else layout_baran.setVisibility(View.GONE);

        //LOSHAD ***************************
        if(loshad.getText().length()>0  && loshad_cena.getText().length()>0){
            float l = Float.valueOf(loshad.getText().toString()) * Float.valueOf(loshad_cena.getText().toString()) ;
            if(l>=1112735) {
                float r = (float) (l*0.025);
                result_loshad.setText(r + " KZT");
            }else {
                result_loshad.setText(no_zakat);
                result_loshad.setTextSize(10);
            }
            layout_loshad.setVisibility(View.VISIBLE);
        }else layout_loshad.setVisibility(View.GONE);

        //KRC ***************************
        if(krc.getText().length()>0){
            int v = Integer.valueOf(krc.getText().toString());
            if(v<30) {
                result_krc.setText(no_zakat);
                result_krc.setTextSize(10);
            }else if(v>=30 && v<=39) {
                result_krc.setText(krc30);
                result_krc.setTextSize(10);
            }else if(v>=40 && v<=59) {
                result_krc.setText(krc40);
                result_krc.setTextSize(10);
            }else if(v>=60 && v<=69) {
                result_krc.setText(krc60);
                result_krc.setTextSize(10);
            }else if(v>=70 && v<=79) {
                result_krc.setText(krc70);
                result_krc.setTextSize(10);
            }else if(v>=80 && v<=89) {
                result_krc.setText(krc80);
                result_krc.setTextSize(10);
            }else if(v>=90 && v<=99) {
                result_krc.setText(krc90);
                result_krc.setTextSize(10);
            }else if(v>=100 && v<=119) {
                result_krc.setText(krc100);
                result_krc.setTextSize(10);
            } else if(v>=120) {
                result_krc.setText(krc120);
                result_krc.setTextSize(10);
            }
            layout_krc.setVisibility(View.VISIBLE);
        }else layout_krc.setVisibility(View.GONE);


        //DNEJNI ***************************
        if(predmet.getText().length()>0){
            if(Float.valueOf(predmet.getText().toString())>=1112735) {
                float r = (float) (Float.valueOf(predmet.getText().toString())*0.025);
                result_predmet.setText(r + " KZT");
            }else {
                result_predmet.setText(no_zakat);
                result_predmet.setTextSize(10);
            }
            layout_predmet.setVisibility(View.VISIBLE);
        }else layout_predmet.setVisibility(View.GONE);

        save_btn.setVisibility(View.VISIBLE);
    }

    @OnClick(R.id.guide)
    void GuideClick(){
        final String vidoeId = YouTubeUrlParser.getVideoId("https://www.youtube.com/watch?v=MWE7HuRfFsE&feature=youtu.be");
        Intent intent = new Intent(CalculateZakat.this, YouTubePlayerActivity.class);
        intent.putExtra(YouTubePlayerActivity.EXTRA_VIDEO_ID, vidoeId);
        intent.putExtra(YouTubePlayerActivity.EXTRA_PLAYER_STYLE, YouTubePlayer.PlayerStyle.DEFAULT);
        intent.putExtra(YouTubePlayerActivity.EXTRA_ORIENTATION, Orientation.AUTO);
        intent.putExtra(YouTubePlayerActivity.EXTRA_SHOW_AUDIO_UI, true);
        intent.putExtra(YouTubePlayerActivity.EXTRA_HANDLE_ERROR, true);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);

    }

}