package com.bugingroup.namazmuftiyat;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.TextView;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Nurik on 12.04.2017.
 */

public class HelpApp extends AppCompatActivity {

    @BindView(R.id.button1)
    TextView btn1;

    @BindString(R.string.help)
    String help;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help_app);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(help);

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    @OnClick(R.id.button1)
    void onBtn1Click(){
        startActivity(new Intent(HelpApp.this,PayHelp.class));
    }

    @OnClick(R.id.button2)
    void onBtn2Click(){
        startActivity(new Intent(HelpApp.this,About.class));
    }




}
