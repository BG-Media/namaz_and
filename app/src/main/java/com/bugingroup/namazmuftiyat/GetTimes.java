package com.bugingroup.namazmuftiyat;

/**
 * Created by Nurik on 06.11.2017.
 */

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bugingroup.namazmuftiyat.Alarm.Alarm;
import com.bugingroup.namazmuftiyat.Alarm.AlarmListAdapter;
import com.bugingroup.namazmuftiyat.Widget.NamazWidget;
import com.bugingroup.namazmuftiyat.utils.CalculatePrayTime;
import com.bugingroup.namazmuftiyat.utils.DatabaseHelper;
import com.bugingroup.namazmuftiyat.utils.Debt;
import com.bugingroup.namazmuftiyat.utils.Requests;
import com.bugingroup.namazmuftiyat.utils.TrackGPS;
import com.bugingroup.namazmuftiyat.utils.volley.AppController;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import butterknife.BindString;

import static com.bugingroup.namazmuftiyat.SplashScreen.PREFS_NAME;

public class GetTimes extends ContextWrapper

{


    Requests r = new Requests();

    AlertDialog scheduleBuilder;

    String[] permissions = {Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION};
    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;


    public static ArrayList<HashMap<String,Object>> mosquesList = new ArrayList<>();
    public static ArrayList<HashMap<String,String>> scheduleList = new ArrayList<>();

    public static String whatPage = "";
    public static int isMen = 0;


    private TrackGPS gps;
    public static double latitude=43.244011;
    public static double longitude=76.911454;
    public static int METHOD=8,ASRM=1,HIGHET=3,M1=0,M2=0,M3=0,M4=0,M5=0,M6=0;

    public static String currentCityName = "Алматы";


    public static int[] addingTimeArr = new int[]{0,0,0,0,0,0,0};
    ConnectivityStatus connectivityStatus = new ConnectivityStatus(this);

    @BindString(R.string.month) String month;
    @BindString(R.string.year) String year;

    @BindString(R.string.gps_title) String gps_title;
    @BindString(R.string.gps_message) String gps_message;

    String[] scheduleArr = new String[]{month,year};


    public static String lang;
    SharedPreferences settings;
    Boolean debt,is_gps=false;

    int YEAR,MONTH,DAY,dd,k=100;
    ArrayList<HashMap<String,String>> namaztimeList ;


    AlarmListAdapter mAlarmListAdapter;

    int [] alarm_of_onn;
    int [] debt_count;
    int [] debt_notifi;
    int [] debt_repeat;
    DatabaseHelper db;
    //@BindArray(R.array.namaz_name)
    String [] namaz_name;

   // public static double timezone;

    Context context;

    public GetTimes(Context base) {
        super(base);
        context = base;

    }
    public void GetPrayTimes(){
        permissionsMain();
        db = new DatabaseHelper(getApplicationContext());
        namaz_name = getResources().getStringArray(R.array.namaz_name);

        settings = getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        lang = settings.getString("language","kk");
        Log.d("languagelanguage",lang);
        METHOD = settings.getInt("METHOD",8);
        ASRM = settings.getInt("ASRM",1);
        HIGHET = settings.getInt("HIGHET",3);
        M1 = settings.getInt("M1",0);
        M2 = settings.getInt("M2",0);
        M3 = settings.getInt("M3",0);
        M4 = settings.getInt("M4",0);
        M5 = settings.getInt("M5",0);
        M6 = settings.getInt("M6",0);


        debt = settings.getBoolean("debt",false);
        gps = new TrackGPS(getBaseContext());
        mAlarmListAdapter =  new AlarmListAdapter(getBaseContext());


        try {
            Intent updateWidget = new Intent(getBaseContext(), NamazWidget.class); // Widget.class is your widget class
            updateWidget.setAction("update_widget");
            PendingIntent pending = PendingIntent.getBroadcast(getBaseContext(), 0, updateWidget, PendingIntent.FLAG_CANCEL_CURRENT);
            pending.send();
        } catch (PendingIntent.CanceledException e) {
            e.printStackTrace();
        }

        alarm_of_onn = new int[]{settings.getInt("fajir_select", 3), settings.getInt("voshod_select", 3),settings.getInt("zuhr_select",3),settings.getInt("asr_select",3),3,
                settings.getInt("magrib_select",3),settings.getInt("isha_select",3)};

        debt_notifi = new int[]{settings.getInt("notifi_fajir", 0), 0,settings.getInt("notifi_zuhr",0),settings.getInt("notifi_asr",0),0,
                settings.getInt("notifi_magrib",0),settings.getInt("notifi_isha",0)};

        debt_repeat = new int[]{settings.getInt("pof_fajir", 0), 0,settings.getInt("pof_zuhr",0),settings.getInt("pof_asr",0),0,
                settings.getInt("pof_magrib",0),settings.getInt("pof_isha",0)};


        is_gps =   settings.getBoolean("is_gps",false);
        List<Debt> allToDos = db.getAlldebt();
        if(allToDos.size()>0) {
            Debt debt = allToDos.get(0);
            debt_count = new int[]{debt.getFajr_count(), 0, debt.getZuhr_count(), debt.getAsr_count(), 0, debt.getMagrib_count(), debt.getIsha_count()};
        } else {
            debt_count = new int[]{0, 0, 0, 0, 0, 0, 0};

        }


        SharedPreferences.Editor editor = settings.edit();
        currentCityName = settings.getString("city", "Алматы");
        if(is_gps) {
            if (gps.canGetLocation()) {
                longitude = gps.getLongitude();
                latitude = gps.getLatitude();
                Log.d("LLLLL22", longitude + "*" + latitude);
            } else {
                if (settings.getBoolean("gps", true)) {
                    gps.showSettingsAlert(gps_title, gps_message);
                    editor.putBoolean("gps", false);
                }
                String coords = settings.getString("coords", "43.244011,76.911454");
                String[] separated = coords.split(",");
                Log.d("LLLLL", separated[1] + "*" + separated[0]);
                longitude = Double.valueOf(separated[1].trim()).doubleValue();
                latitude = Double.valueOf(separated[0].trim()).doubleValue();
                Log.d("LLLLL22", longitude + "*" + latitude);

            }


            Geocoder gcd = new Geocoder(getApplicationContext(), Locale.getDefault());
            List<Address> addresses = null;
            try {
                addresses = gcd.getFromLocation(latitude, longitude, 1);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (addresses != null) {
                if (addresses.size() > 0) {
                    Log.d("testaddress", (addresses.get(0).getLocality()));
                    currentCityName = addresses.get(0).getLocality();
                    editor.putString("city",currentCityName);
                }
            }
        }else {
            String coords = settings.getString("coords", "43.244011,76.911454");
            Log.d("LLLLLrrrr", coords);
            String[] separated = coords.split(",");
            Log.d("LLLLLqqqq", separated[1] + "*" + separated[0]);
            longitude = Double.valueOf(separated[1].trim()).doubleValue();
            latitude = Double.valueOf(separated[0].trim()).doubleValue();
            Log.d("LLLLL22qqqq", longitude + "*" + latitude);
        }

        editor.putString("city_id","0");
        editor.putString("coords", latitude+","+longitude);
        editor.commit();

        Calendar cal = Calendar.getInstance();
        Date now = new Date();
        cal.setTime(now);

        YEAR = cal.get(Calendar.YEAR);
        MONTH = cal.get(Calendar.MONTH)+1;
        DAY = cal.get(Calendar.DATE);
        dd = cal.get(Calendar.DATE);
        Log.d("MONTHaaa",YEAR+"*"+MONTH+"*"+DAY);

        GetTimeZone();

//        if(connectivityStatus.isInternetOn()) {
//            r.getMosques(this,latitude,longitude);
//        }

    }



    // onClick method for the button
    public void getTime(int y, int m ,int d) {

        namaztimeList = new ArrayList<>();

        CalculatePrayTime prayers = new CalculatePrayTime();
        prayers.setTimeFormat(prayers.Time24);
        prayers.setCalcMethod(METHOD);
        prayers.setAsrJuristic(ASRM);
        prayers.setAdjustHighLats(HIGHET);

        ArrayList<String> timeNames = new ArrayList<String>();
        timeNames.add(namaz_name[0]);
        timeNames.add(namaz_name[1]);
        timeNames.add(namaz_name[2]);
        timeNames.add(namaz_name[3]);
        timeNames.add(namaz_name[4]);
        timeNames.add(namaz_name[4]);
        timeNames.add(namaz_name[5]);
        prayers.setTimeNames(timeNames);

        int[] offsets =new int[]{M1, M2, M3, M4, 0, M5, M6};
        if(METHOD==8){
            if (latitude < 48) {
                offsets  = new int[]{M1, M2-3, M3+3, M4+3,0, M5+3, M6};
            } else {
                offsets  = new int[]{M1, M2-5, M3+5, M4+5,0, M5+5, M6};
            }
        }

        prayers.tune(offsets);

        SimpleDateFormat df = new SimpleDateFormat("HH:mm");
        Calendar calculateCalendar = Calendar.getInstance();

        Calendar cal = Calendar.getInstance();
        Date now = new Date();
        cal.setTime(now);


        Log.d("HHH", y + "" + m + "" + d + "" + latitude + "" + longitude + "" + MainActivity.timezone);
        ArrayList prayerTimes = prayers.getDatePrayerTimes(y, m, d, latitude, longitude, MainActivity.timezone);
        ArrayList prayerNames = prayers.getTimeNames();
        mAlarmListAdapter.clear();
        int j=0;
        for (int i = 0; i < prayerTimes.size(); i++) {
            if(i!=4){
                Log.d("SSSS", prayerTimes.size() + "*" + j + "" + prayerTimes.get(j));
                try {
                    Date dateForCalculation = df.parse(prayerTimes.get(i) + "");
                    calculateCalendar.setTime(dateForCalculation);
                    calculateCalendar.add(Calendar.MINUTE, addingTimeArr[i]);
                    String newTime = df.format(calculateCalendar.getTime());

                    HashMap<String, String> hashMap = new HashMap<>();
                    hashMap.put("name", prayerNames.get(i) + "");
                    hashMap.put("time", newTime);
                    namaztimeList.add(hashMap);


                    System.out.println("---------------------------------888888888-----------------------------------");
                    System.out.println("---"+i+"????"+y+"---"+ (m-1)+"---"+d+"---"+ calculateCalendar.getTime().getHours()+"---"+ calculateCalendar.getTime().getMinutes());
                    GregorianCalendar mCalendar;
                    mCalendar = new GregorianCalendar(y, m-1, d, calculateCalendar.getTime().getHours(), calculateCalendar.getTime().getMinutes());
                    if(mCalendar.getTimeInMillis() > System.currentTimeMillis() && alarm_of_onn[i]!=3) {
                        Alarm mAlarm = new Alarm(GetTimes.this);
                        mAlarm.setId(i);
                        mAlarm.setType(i);
                        mAlarm.setRepeat_t(0);
                        mAlarm.setIs_debt(false);
                        mAlarm.setTitle("" + prayerNames.get(i));
                        mAlarm.setDate(mCalendar.getTimeInMillis());
                        mAlarm.setDays(0);
                        mAlarm.setEnabled(true);
                        mAlarm.setOccurence(0);
                        mAlarmListAdapter.add(mAlarm);
                    }

                    if(debt_notifi[i]!=0) {
                        GregorianCalendar dCalendar;
                        dCalendar = new GregorianCalendar(y, m - 1, d, calculateCalendar.getTime().getHours(), calculateCalendar.getTime().getMinutes() + debt_notifi[i]);

                        if (dCalendar.getTimeInMillis() > System.currentTimeMillis() && debt_count[i] > 0) {
                            Alarm mAlarm = new Alarm(GetTimes.this);
                            mAlarm.setId(i);
                            mAlarm.setType(i);
                            mAlarm.setRepeat_t(debt_repeat[i]);
                            mAlarm.setIs_debt(true);
                            mAlarm.setTitle(getResources().getString(R.string.notification_debt) + " " + prayerNames.get(i));
                            mAlarm.setDate(dCalendar.getTimeInMillis());
                            mAlarm.setDays(0);
                            mAlarm.setEnabled(true);
                            mAlarm.setOccurence(0);
                            mAlarmListAdapter.add(mAlarm);

                        }
                    }



                    int hours = new Time(System.currentTimeMillis()).getHours();
                    int minute = new Time(System.currentTimeMillis()).getMinutes();
                    String[] separated = newTime.split(":");
                    int h = Integer.valueOf(separated[0]);
                    int M = Integer.valueOf(separated[1]);

                    Log.d("HH", hours + "_" + h + "M:" + minute + "_" + M);
                    if (k == 100 && dd == d) {
                        if (h >= hours && M > minute) {
                            k = j;
                        } else if (h > hours && h != 0) {
                            k = j;
                        }
                    }

                } catch (ParseException e) {
                    e.printStackTrace();
                }
                j++;
            }

        }


    }
    public void permissionsMain() {
        for (int i = 0; i < permissions.length; i++) {
            if (ContextCompat.checkSelfPermission(this,
                    permissions[i])
                    != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) getBaseContext(),
                        permissions[i])) {
                } else {
                    ActivityCompat.requestPermissions((Activity) getBaseContext(),
                            new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION},
                            MY_PERMISSIONS_REQUEST_LOCATION);
                }
            }
        }
    }
//    public void alertSchedule(){
//        try{
//            scheduleBuilder = new AlertDialog.Builder(this).setTitle("Выберите метод")
//                    .setSingleChoiceItems(scheduleArr, 0, null)
//                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
//                        public void onClick(DialogInterface dialog, int whichButton) {
//                            dialog.dismiss();
//                            int selectedPosition = ((AlertDialog)dialog).getListView().getCheckedItemPosition();
//                            Intent in = new Intent(getApplicationContext(),Schedule.class);
//
//                            if(selectedPosition == 0){
//                                in.putExtra("period","month");
//                            }else{
//                                in.putExtra("period","year");
//                            }
//                            startActivity(in);
//                            // Do something useful withe the position of the selected radio button
//                        }
//                    }).setNegativeButton("Отмена", new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialogInterface, int i) {
//
//                        }
//                    })
//                    .show();
//
//        }catch (Exception e){
//
//        }
//    }








    private void GetTimeZone() {

//        timezone = (Calendar.getInstance().getTimeZone()
//                .getOffset(Calendar.getInstance().getTimeInMillis()))
//                / (1000 * 60 * 60);
//        TimeZone tz = TimeZone.getDefault();
//        Log.d("KKKKKKK", "TimeZone   "+tz.getDisplayName(false, TimeZone.SHORT)+" Timezon id :: " +tz.getID());
//
//        StringRequest jsonReq = new StringRequest (Request.Method.GET,
//                "http://api.geonames.org/timezoneJSON?lat="+latitude+"&lng="+longitude+"&username=adiletmaks",new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                Log.d("RRRRRR",response);
//                if(response!=null){
//                    try {
//                        JSONObject jsonObject = new JSONObject(response);
//                        MainActivity.timezone = jsonObject.getDouble("gmtOffset");
                        getTime(YEAR,MONTH,DAY);

//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                    }
//                }
//            }
//        }, new Response.ErrorListener() {
//
//            @Override
//            public void onErrorResponse(VolleyError error) {
//
//            }
//        }) {
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> headers = new HashMap<String, String>();
//                headers.put("Content-Type", "application/json");
//                return headers;
//            }
//
//        };
//        AppController.getInstance().addToRequestQueue(jsonReq);
    }



}