package com.bugingroup.namazmuftiyat.useful;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.bugingroup.namazmuftiyat.R;
import com.bugingroup.namazmuftiyat.bookTaharatTerminDestroy.SingleBook;
import com.bugingroup.namazmuftiyat.utils.Requests;
import com.bugingroup.namazmuftiyat.utils.volley.ImageVolleyRequest;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindString;
import butterknife.ButterKnife;

/**
 * Created by Nurik on 06.04.2017.
 */

public class UsefulMaterial  extends AppCompatActivity {


    ListView listview;
   public static ListAdapter usefulyAdapter;
   public static ArrayList<HashMap<String,String>> usefulyList = new ArrayList<>();
    Requests r = new Requests();
    ImageLoader imageLoader;

    @BindString(R.string.useful_materials)
    String useful_materials;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_common_list);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(useful_materials);
        listview = (ListView) findViewById(R.id.list);
        imageLoader = ImageVolleyRequest.getInstance(this)
                .getImageLoader();
        usefulyAdapter = new ListAdapter(UsefulMaterial.this,usefulyList);
        listview.setAdapter(usefulyAdapter);
        r.getUsefuly(this,usefulyList);
    }



    public class ListAdapter extends BaseAdapter {

        ArrayList<HashMap<String, String>> commentsList;
        Activity activity;

        public ListAdapter(Activity activity, ArrayList<HashMap<String, String>> commentsList) {
            this.activity = activity;
            this.commentsList = commentsList;
        }


        @Override
        public int getCount() {
            return commentsList.size();
        }

        @Override
        public Object getItem(int location) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }


        class ViewHolder {
            TextView title;
            TextView description;
            NetworkImageView image;

        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            final ViewHolder viewHolder;
            if (convertView == null) {
                LayoutInflater inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.item_usefuly, parent, false);
                viewHolder = new ViewHolder();

                viewHolder.title = (TextView) convertView.findViewById(R.id.title);
                viewHolder.description = (TextView) convertView.findViewById(R.id.description);
                viewHolder.image = (NetworkImageView) convertView.findViewById(R.id.image);

                convertView.setTag(viewHolder);

            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }


            viewHolder.title.setText(commentsList.get(position).get("title"));
            viewHolder.description.setText(Html.fromHtml(commentsList.get(position).get("description")));
            viewHolder.image.setImageUrl(commentsList.get(position).get("image"),imageLoader);



            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent inDesc = new Intent(activity,SingleBook.class);
                    inDesc.putExtra("position",position);
                    inDesc.putExtra("type","usefuly");
                    startActivity(inDesc);
                }
            });

            return convertView;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}

