package com.bugingroup.namazmuftiyat;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Nurik on 17.04.2017.
 */

public class ActivityRekvizit extends AppCompatActivity {



    @BindString(R.string.help)
    String help;

    @BindString(R.string.valid_empty)
    String valid_empty;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rekvizit);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(help);

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @OnClick(R.id.yes)
    void Yes(){
        final Dialog dialoga = new Dialog(ActivityRekvizit.this);
        dialoga.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialoga.setContentView(R.layout.activity_help_pay);
        dialoga.show();
        final EditText price = (EditText) dialoga.findViewById(R.id.price) ;
        Button pp = (Button) dialoga.findViewById(R.id.pay) ;
        pp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(price.getText().toString().length()>0) {
                    dialoga.dismiss();
                    startActivity(new Intent(ActivityRekvizit.this, ActivityPay.class).putExtra("price", price.getText().toString()));
                }
                else Toast.makeText(ActivityRekvizit.this, valid_empty, Toast.LENGTH_SHORT).show();
            }
        });
        finish();
    }

}
