package com.bugingroup.namazmuftiyat;

import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.bugingroup.namazmuftiyat.utils.volley.ImageVolleyRequest;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import static com.bugingroup.namazmuftiyat.MainActivity.latitude;
import static com.bugingroup.namazmuftiyat.MainActivity.longitude;
import static com.bugingroup.namazmuftiyat.MainActivity.mosquesList;

public class Mosques extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    TextView nameText;
    TextView addressText;
    NetworkImageView mosquesImage;
    Marker[] markersArray;
    ImageLoader imageLoader;
    LinearLayout scrollableLayout;
    int count = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_mosques);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

    }


    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;
        nameText = (TextView) findViewById(R.id.mosques_name);
        addressText = (TextView) findViewById(R.id.mosques_address);
        mosquesImage = (NetworkImageView) findViewById(R.id.mosques_image);
        scrollableLayout = (LinearLayout) findViewById(R.id.mosques_scrollable_layout);
        scrollableLayout.setVisibility(View.GONE);

        LatLng Almaty = new LatLng(latitude, longitude);
        markersArray = new Marker[mosquesList.size()];
        mMap.setMyLocationEnabled(true);
        for(int i = 0;i<mosquesList.size();i++){
            LatLng latLng = new LatLng(Double.parseDouble(mosquesList.get(i).get("lat").toString()),
                    Double.parseDouble(mosquesList.get(i).get("long").toString()));
            mMap.addMarker(new MarkerOptions().position(latLng)
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.maps_flags))
                    .title(mosquesList.get(i).get("title").toString())
                    .snippet(mosquesList.get(i).get("address").toString()));
        }

        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(Almaty,12));



        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                scrollableLayout.setVisibility(View.VISIBLE);
                nameText.setText(marker.getTitle());
                addressText.setText(marker.getSnippet());
                for(int i = 0;i<mosquesList.size();i++){
                    if(marker.getTitle().equals(mosquesList.get(i).get("title"))){
                        loadImage(mosquesList.get(i).get("image").toString());
                    }
                }
                return false;
            }
        });

        scrollableLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                scrollableLayout.setVisibility(View.GONE);
            }
        });


    }

    private void loadImage(String url){

        if(url.equals("")){
            return;
        }

        Log.d("UUUUUuuuuuu",url);

        imageLoader = ImageVolleyRequest.getInstance(this.getApplicationContext())
                .getImageLoader();
        imageLoader.get(url, ImageLoader.getImageListener(mosquesImage,
                R.mipmap.ic_launcher, android.R.drawable
                        .ic_dialog_alert));
        mosquesImage.setImageUrl(url, imageLoader);
    }
}
