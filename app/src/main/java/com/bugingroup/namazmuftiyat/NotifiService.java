package com.bugingroup.namazmuftiyat;

import android.app.IntentService;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

/**
 * Created by nurbaqyt on 01.07.17.
 */

public class NotifiService extends IntentService {
    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     *
     * @param name Used to name the worker thread, important only for debugging.
     */
    public NotifiService(String name) {
        super(name);
    }

    @Override
    protected void onHandleIntent(Intent workIntent) {
        // Gets data from the incoming Intent
        long endTime = System.currentTimeMillis() + 5*1000;
        while (System.currentTimeMillis() < endTime) {
            synchronized (this) {
                try {
                    System.out.println("-----------------0----------------------");

                    wait(endTime - System.currentTimeMillis());
                } catch (Exception e) {
                }
            }
        }
        System.out.println("--------------------1-------------------");

    }
}