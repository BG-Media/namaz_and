package com.bugingroup.namazmuftiyat;

import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.bugingroup.namazmuftiyat.utils.AlarmReceiver;
import com.bugingroup.namazmuftiyat.utils.TrackGPS;

import java.io.IOException;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import butterknife.BindString;

import static com.bugingroup.namazmuftiyat.MainActivity.latitude;
import static com.bugingroup.namazmuftiyat.MainActivity.longitude;

public class TimeSettings extends AppCompatActivity {

    RelativeLayout cityLayout;
    RelativeLayout languageLayout;
    RelativeLayout asrLayout;
    RelativeLayout methodsLayout;
    RelativeLayout manuallyLayout;
    RelativeLayout correctionLayout;

    Switch notificationSwitch;

    ImageButton locationButton;

    private TrackGPS gps;

    AlertDialog asrBuilder;

    AlarmManager alarmManager;
    private PendingIntent pending_intent;
    private TimePicker alarmTimePicker;
    private static MainActivity inst;
    private TextView alarmTextView;
    private AlarmReceiver alarm;
    private Context context;
    Spinner spinner;
    int richard_quote = 0;


    public static String whatSettings = "";
    Intent intent = getIntent();
    String[] titlesSettingsArr = new String[]{"Город","Язык","Азаны"};
    String[] titlesTimeArr = new String[]{"Расчет Асра","Организация времени молитвы",
            "Организация времени вручную","Коррекция"};
    String[] titlesAsrArr = new String[]{"Shafii","Hanafi"};



    @BindString(R.string.gps_title) String gps_title;
    @BindString(R.string.gps_message) String gps_message;
    @Override
    protected void onCreate (Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_time_settings);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        cityLayout = (RelativeLayout) findViewById(R.id.settings_city_layout);
        languageLayout = (RelativeLayout) findViewById(R.id.settings_language_layout);
        asrLayout = (RelativeLayout) findViewById(R.id.settings_asr_layout);
        methodsLayout = (RelativeLayout) findViewById(R.id.settings_methods_layout);
        manuallyLayout = (RelativeLayout) findViewById(R.id.settings_manually_layout);
        correctionLayout = (RelativeLayout) findViewById(R.id.settings_correction_layout);
        locationButton = (ImageButton) findViewById(R.id.location_button);
        notificationSwitch = (Switch) findViewById(R.id.notification_switch);

        final Calendar calendar = Calendar.getInstance();
        final Intent myIntent = new Intent(TimeSettings.this, AlarmReceiver.class);

//        notificationSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton compoundButton, boolean checked) {
//                Log.d("b",checked+"");
//                if(checked){
//                    calendar.set(Calendar.HOUR_OF_DAY, alarmTimePicker.getHour());
//                    calendar.set(Calendar.MINUTE, alarmTimePicker.getMinute());
//
//                    final int hour = alarmTimePicker.getHour();
//                    final int minute = alarmTimePicker.getMinute();;
//
//                    String minute_string = String.valueOf(minute);
//                    String hour_string = String.valueOf(hour);
//
//                    if (minute < 10) {
//                        minute_string = "0" + String.valueOf(minute);
//                    }
//
//                    if (hour > 12) {
//                        hour_string = String.valueOf(hour - 12) ;
//                    }
//
//                    myIntent.putExtra("extra", "yes");
//                    myIntent.putExtra("quote id", String.valueOf(richard_quote));
//                    pending_intent = PendingIntent.getBroadcast(TimeSettings.this, 0, myIntent, PendingIntent.FLAG_UPDATE_CURRENT);
//
//                    alarmManager.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pending_intent);
//
////                    setAlarmText("Alarm set to " + hour_string + ":" + minute_string);
//                }
//            }
//        });
     //   notificationSwitch.setChecked(true);

        gps = new TrackGPS(TimeSettings.this);
        locationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(gps.canGetLocation()){

                    longitude = gps.getLongitude();
                    latitude = gps .getLatitude();

                    Toast.makeText(getApplicationContext(),"Longitude:"+Double.toString(longitude)+"\nLatitude:"+Double.toString(latitude),Toast.LENGTH_SHORT).show();
                }
                else
                {
                    gps.showSettingsAlert(gps_title,gps_message);
                }

                Geocoder gcd = new Geocoder(getApplicationContext(), Locale.getDefault());
                List<Address> addresses = null;
                try {
                    addresses = gcd.getFromLocation(latitude, longitude, 1);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                if (addresses.size() > 0)
                {
                    Log.d("testaddress",(addresses.get(0).getLocality()));
                }

                else
                {
                    // do your staff
                }
            }
        });

        cityLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                whatSettings = "city";
                Intent in = new Intent(getApplicationContext(),ChooseSettings.class);
                startActivity(in);
            }
        });

        languageLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                whatSettings = "chooseAzan";

                Intent in = new Intent(getApplicationContext(),ChooseAzan.class);
                startActivity(in);
            }
        });

        asrLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                whatSettings = "calculateAsr";
                alertAsr();
            }
        });

        methodsLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                whatSettings = "methods";
                Intent in = new Intent(getApplicationContext(),ChooseAzan.class);
                startActivity(in);
            }
        });

        manuallyLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                whatSettings = "degreeTime";
                Intent in = new Intent(getApplicationContext(),ChooseAzan.class);
                startActivity(in);
            }
        });

        correctionLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                whatSettings = "chooseAzan";
                Intent in = new Intent(getApplicationContext(),ChooseAzan.class);
                startActivity(in);
            }
        });

    }

    private void onClick(String tag){
        whatSettings = tag;
        Intent in = new Intent(getApplicationContext(),ChooseAzan.class);
        startActivity(in);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onBackPressed() {
        whatSettings = "";
        super.onBackPressed();
    }

    public void alertAsr(){
        try{
            asrBuilder = new AlertDialog.Builder(this).setTitle("Выберите метод")
                    .setSingleChoiceItems(titlesAsrArr, 0, null)
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            dialog.dismiss();
                            int selectedPosition = ((AlertDialog)dialog).getListView().getCheckedItemPosition();
//                            if(selectedPosition == 0){
//                                asrCalculation = 0;
//                            }else{
//                                asrCalculation = 1;
//                            }
                            // Do something useful withe the position of the selected radio button
                        }
                    }).setNegativeButton("Отмена", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

                        }
                    })
                    .show();

        }catch (Exception e){

        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
