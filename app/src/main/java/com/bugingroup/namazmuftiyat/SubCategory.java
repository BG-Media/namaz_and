package com.bugingroup.namazmuftiyat;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.SectionIndexer;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.bugingroup.namazmuftiyat.bookTaharatTerminDestroy.SingleBook;
import com.bugingroup.namazmuftiyat.utils.Requests;
import com.bugingroup.namazmuftiyat.utils.volley.AppController;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindString;
import butterknife.ButterKnife;

/**
 * Created by Nurik on 30.05.2017.
 */

public class SubCategory extends AppCompatActivity {

    public static ArrayList<HashMap<String,String>> subList = new ArrayList<>();
    Requests r = new Requests();

    @BindString(R.string.sura) String sura;
    @BindString(R.string.dua) String dua;
    ListView bookListView;
    public static SimpleAdapter bookAdapter;
    String[] first;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_common_list);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getIntent().getStringExtra("title"));


        bookListView = (ListView) findViewById(R.id.list);


        if(getIntent().getStringExtra("title").equals(sura)){
            first = getResources().getStringArray(R.array.arabic);
            bookListView.setAdapter(new MainAdapter(SubCategory.this,first));
        }else if(getIntent().getStringExtra("title").equals(dua)) {
            bookAdapter = new SimpleAdapter(this,subList,R.layout.sub_item,
                    new String[]{"title"},new int[]{R.id.common_item_text});
            bookListView.setAdapter(bookAdapter);
            getDuaFromServer();
        }else {
            bookAdapter = new SimpleAdapter(this,subList,R.layout.sub_item,
                    new String[]{"title"},new int[]{R.id.common_item_text});
            bookListView.setAdapter(bookAdapter);

            r.getNamazBook(SubCategory.this, bookAdapter, subList, getIntent().getStringExtra("id"));
        }


        bookListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if(getIntent().getStringExtra("title").equals(sura)) {
                    Intent intent = new Intent(SubCategory.this, Sura.class);
                    intent.putExtra("pos", i);
                    intent.putExtra("title", first[i]);
                    startActivity(intent);
                }else if(getIntent().getStringExtra("title").equals(dua)) {
                    Intent intent = new Intent(SubCategory.this, Dua.class);
                    intent.putExtra("position", subList.get(i).get("id"));
                    intent.putExtra("name", subList.get(i).get("title"));
                    startActivity(intent);
                }else {
                    Intent inDesc = new Intent(SubCategory.this, SingleBook.class);
                    inDesc.putExtra("position", i);
                    inDesc.putExtra("type", "subList");
                    startActivity(inDesc);

                }
            }
        });

    }

    private void getDuaFromServer() {
        subList.clear();
        StringRequest jsonReq = new StringRequest (Request.Method.GET,
                "http://195.210.47.17/hidayat/api/v1/dugalar",new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                if(response!=null){
                    Log.d("ddddd",response);

                    try {
                        String newStr = URLDecoder.decode(URLEncoder.encode(response, "iso8859-1"),"UTF-8");
                        JSONArray jsonArray = new JSONArray(newStr);
                        for (int i=0;i<jsonArray.length();i++) {
                            JSONObject jsonObject = (JSONObject) jsonArray.get(i);
                            HashMap<String, String> hashMap = new HashMap<>();
                            hashMap.put("id", jsonObject.getString("id"));
                            hashMap.put("title", jsonObject.getString("name"));
                            subList.add(hashMap);
                        }
                        bookAdapter.notifyDataSetChanged();

                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                Toast.makeText(SubCategory.this,SubCategory.this.getResources().getString(R.string.volley_error),Toast.LENGTH_SHORT).show();

                VolleyLog.d("Error: " + error.getMessage());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json");
                return headers;
            }
        };
        AppController.getInstance().addToRequestQueue(jsonReq);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public class MainAdapter extends BaseAdapter implements SectionIndexer {
        Context context;
        String[] first;
        Typeface scheherazade;
        String[] second;
        String[] sections = new String[114];

        public MainAdapter(Context c,String[] first) {
            this.context = c;
            this.first = first;
            this.second = c.getResources().getStringArray(R.array.kazakh);
            for (int i = 0; i < 114; i++) {
                this.sections[i] = String.valueOf(i + 1);
            }
            this.scheherazade = Typeface.createFromAsset(c.getAssets(), "Scheherazade.ttf");
        }

        public int getCount() {
            return this.first.length;
        }

        public String getItem(int pos) {
            return null;
        }

        public long getItemId(int pos) {
            return (long) pos;
        }

        public View getView(int pos, View root, ViewGroup parent) {
            if (root == null) {
                root = LayoutInflater.from(this.context).inflate(R.layout.sura_item, parent, false);
            }
            TextView firstv = (TextView) root.findViewById(R.id.first);
            TextView secondv = (TextView) root.findViewById(R.id.second);
            ((TextView) root.findViewById(R.id.num)).setText(String.valueOf(pos + 1));
            firstv.setText(this.first[pos]);
            firstv.setTypeface(this.scheherazade);
            secondv.setText(this.second[pos]);
            return root;
        }

        public int getPositionForSection(int sectionIndex) {
            return sectionIndex;
        }

        public int getSectionForPosition(int position) {
            return position;
        }

        public Object[] getSections() {
            return this.sections;
        }
    }


}
