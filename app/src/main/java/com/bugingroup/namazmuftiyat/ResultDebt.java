package com.bugingroup.namazmuftiyat;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bugingroup.namazmuftiyat.utils.DatabaseHelper;
import com.bugingroup.namazmuftiyat.utils.Debt;
import com.bugingroup.namazmuftiyat.utils.Zakat;

import java.util.List;

import butterknife.BindArray;
import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;

import static com.bugingroup.namazmuftiyat.SplashScreen.PREFS_NAME;

/**
 * Created by Nurik on 10.04.2017.
 */

public class ResultDebt extends AppCompatActivity {

    @BindView(R.id.fazhir)
    TextView fazhir;
    @BindView(R.id.zuhyr)
    TextView zuhyr;
    @BindView(R.id.asyr)
    TextView asyr;
    @BindView(R.id.magrib)
    TextView magrib;
    @BindView(R.id.isha)
    TextView isha;

    @BindView(R.id.f_et)
    EditText f_et;
    @BindView(R.id.z_et)
    EditText z_et;
    @BindView(R.id.a_et)
    EditText a_et;
    @BindView(R.id.m_et)
    EditText m_et;
    @BindView(R.id.i_et)
    EditText i_et;

    int f_Day,z_Day,a_Day,m_Day,i_Day ;
    DatabaseHelper db;

    @BindString(R.string.debt_recovery)
    String debt_recovery;

    Boolean has_debt = false;

    @BindView(R.id.notifi_fajir) Spinner notifi_fajir;
    @BindView(R.id.notifi_zuhr) Spinner notifi_zuhr;
    @BindView(R.id.notifi_asr) Spinner notifi_asr;
    @BindView(R.id.notifi_magrib) Spinner notifi_magrib;
    @BindView(R.id.notifi_isha) Spinner notifi_isha;
    @BindView(R.id.pof_fajir) Spinner pof_fajir;
    @BindView(R.id.pof_zuhr) Spinner pof_zuhr;
    @BindView(R.id.pof_asr) Spinner pof_asr;
    @BindView(R.id.pof_magrib) Spinner pof_magrib;
    @BindView(R.id.pof_isha) Spinner pof_isha;

    @BindArray(R.array.minute)
    String [] minute;


    SharedPreferences settings;
    SharedPreferences.Editor editor;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_debt_result);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(debt_recovery);

        settings = getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        editor = settings.edit();

        db = new DatabaseHelper(getApplicationContext());
        Intent i = getIntent();
        has_debt  = i.getBooleanExtra("debt",false);
        if(has_debt){
            List<Debt> allToDos = db.getAlldebt();
            Debt debt = allToDos.get(0);
            f_et.setText(debt.getFajr_count()+"");
            fazhir.setText(debt.getFajr_result()+ "");
            z_et.setText(debt.getZuhr_count()+"");
            zuhyr.setText(debt.getZuhr_result() + "");
            a_et.setText(debt.getAsr_count()+"");
            asyr.setText(debt.getAsr_result() + "");
            m_et.setText(debt.getMagrib_count()+"");
            magrib.setText(debt.getMagrib_result() + "");
            i_et.setText(debt.getIsha_count()+"");
            isha.setText(debt.getIsha_result() + "");

            f_Day= Integer.valueOf(fazhir.getText().toString()) * Integer.valueOf(f_et.getText().toString());
            z_Day= Integer.valueOf(zuhyr.getText().toString()) * Integer.valueOf(z_et.getText().toString());
            a_Day= Integer.valueOf(asyr.getText().toString()) * Integer.valueOf(a_et.getText().toString());
            m_Day= Integer.valueOf(magrib.getText().toString()) * Integer.valueOf(m_et.getText().toString());
            i_Day= Integer.valueOf(isha.getText().toString()) * Integer.valueOf(i_et.getText().toString());
        }else {
            f_Day= getIntent().getIntExtra("day", 0);
            z_Day= getIntent().getIntExtra("day", 0);
            a_Day= getIntent().getIntExtra("day", 0);
            m_Day= getIntent().getIntExtra("day", 0);
            i_Day= getIntent().getIntExtra("day", 0);
            fazhir.setText(f_Day + "");
            zuhyr.setText(z_Day + "");
            asyr.setText(a_Day + "");
            magrib.setText(m_Day + "");
            isha.setText(i_Day + "");
        }


        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, minute);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        notifi_fajir.setAdapter(adapter);
        notifi_zuhr.setAdapter(adapter);
        notifi_asr.setAdapter(adapter);
        notifi_magrib.setAdapter(adapter);
        notifi_isha.setAdapter(adapter);
        pof_fajir.setAdapter(adapter);
        pof_zuhr.setAdapter(adapter);
        pof_asr.setAdapter(adapter);
        pof_magrib.setAdapter(adapter);
        pof_isha.setAdapter(adapter);

        notifi_fajir.setSelection(settings.getInt("notifi_fajir",15));
        notifi_zuhr.setSelection(settings.getInt("notifi_zuhr",15));
        notifi_asr.setSelection(settings.getInt("notifi_asr",15));
        notifi_magrib.setSelection(settings.getInt("notifi_magrib",15));
        notifi_isha.setSelection(settings.getInt("notifi_isha",15));
        pof_fajir.setSelection(settings.getInt("pof_fajir",5));
        pof_zuhr.setSelection(settings.getInt("pof_zuhr",5));
        pof_asr.setSelection(settings.getInt("pof_asr",5));
        pof_magrib.setSelection(settings.getInt("pof_magrib",5));
        pof_isha.setSelection(settings.getInt("pof_isha",5));

        notifi_fajir.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                editor.putInt("notifi_fajir",position);
            }
            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });



        notifi_zuhr.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                editor.putInt("notifi_zuhr",position);

            }
            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });



        notifi_asr.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                editor.putInt("notifi_asr",position);

            }
            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });



        notifi_magrib.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                editor.putInt("notifi_magrib",position);

            }
            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });



        notifi_isha.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                editor.putInt("notifi_isha",position);

            }
            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });



        pof_fajir.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                editor.putInt("pof_fajir",position);

            }
            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });



        pof_zuhr.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                editor.putInt("pof_zuhr",position);

            }
            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });



        pof_asr.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                editor.putInt("pof_asr",position);

            }
            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });



        pof_magrib.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                editor.putInt("pof_magrib",position);

            }
            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });



        pof_isha.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                editor.putInt("pof_isha",position);

            }
            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });












    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @OnTextChanged(R.id.f_et)
    protected void f_etonTextChanged(CharSequence text) {
        if(text.length()>0) {
            int count = Integer.valueOf(String.valueOf(text));
            int day = f_Day / count;
            fazhir.setText(day + "");
        }else {
            fazhir.setText(f_Day + "");
        }
    }

    @OnTextChanged(R.id.z_et)
    protected void z_etonTextChanged(CharSequence text) {
        if(text.length()>0) {
            int count = Integer.valueOf(String.valueOf(text));
            int day = z_Day / count;
            zuhyr.setText(day + "");
        }else {
            zuhyr.setText(z_Day + "");
        }
    }

    @OnTextChanged(R.id.a_et)
    protected void a_etonTextChanged(CharSequence text) {
        if(text.length()>0) {
            int count = Integer.valueOf(String.valueOf(text));
            int day = a_Day / count;
            asyr.setText(day + "");
        }else {
            asyr.setText(a_Day + "");
        }
    }

    @OnTextChanged(R.id.m_et)
    protected void m_etonTextChanged(CharSequence text) {
        if(text.length()>0) {
            int count = Integer.valueOf(String.valueOf(text));
            int day = m_Day / count;
            magrib.setText(day + "");
        }else {
            magrib.setText(m_Day + "");
        }
    }

    @OnTextChanged(R.id.i_et)
    protected void i_etonTextChanged(CharSequence text) {
        if(text.length()>0) {
            int count = Integer.valueOf(String.valueOf(text));
            int day = i_Day / count;
            isha.setText(day + "");
        }else {
            isha.setText(i_Day + "");
        }
    }

    @OnClick(R.id.clear_btn)
    void Clean_btn(){
        db.deleteDebt();
        editor.putBoolean("debt", false);
        editor.commit();
        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }


    @OnClick(R.id.save_btn)
    void save_btn(){
        int Fcount = Integer.valueOf(String.valueOf(f_et.getText().toString()));
        int Fres = Integer.valueOf(String.valueOf(fazhir.getText().toString()));
        int Zcount = Integer.valueOf(String.valueOf(z_et.getText().toString()));
        int Zres = Integer.valueOf(String.valueOf(zuhyr.getText().toString()));
        int Acount = Integer.valueOf(String.valueOf(a_et.getText().toString()));
        int Ares = Integer.valueOf(String.valueOf(asyr.getText().toString()));
        int Mcount = Integer.valueOf(String.valueOf(m_et.getText().toString()));
        int Mres = Integer.valueOf(String.valueOf(magrib.getText().toString()));
        int Icount = Integer.valueOf(String.valueOf(i_et.getText().toString()));
        int Ires = Integer.valueOf(String.valueOf(isha.getText().toString()));

        Debt debt = new Debt(Fcount,Fres,Zcount,Zres,Acount,Ares,Mcount,Mres,Icount,Ires);

        if(has_debt){
            db.updateDebt(debt);
        }else {
            db.createDebt(debt);
        }

        db.closeDB();
        editor.putBoolean("debt", true);
        editor.commit();

        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();

    }
}