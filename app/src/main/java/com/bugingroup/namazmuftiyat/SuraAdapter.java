package com.bugingroup.namazmuftiyat;

/**
 * Created by nurbaqyt on 04.08.17.
 */


import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.SectionIndexer;
import android.widget.TextView;
import android.widget.Toast;

public class SuraAdapter extends BaseAdapter implements SectionIndexer {
    Context context;
    String[] fifth;
    private String[] first;
    private String[] forth;
    private Typeface scheherazade;
    private String[] second;
    private String[] sections;
    private boolean show_first;
    private boolean show_forth;
    private boolean show_second;
    private boolean show_third;
    private int sura;
    private String[] third;

    public SuraAdapter(Context c, int pos) {
        this.context = c;
        this.sura = pos;
        this.scheherazade = Typeface.createFromAsset(c.getAssets(), "Scheherazade.ttf");
        int f = c.getResources().getIdentifier("arabic_" + (pos + 1), "array", c.getPackageName());
        int s = c.getResources().getIdentifier("kazakh_" + (pos + 1), "array", c.getPackageName());
        int t = c.getResources().getIdentifier("translit_" + (pos + 1), "array", c.getPackageName());
        int o = c.getResources().getIdentifier("russian_" + (pos + 1), "array", c.getPackageName());
        this.first = c.getResources().getStringArray(f);
        this.second = c.getResources().getStringArray(s);
        this.third = c.getResources().getStringArray(t);
        this.forth = c.getResources().getStringArray(o);
        if (!(this.first.length == this.second.length && this.first.length == this.third.length && this.first.length == this.forth.length)) {
            Toast.makeText(c, "Fail " + (pos + 1) + ": " + this.first.length + " " + this.second.length + " " + this.third.length + " " + this.forth.length, Toast.LENGTH_LONG).show();

        }
        this.sections = new String[this.first.length];
        int start = (this.sura == 8 || this.sura == 0) ? 1 : 0;
        int i = 0;
        while (i < this.sections.length) {
            int start2 = start + 1;
            this.sections[i] = String.valueOf(start);
            i++;
            start = start2;
        }
        SharedPreferences settings = this.context.getSharedPreferences(SplashScreen.PREFS_NAME, 0);
        this.show_first = settings.getBoolean("ch0", true);
        this.show_second = settings.getBoolean("ch1", true);
        this.show_third = settings.getBoolean("ch2", true);
        this.show_forth = settings.getBoolean("ch3", true);
    }

    public int getCount() {
        return this.first.length;
    }

    public String getItem(int pos) {
        return null;
    }

    public long getItemId(int pos) {
        return (long) pos;
    }

    public View getView(int pos, View root, ViewGroup parent) {
        root = LayoutInflater.from(this.context).inflate(R.layout.sura_item2, parent, false);
        TextView num = (TextView) root.findViewById(R.id.num);
        TextView firstv = (TextView) root.findViewById(R.id.first);
        TextView secondv = (TextView) root.findViewById(R.id.second);
        TextView thirdv = (TextView) root.findViewById(R.id.third);
        TextView forthv = (TextView) root.findViewById(R.id.forth);
        num.setTypeface(null, 0);
        firstv.setTypeface(this.scheherazade);
        secondv.setTypeface(null, 0);
        thirdv.setTypeface(null, 0);
        forthv.setTypeface(null, 0);
        firstv.setText(this.first[pos]);
        secondv.setText(this.second[pos]);
        thirdv.setText(this.third[pos]);
        forthv.setText(this.forth[pos]);
        if (this.show_first) {
            firstv.setPadding(0, 0, 0, 30);
        } else {
            firstv.setHeight(0);
        }
        if (this.show_second) {
            secondv.setPadding(0, 0, 0, 10);
        } else {
            secondv.setHeight(0);
        }
        if (this.show_third) {
            thirdv.setPadding(0, 0, 0, 10);
        } else {
            thirdv.setHeight(0);
        }
        if (this.show_forth) {
            forthv.setPadding(0, 0, 0, 10);
        } else {
            forthv.setHeight(0);
        }
        boolean[] shows = new boolean[]{this.show_first, this.show_second, this.show_third, this.show_forth};
        int[] ids = new int[]{firstv.getId(), secondv.getId(), thirdv.getId(), forthv.getId()};
        int k = 0;
        for (int i = 0; i < shows.length; i++) {
            if (shows[i]) {
                k++;
                if (k > 2) {
//                    ((TextView) root.findViewById(ids[i])).setTextColor(context.getColor(R.color.green_primary));
                }
            }
        }
        if (this.sura == 8 || this.sura == 0) {
            pos++;
        }
        if (pos > 0) {
            num.setText(String.valueOf(pos));
        } else {
            num.setText(null);
        }
        return root;
    }

    public int getPositionForSection(int sectionIndex) {
        return sectionIndex;
    }

    public int getSectionForPosition(int position) {
        return position;
    }

    public Object[] getSections() {
        return this.sections;
    }
}