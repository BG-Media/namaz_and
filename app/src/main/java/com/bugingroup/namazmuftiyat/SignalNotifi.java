package com.bugingroup.namazmuftiyat;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.webkit.WebView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by nurbaqyt on 18.08.17.
 */

public class SignalNotifi extends AppCompatActivity {

    @BindView(R.id.title)
    TextView title;

//    @BindView(R.id.message)
//    TextView message;

    @BindView(R.id.webview)
    WebView webview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_notifi);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Typeface scheherazade = Typeface.createFromAsset(getAssets(), "Scheherazade.ttf");
        title.setText(getIntent().getStringExtra("title"));
//        message.setText(getIntent().getStringExtra("message"));
        title.setTypeface(scheherazade);
//        message.setTypeface(scheherazade);


        webview.loadData(getIntent().getStringExtra("message"), "text/html; charset=UTF-8", null);

    }

    @Override
    public void onBackPressed()
    {
        Intent i = new Intent(SignalNotifi.this,SplashScreen.class);
        startActivity(i);
        finish();
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
