package com.bugingroup.namazmuftiyat.utils;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.preference.PreferenceManager;

import com.bugingroup.namazmuftiyat.Namaz;
import com.google.gson.Gson;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by Nurik on 26.04.2017.
 */

public class DatabaseHelper extends SQLiteOpenHelper {

    // Logcat tag
    private static final String LOG = "DatabaseHelper";

    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "namaz";

    // Table Names
    private static final String TABLE_ZAKAT = "zakat";
    private static final String TABLE_PAY = "pay";
    private static final String TABLE_DEBT = "debt";

    // Common column names
    private static final String KEY_ID = "id";

    private static final String KEY_DNEJNI      = "dnejni";
    private static final String KEY_ZOLOTO      = "zoloto";
    private static final String KEY_SEREBRO     = "serebro";
    private static final String KEY_VERBLIUD    = "verbliud";
    private static final String KEY_BARAN       = "baran";
    private static final String KEY_LOSHAD      = "loshad";
    private static final String KEY_LOSHADC     = "loshadc";
    private static final String KEY_KRC         = "krc";
    private static final String KEY_PREDMET     = "predmet";

    private static final String KEY_PRICE      = "price";
    private static final String KEY_TIME      = "create_time";
    private static final String KEY_DATE      = "create_date";


    private static final String KEY_F_COUNT    = "fajr_count";
    private static final String KEY_F_RESULT   = "fajr_result";
    private static final String KEY_Z_COUNT    = "zuhr_count";
    private static final String KEY_Z_RESULT   = "zuhr_result";
    private static final String KEY_A_COUNT    = "asr_count";
    private static final String KEY_A_RESULT   = "asr_result";
    private static final String KEY_M_COUNT    = "magrib_count";
    private static final String KEY_M_RESULT   = "magrib_result";
    private static final String KEY_I_COUNT    = "isha_count";
    private static final String KEY_I_RESULT   = "isha_result";





    // Table Create Statements
    // Todo table create statement
    private static final String CREATE_TABLE_Pay = "CREATE TABLE "
            + TABLE_PAY + "("
            + KEY_PRICE   + " TEXT,"
            + KEY_TIME   + " TEXT,"
            + KEY_DATE  + " TEXT"
            +")";

    // Table Create Statements
    // Todo table create statement
    private static final String CREATE_TABLE_DEBT = "CREATE TABLE "
            + TABLE_DEBT + "(" + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + KEY_F_COUNT    + " INTEGER,"
            + KEY_F_RESULT   + " INTEGER,"
            + KEY_Z_COUNT    + " INTEGER,"
            + KEY_Z_RESULT   + " INTEGER,"
            + KEY_A_COUNT    + " INTEGER,"
            + KEY_A_RESULT   + " INTEGER,"
            + KEY_M_COUNT    + " INTEGER,"
            + KEY_M_RESULT   + " INTEGER,"
            + KEY_I_COUNT    + " INTEGER,"
            + KEY_I_RESULT   + " INTEGER"
            +")";



    // Table Create Statements
    // Todo table create statement
    private static final String CREATE_TABLE_ZAKAT = "CREATE TABLE "
            + TABLE_ZAKAT + "(" + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + KEY_DNEJNI   + " TEXT,"
            + KEY_ZOLOTO   + " TEXT,"
            + KEY_SEREBRO  + " TEXT,"
            + KEY_VERBLIUD + " TEXT,"
            + KEY_BARAN    + " TEXT,"
            + KEY_LOSHAD   + " TEXT,"
            + KEY_LOSHADC  + " TEXT,"
            + KEY_KRC      + " TEXT,"
            + KEY_PREDMET  + " TEXT"
            +")";

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_ZAKAT);
        db.execSQL(CREATE_TABLE_Pay);
        db.execSQL(CREATE_TABLE_DEBT);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ZAKAT);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PAY);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_DEBT);
        onCreate(db);
    }

    /**
     * Creating a zakat
     */
    public long createToDo(Zakat zakat) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_DNEJNI  , zakat.getDnejni());
        values.put(KEY_ZOLOTO  , zakat.getZoloto());
        values.put(KEY_SEREBRO , zakat.getSerebro());
        values.put(KEY_VERBLIUD, zakat.getVerbliud());
        values.put(KEY_BARAN   , zakat.getBaran());
        values.put(KEY_LOSHAD  , zakat.getLoshad());
        values.put(KEY_LOSHADC , zakat.getLoshadc());
        values.put(KEY_KRC     , zakat.getKrc());
        values.put(KEY_PREDMET , zakat.getPredmet());
        long todo_id = db.insert(TABLE_ZAKAT, null, values);
        return todo_id;
    }
    /**
     * Creating a DEBT
     */
    public long createDebt(Debt debt) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_F_COUNT , debt.getFajr_count());
        values.put(KEY_F_RESULT, debt.getFajr_result());
        values.put(KEY_Z_COUNT , debt.getZuhr_count());
        values.put(KEY_Z_RESULT, debt.getZuhr_result());
        values.put(KEY_A_COUNT , debt.getAsr_count());
        values.put(KEY_A_RESULT, debt.getAsr_result());
        values.put(KEY_M_COUNT , debt.getMagrib_count());
        values.put(KEY_M_RESULT, debt.getMagrib_result());
        values.put(KEY_I_COUNT , debt.getIsha_count());
        values.put(KEY_I_RESULT, debt.getIsha_result());
        long todo_id = db.insert(TABLE_DEBT, null, values);
        return todo_id;
    }
    /**
     * Creating a pay
     */
    public long createPay(Zakat zakat) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_PRICE  , zakat.getPrice());
        values.put(KEY_TIME  , getTime());
        values.put(KEY_DATE , getDate());
        return db.insert(TABLE_PAY, null, values);
    }

    /*
     * getting all zakat
     * */
    public List<Zakat> getAllzakat() {
        List<Zakat> todos = new ArrayList<Zakat>();
        String selectQuery = "SELECT  * FROM " + TABLE_ZAKAT;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);
        if (c.moveToFirst()) {
            do {
                Zakat td = new Zakat();
                td.setId(c.getInt((c.getColumnIndex(KEY_ID))));
                td.setDnejni(c.getString((c.getColumnIndex(KEY_DNEJNI  ))));
                td.setZoloto(c.getString((c.getColumnIndex(KEY_ZOLOTO  ))));
                td.setSerebro(c.getString((c.getColumnIndex(KEY_SEREBRO ))));
                td.setVerbliud(c.getString((c.getColumnIndex(KEY_VERBLIUD))));
                td.setBaran(c.getString((c.getColumnIndex(KEY_BARAN   ))));
                td.setLoshad(c.getString((c.getColumnIndex(KEY_LOSHAD  ))));
                td.setLoshadc(c.getString((c.getColumnIndex(KEY_LOSHADC ))));
                td.setKrc(c.getString((c.getColumnIndex(KEY_KRC     ))));
                td.setPredmet(c.getString((c.getColumnIndex(KEY_PREDMET ))));
                todos.add(td);
            } while (c.moveToNext());
        }
        return todos;
    }

    /*
     * getting all debt
     * */
    public List<Debt> getAlldebt() {
        List<Debt> todos = new ArrayList<Debt>();
        String selectQuery = "SELECT  * FROM " + TABLE_DEBT;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);
        if (c.moveToFirst()) {
            do {
                Debt td = new Debt();
                td.setFajr_count(c.getInt((c.getColumnIndex(KEY_F_COUNT ))));
                td.setFajr_result(c.getInt((c.getColumnIndex(KEY_F_RESULT))));
                td.setZuhr_count(c.getInt((c.getColumnIndex(KEY_Z_COUNT ))));
                td.setZuhr_result(c.getInt((c.getColumnIndex(KEY_Z_RESULT))));
                td.setAsr_count(c.getInt((c.getColumnIndex(KEY_A_COUNT ))));
                td.setAsr_result(c.getInt((c.getColumnIndex(KEY_A_RESULT))));
                td.setMagrib_count(c.getInt((c.getColumnIndex(KEY_M_COUNT ))));
                td.setMagrib_result(c.getInt((c.getColumnIndex(KEY_M_RESULT))));
                td.setIsha_count(c.getInt((c.getColumnIndex(KEY_I_COUNT ))));
                td.setIsha_result(c.getInt((c.getColumnIndex(KEY_I_RESULT))));
                todos.add(td);
            } while (c.moveToNext());
        }
        return todos;
    }
  /*
     * getting all Pay
     * */
    public List<Zakat> getAllPay() {
        List<Zakat> todos = new ArrayList<Zakat>();
        String selectQuery = "SELECT  * FROM " + TABLE_PAY;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);
        if (c.moveToFirst()) {
            do {
                Zakat td = new Zakat();
                td.setPrice(c.getString((c.getColumnIndex(KEY_PRICE))));
                td.setCreate_time(c.getString((c.getColumnIndex(KEY_TIME  ))));
                td.setCreate_date(c.getString((c.getColumnIndex(KEY_DATE  ))));
                todos.add(td);
            } while (c.moveToNext());
        }
        return todos;
    }

    /**
     * get single todo
     */
    public Zakat getSingle(long id) {
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT  * FROM " + TABLE_ZAKAT + " WHERE "
                + KEY_ID + " = " + id;

        Cursor c = db.rawQuery(selectQuery, null);
        if (c != null)
            c.moveToFirst();
        Zakat td = new Zakat();
        td.setId(c.getInt((c.getColumnIndex(KEY_ID))));
        td.setDnejni(c.getString((c.getColumnIndex(KEY_DNEJNI  ))));
        td.setZoloto(c.getString((c.getColumnIndex(KEY_ZOLOTO  ))));
        td.setSerebro(c.getString((c.getColumnIndex(KEY_SEREBRO ))));
        td.setVerbliud(c.getString((c.getColumnIndex(KEY_VERBLIUD))));
        td.setBaran(c.getString((c.getColumnIndex(KEY_BARAN   ))));
        td.setLoshad(c.getString((c.getColumnIndex(KEY_LOSHAD  ))));
        td.setLoshadc(c.getString((c.getColumnIndex(KEY_LOSHADC ))));
        td.setKrc(c.getString((c.getColumnIndex(KEY_KRC     ))));
        td.setPredmet(c.getString((c.getColumnIndex(KEY_PREDMET ))));
        return td;
    }


    /**
     * Deleting a todo tag
     */
    public void deleteToDoTag(long id) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_ZAKAT, KEY_ID + " = ?",
                new String[] { String.valueOf(id) });
    }

    /**
     * Deleting a todo tag
     */
    public void deleteDebt() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_DEBT, KEY_ID + " = ?",
                new String[] { "1"});
    }


    /**
     * Deleting a todo tag
     */
    public void updateDebt(Debt debt) {
        ContentValues initialValues = new ContentValues();
        initialValues.put(KEY_F_COUNT , debt.getFajr_count());
        initialValues.put(KEY_F_RESULT, debt.getFajr_result());
        initialValues.put(KEY_Z_COUNT , debt.getZuhr_count());
        initialValues.put(KEY_Z_RESULT, debt.getZuhr_result());
        initialValues.put(KEY_A_COUNT , debt.getAsr_count());
        initialValues.put(KEY_A_RESULT, debt.getAsr_result());
        initialValues.put(KEY_M_COUNT , debt.getMagrib_count());
        initialValues.put(KEY_M_RESULT, debt.getMagrib_result());
        initialValues.put(KEY_I_COUNT , debt.getIsha_count());
        initialValues.put(KEY_I_RESULT, debt.getIsha_result());
        SQLiteDatabase db = this.getWritableDatabase();
        db.update(TABLE_DEBT,initialValues,KEY_ID + " = ?", new String[] {"1"});
    }




    // closing database
    public void closeDB() {
        SQLiteDatabase db = this.getReadableDatabase();
        if (db != null && db.isOpen())
            db.close();
    }



    private String getDate() {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "dd.MM.yyyy", Locale.getDefault());
        Date date = new Date();
        return dateFormat.format(date);
    }
    private String getTime() {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "HH:mm", Locale.getDefault());
        Date date = new Date();
        return dateFormat.format(date);
    }

    public void saveSmallAdapter(Context context, List<Namaz> favorites) {
        SharedPreferences settings;
        SharedPreferences.Editor editor;

        settings = context.getSharedPreferences("namaz",
                Context.MODE_PRIVATE);
        editor = settings.edit();

        Gson gson = new Gson();
        String jsonFavorites = gson.toJson(favorites);

        editor.putString("namaz", jsonFavorites);

        editor.commit();
    }


    public ArrayList<Namaz> getSmallAdapter(Context context) {
        SharedPreferences settings;
        List<Namaz> favorites;

        settings = context.getSharedPreferences("namaz",
                Context.MODE_PRIVATE);

        if (settings.contains("namaz")) {
            String jsonFavorites = settings.getString("namaz", null);
            Gson gson = new Gson();
            Namaz[] favoriteItems = gson.fromJson(jsonFavorites,
                    Namaz[].class);


            favorites = Arrays.asList(favoriteItems);
            favorites = new ArrayList<Namaz>(favorites);
        } else
            return null;

        return (ArrayList<Namaz>) favorites;
    }

    public void removeSmallAdapter(Context context, List<Namaz> product) {
                ArrayList<Namaz> favorites = getSmallAdapter(context);
                if (favorites != null) {
                        favorites.removeAll(product);
                      saveSmallAdapter(context, favorites);
                   }
            }

//saving timezones
    public void setTimeZone(String timezone, Context context){
        SharedPreferences.Editor prefEditor = PreferenceManager.getDefaultSharedPreferences(context).edit();
        prefEditor.putString("timezone", timezone);
        prefEditor.apply();
    }

    public String getTimeZone(Context context){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        String timezone = prefs.getString("timezone", "");
        return timezone;
    }

}