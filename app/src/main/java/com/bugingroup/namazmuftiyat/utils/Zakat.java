package com.bugingroup.namazmuftiyat.utils;

/**
 * Created by Nurik on 26.04.2017.
 */

public class Zakat {
    private int id;
    private String dnejni,zoloto,serebro,verbliud,baran,loshad,loshadc,krc,predmet,price,create_date,create_time;

    // constructors
    public Zakat() {
    }

    public Zakat(String dnejni,String zoloto,String serebro,String verbliud,String baran,String loshad,String loshadc,String krc,String predmet) {
        this.dnejni= dnejni;
        this.zoloto= zoloto;
        this.serebro= serebro;
        this.verbliud= verbliud;
        this.baran= baran;
        this.loshad= loshad;
        this.loshadc= loshadc;
        this.krc= krc;
        this.predmet= predmet;

    }
    public Zakat(String price) {
        this.price= price;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDnejni() {
        return dnejni;
    }

    public void setDnejni(String dnejni) {
        this.dnejni = dnejni;
    }

    public String getZoloto() {
        return zoloto;
    }

    public void setZoloto(String zoloto) {
        this.zoloto = zoloto;
    }

    public String getSerebro() {
        return serebro;
    }

    public  void setSerebro(String serebro) {
        this.serebro = serebro;
    }

    public String getVerbliud() {
        return verbliud;
    }

    public void setVerbliud(String verbliud) {
        this.verbliud = verbliud;
    }

    public String getBaran() {
        return baran;
    }

    public void setBaran(String baran) {
        this.baran = baran;
    }

    public String getLoshad() {
        return loshad;
    }

    public void setLoshad(String loshad) {
        this.loshad = loshad;
    }

    public  String getLoshadc() {
        return loshadc;
    }

    public  void setLoshadc(String loshadc) {
        this.loshadc = loshadc;
    }

    public String getKrc() {
        return krc;
    }

    public  void setKrc(String krc) {
        this.krc = krc;
    }

    public String getPredmet() {
        return predmet;
    }

    public void setPredmet(String predmet) {
        this.predmet = predmet;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getCreate_date() {
        return create_date;
    }

    public void setCreate_date(String create_date) {
        this.create_date = create_date;
    }

    public String getCreate_time() {
        return create_time;
    }

    public void setCreate_time(String create_time) {
        this.create_time = create_time;
    }
}
