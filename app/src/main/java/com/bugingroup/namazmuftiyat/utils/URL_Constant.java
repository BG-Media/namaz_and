package com.bugingroup.namazmuftiyat.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.widget.EditText;

import java.util.regex.Pattern;

import static com.bugingroup.namazmuftiyat.MainActivity.lang;
import static com.bugingroup.namazmuftiyat.SplashScreen.PREFS_NAME;

public class URL_Constant {
   public static final String DOMAIN = "http://namaz.muftyat.kz/api/";
    public static final String IMAGE = "http://namaz.muftyat.kz";
    public static final String URL_GET_TIME = DOMAIN+"times/";

    public static final String URL_GET_NAMAZ_SPOILED = DOMAIN+"/namazSpoiled";
    public static final String URL_GET_TERMS = DOMAIN+"/terms";
    public static final String URL_GET_TAHARAT = DOMAIN+"/ablution";
    public static final String URL_GET_MOSQUES_BY_COORDS = "http://new2.muftyat.kz/api/mosques/";
    public static final String URL_GET_CITIES = DOMAIN+"/cities";
    public static final String URL_GET_SCHEDULE = DOMAIN+"/namazExport";

    // CHECK EMAIL IS TRUE   TODO:  URL_Constant.checkEmail(editText);
    public static final Pattern EMAIL_ADDRESS_PATTERN = Pattern.compile(
            "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" +
                    "\\@" +
                    "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
                    "(" +
                    "\\." +
                    "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
                    ")+"
    );
    public static boolean checkEmail(EditText editText) {
        String email = editText.getText().toString();
        return EMAIL_ADDRESS_PATTERN.matcher(email).matches();
    }


    public static final String CSS_style = "<style>\n" +
            "    img, embed\n" +
            "    {\n" +
            "/*        width:100% !important;\n" +
            "        height:auto !important;*/\n" +
            "        max-width:100%;\n" +
            "        display: block;\n" +
            "        padding-top: 0px;\n" +
            "        padding-bottom: 0px;\n" +
            "        clear:both\n" +
            "    } \n" +
            "    iframe\n" +
            "    {\n" +
            "      width:100%;\n" +
            "      height:200px;\n" +
            "    }\n" +
            "    \n" +
            "    .firstImg\n" +
            "    {\n" +
            "        padding-top: 10px;\n" +
            "    }\n" +
            "    h3\n" +
            "    {\n" +
            "        margin-bottom:0px\n" +
            "    }\n" +
            "    .date\n" +
            "    {\n" +
            "        color: #808080;\n" +
            "        margin-top: 10px;\n" +
            "    }\n" +
            "    body,body span\n" +
            "    {\n" +
            "        font-family:'Helvetica Neue' !important;\n" +
            "    font-size:12.0pt !important;\n" +
            "    }\n" +
            "</style>";

}
