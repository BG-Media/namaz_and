package com.bugingroup.namazmuftiyat.utils;

/**
 * Created by Nurik on 26.04.2017.
 */

public class Debt {
    private int  fajr_count,fajr_result,zuhr_count,zuhr_result,asr_count,asr_result,magrib_count,magrib_result,isha_count,isha_result;

    // constructors
    public Debt() {
    }

    public Debt(int fajr_count,int fajr_result,int zuhr_count,int zuhr_result,int asr_count,int asr_result,int magrib_count,int magrib_result,int isha_count,int isha_result) {
        this.fajr_count =   fajr_count;
        this.fajr_result =  fajr_result;
        this.zuhr_count =   zuhr_count;
        this.zuhr_result =  zuhr_result;
        this.asr_count =    asr_count;
        this.asr_result =   asr_result;
        this.magrib_count = magrib_count;
        this.magrib_result= magrib_result;
        this.isha_count =   isha_count;
        this.isha_result =  isha_result;

    }

    public int getFajr_count() {
        return fajr_count;
    }

    public void setFajr_count(int fajr_count) {
        this.fajr_count = fajr_count;
    }

    public int getFajr_result() {
        return fajr_result;
    }

    public void setFajr_result(int fajr_result) {
        this.fajr_result = fajr_result;
    }

    public int getZuhr_count() {
        return zuhr_count;
    }

    public void setZuhr_count(int zuhr_count) {
        this.zuhr_count = zuhr_count;
    }

    public int getZuhr_result() {
        return zuhr_result;
    }

    public void setZuhr_result(int zuhr_result) {
        this.zuhr_result = zuhr_result;
    }

    public int getAsr_count() {
        return asr_count;
    }

    public void setAsr_count(int asr_count) {
        this.asr_count = asr_count;
    }

    public int getAsr_result() {
        return asr_result;
    }

    public void setAsr_result(int asr_result) {
        this.asr_result = asr_result;
    }

    public int getMagrib_count() {
        return magrib_count;
    }

    public void setMagrib_count(int magrib_count) {
        this.magrib_count = magrib_count;
    }

    public int getMagrib_result() {
        return magrib_result;
    }

    public void setMagrib_result(int magrib_result) {
        this.magrib_result = magrib_result;
    }

    public int getIsha_count() {
        return isha_count;
    }

    public void setIsha_count(int isha_count) {
        this.isha_count = isha_count;
    }

    public int getIsha_result() {
        return isha_result;
    }

    public void setIsha_result(int isha_result) {
        this.isha_result = isha_result;
    }
}
