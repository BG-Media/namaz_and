package com.bugingroup.namazmuftiyat.utils.volley;

import android.app.Application;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Address;
import android.location.Geocoder;
import android.support.v7.app.AppCompatDelegate;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.bugingroup.namazmuftiyat.Alarm.Alarm;
import com.bugingroup.namazmuftiyat.Alarm.AlarmListAdapter;
import com.bugingroup.namazmuftiyat.ConnectivityStatus;
import com.bugingroup.namazmuftiyat.GetTimes;
import com.bugingroup.namazmuftiyat.MainActivity;
import com.bugingroup.namazmuftiyat.R;
import com.bugingroup.namazmuftiyat.SignalNotifi;
import com.bugingroup.namazmuftiyat.Widget.NamazWidget;
import com.bugingroup.namazmuftiyat.utils.CalculatePrayTime;
import com.bugingroup.namazmuftiyat.utils.DatabaseHelper;
import com.bugingroup.namazmuftiyat.utils.Debt;
import com.bugingroup.namazmuftiyat.utils.TrackGPS;
import com.onesignal.OneSignal;

import org.json.JSONObject;

import java.io.IOException;
import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindArray;

import static com.bugingroup.namazmuftiyat.SplashScreen.PREFS_NAME;

public class AppController extends Application {
	public static int METHOD=8,ASRM=1,HIGHET=3,M1=0,M2=0,M3=0,M4=0,M5=0,M6=0;
	public static final String TAG = AppController.class.getSimpleName();
	DatabaseHelper db;
	String [] namaz_name;
	int YEAR,MONTH,DAY,dd,k=100;
	int pray_year;
	ArrayList<HashMap<String,String>> namaztimeList ;
	public static double timezone;
	public static double latitude=43.244011;
	public static double longitude=76.911454;
	SharedPreferences settings;
	Boolean debt,is_gps=false;
	AlarmListAdapter mAlarmListAdapter;
	int [] alarm_of_onn;
	int [] debt_count;
	int [] debt_notifi;
	int [] debt_repeat;

	private RequestQueue mRequestQueue;
	private ImageLoader mImageLoader;
	LruBitmapCache mLruBitmapCache;
    ConnectivityStatus connectivityStatus = new ConnectivityStatus(this);
	public static int[] addingTimeArr = new int[]{0,0,0,0,0,0,0};

	private static AppController mInstance;

	DatabaseHelper databaseHelper = new DatabaseHelper(this);
	private TrackGPS gps;


	@Override
	public void onCreate() {
		super.onCreate();

		mAlarmListAdapter =  new AlarmListAdapter(this);
		gps = new TrackGPS(this);

		getPrayTime();

//		OneSignal.startInit(this)
//				.inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
//				.unsubscribeWhenNotificationsAreDisabled(true)
//				.init();

		AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
		OneSignal.startInit(this)
				.setNotificationOpenedHandler(new ExampleNotificationOpenedHandler())
				.init();

		mInstance = this;
	}

	// This fires when a notification is opened by tapping on it or one is received while the app is running.
	private class ExampleNotificationOpenedHandler implements OneSignal.NotificationOpenedHandler {

		@Override
		public void notificationOpened(String message, JSONObject additionalData, boolean isActive) {
			Toast.makeText(AppController.this, "ASASAS", Toast.LENGTH_SHORT).show();
			Log.d("@@@@@@@@@@@@@@@@",additionalData+" "+message);
			try {
				if (additionalData != null) {
					Intent intent = new Intent(getApplicationContext(), SignalNotifi.class);
					intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_NEW_TASK);
					intent.putExtra("title", additionalData.getString("title"));
					intent.putExtra("message", message);
					startActivity(intent);
				}

			} catch (Throwable t) {
				t.printStackTrace();
			}



		}
	}

	public static synchronized AppController getInstance() {
		return mInstance;
	}

	public RequestQueue getRequestQueue() {
		if (mRequestQueue == null) {
			mRequestQueue = Volley.newRequestQueue(getApplicationContext());
		}

		return mRequestQueue;
	}

	public ImageLoader getImageLoader() {
		getRequestQueue();
		if (mImageLoader == null) {
			getLruBitmapCache();
			mImageLoader = new ImageLoader(this.mRequestQueue, mLruBitmapCache);
		}

		return this.mImageLoader;
	}

	public LruBitmapCache getLruBitmapCache() {
		if (mLruBitmapCache == null)
			mLruBitmapCache = new LruBitmapCache();
		return this.mLruBitmapCache;
	}

	public <T> void addToRequestQueue(Request<T> req, String tag) {
		req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
		getRequestQueue().add(req);
	}

	public <T> void addToRequestQueue(Request<T> req) {
		req.setTag(TAG);
		getRequestQueue().add(req);
	}

	public void cancelPendingRequests(Object tag) {
		if (mRequestQueue != null) {
			mRequestQueue.cancelAll(tag);
		}
	}


	public void timeHandler() {
		Timer t=new Timer();
		t.scheduleAtFixedRate(new TimerTask() {
			@Override
			public void run() {
//				if(connectivityStatus.isInternetOn()) {
//				}
				getPrayTime();
			}
		}, 0, 43200000);
	}

    public void getPrayTime(){
        GregorianCalendar gc = new GregorianCalendar();
        gc.add(Calendar.DATE, 1);
        Date date = gc.getTime();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        String tomorrowTime = format.format(date)+" 00:00:00";
        Log.d("TTTTT", tomorrowTime);

        Timer t=new Timer();
        try {
            t.schedule(new TimerTask() {
                public void run() {

                   // if(connectivityStatus.isInternetOn()) {
					Log.d("KKKKKKK", "run: ");
//					GetTimes getTimes = new GetTimes(getBaseContext());
//                    getTimes.GetPrayTimes();
					timeHandler();
					GetTimeZone();

					//}

                }
            }, new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(tomorrowTime));
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

	private void GetTimeZone() {
		Calendar cal = Calendar.getInstance();
		Date now = new Date();
		cal.setTime(now);
		Log.d("MONTHaaa",YEAR+"*"+MONTH+"*"+DAY);
		YEAR = cal.get(Calendar.YEAR);
		MONTH = cal.get(Calendar.MONTH)+1;
		DAY = cal.get(Calendar.DATE);
		dd = cal.get(Calendar.DATE);


		settings = getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
		gps = new TrackGPS(this);
		Calendar c = Calendar.getInstance();
		pray_year = c.get(Calendar.YEAR);
		db = new DatabaseHelper(this);

		if(databaseHelper.getTimeZone(this).equals("")) {
			timezone = 6;
		}else{
			timezone = Double.parseDouble(databaseHelper.getTimeZone(this));
		}
		METHOD = settings.getInt("METHOD",8);
		ASRM = settings.getInt("ASRM",1);
		HIGHET = settings.getInt("HIGHET",3);
		M1 = settings.getInt("M1",0);
		M2 = settings.getInt("M2",0);
		M3 = settings.getInt("M3",0);
		M4 = settings.getInt("M4",0);
		M5 = settings.getInt("M5",0);
		M6 = settings.getInt("M6",0);

		debt = settings.getBoolean("debt",false);

		try {
			Intent updateWidget = new Intent(this, NamazWidget.class); // Widget.class is your widget class
			updateWidget.setAction("update_widget");
			PendingIntent pending = PendingIntent.getBroadcast(this, 0, updateWidget, PendingIntent.FLAG_CANCEL_CURRENT);
			pending.send();
		} catch (PendingIntent.CanceledException e) {
			e.printStackTrace();
		}


		getTime(YEAR, MONTH, DAY);
	}

	public void getTime(int y, int m ,int d) {
		if(databaseHelper.getTimeZone(this).equals("")) {
			timezone = 6;
		}else{
			timezone = Double.parseDouble(databaseHelper.getTimeZone(this));
		}


		alarm_of_onn = new int[]{settings.getInt("fajir_select", 3), settings.getInt("voshod_select", 3),settings.getInt("zuhr_select",3),settings.getInt("asr_select",3),3,
				settings.getInt("magrib_select",3),settings.getInt("isha_select",3)};

		debt_notifi = new int[]{settings.getInt("notifi_fajir", 0), 0,settings.getInt("notifi_zuhr",0),settings.getInt("notifi_asr",0),0,
				settings.getInt("notifi_magrib",0),settings.getInt("notifi_isha",0)};

		debt_repeat = new int[]{settings.getInt("pof_fajir", 0), 0,settings.getInt("pof_zuhr",0),settings.getInt("pof_asr",0),0,
				settings.getInt("pof_magrib",0),settings.getInt("pof_isha",0)};


		is_gps =   settings.getBoolean("is_gps",false);
		List<Debt> allToDos = db.getAlldebt();
		if(allToDos.size()>0) {
			Debt debt = allToDos.get(0);
			debt_count = new int[]{debt.getFajr_count(), 0, debt.getZuhr_count(), debt.getAsr_count(), 0, debt.getMagrib_count(), debt.getIsha_count()};
		} else {
			debt_count = new int[]{0, 0, 0, 0, 0, 0, 0};

		}


		SharedPreferences.Editor editor = settings.edit();

		if(is_gps) {
			if (gps.canGetLocation()) {
				longitude = gps.getLongitude();
				latitude = gps.getLatitude();
				Log.d("LLLLL22", longitude + "***" + latitude);
			} else {
				if (settings.getBoolean("gps", true)) {
					//gps.showSettingsAlert(gps_title, gps_message);
					editor.putBoolean("gps", false);
				}
				String coords = settings.getString("coords", "43.244011,76.911454");
				String[] separated = coords.split(",");
				Log.d("LLLLL", separated[1] + "***" + separated[0]);
				longitude = Double.valueOf(separated[1].trim()).doubleValue();
				latitude = Double.valueOf(separated[0].trim()).doubleValue();
				Log.d("LLLLL22", longitude + "***" + latitude);

			}


			Geocoder gcd = new Geocoder(getApplicationContext(), Locale.getDefault());
			List<Address> addresses = null;
			try {
				addresses = gcd.getFromLocation(latitude, longitude, 1);
			} catch (IOException e) {
				e.printStackTrace();
			}

		}else {
			String coords = settings.getString("coords", "43.244011,76.911454");
			Log.d("LLLLLrrrr", coords);
			String[] separated = coords.split(",");
			Log.d("LLLLLqqqq", separated[1] + "***" + separated[0]);
			longitude = Double.valueOf(separated[1].trim()).doubleValue();
			latitude = Double.valueOf(separated[0].trim()).doubleValue();
			Log.d("LLLLL22qqqq", longitude + "***" + latitude);
		}

		editor.putString("city_id","0");
		editor.putString("coords", latitude+","+longitude);
		editor.commit();


		namaztimeList = new ArrayList<>();
//		alarm_of_onn = new int[]{settings.getInt("fajir_select", 3), settings.getInt("voshod_select", 3),settings.getInt("zuhr_select",3),settings.getInt("asr_select",3),3,
//				settings.getInt("magrib_select",3),settings.getInt("isha_select",3)};
//
//		debt_notifi = new int[]{settings.getInt("notifi_fajir", 0), 0,settings.getInt("notifi_zuhr",0),settings.getInt("notifi_asr",0),0,
//				settings.getInt("notifi_magrib",0),settings.getInt("notifi_isha",0)};
//
//		debt_repeat = new int[]{settings.getInt("pof_fajir", 0), 0,settings.getInt("pof_zuhr",0),settings.getInt("pof_asr",0),0,
//				settings.getInt("pof_magrib",0),settings.getInt("pof_isha",0)};

		CalculatePrayTime prayers = new CalculatePrayTime();
		prayers.setTimeFormat(prayers.Time24);
		prayers.setCalcMethod(METHOD);
		prayers.setAsrJuristic(ASRM);
		prayers.setAdjustHighLats(HIGHET);

		namaz_name =getResources().getStringArray(R.array.namaz_name);

		ArrayList<String> timeNames = new ArrayList<String>();
		timeNames.add(namaz_name[0]);
		timeNames.add(namaz_name[1]);
		timeNames.add(namaz_name[2]);
		timeNames.add(namaz_name[3]);
		timeNames.add(namaz_name[4]);
		timeNames.add(namaz_name[4]);
		timeNames.add(namaz_name[5]);
		prayers.setTimeNames(timeNames);

		int[] offsets = new int[]{M1, M2, M3, M4, 0, M5, M6};
		if (METHOD == 8) {
			if (latitude < 48) {
				offsets = new int[]{M1, M2 - 3, M3 + 3, M4 + 3, 0, M5 + 3, M6};
			} else {
				offsets = new int[]{M1, M2 - 5, M3 + 5, M4 + 5, 0, M5 + 5, M6};
			}
		}

		prayers.tune(offsets);

		SimpleDateFormat df = new SimpleDateFormat("HH:mm");
		Calendar calculateCalendar = Calendar.getInstance();

//		Calendar cal = Calendar.getInstance();
//		Date now = new Date();
//		cal.setTime(now);


		Log.d("HHH", y + "**" + m + "**" + d + "**" + latitude + "**" + longitude + "**" + timezone);
		ArrayList prayerTimes = prayers.getDatePrayerTimes(y, m, d, latitude, longitude, timezone);
		ArrayList prayerNames = prayers.getTimeNames();
		mAlarmListAdapter.clear();
		int j = 0;
		for (int i = 0; i < prayerTimes.size(); i++) {
			if (i != 4) {
				Log.d("SSSS", prayerTimes.size() + "***" + j + "**" + prayerTimes.get(j));
				try {
					Date dateForCalculation = df.parse(prayerTimes.get(i) + "");
					calculateCalendar.setTime(dateForCalculation);
					calculateCalendar.add(Calendar.MINUTE, addingTimeArr[i]);
					String newTime = df.format(calculateCalendar.getTime());

					HashMap<String, String> hashMap = new HashMap<>();
					hashMap.put("name", prayerNames.get(i) + "");
					hashMap.put("time", newTime);
					namaztimeList.add(hashMap);
					Log.d("namaztimeList", namaztimeList + "");


					System.out.println("---------------------------------888888888-----------------------------------");
					System.out.println("---" + i + "????" + y + "---" + (m - 1) + "---" + d + "---" + calculateCalendar.getTime().getHours() + "---" + calculateCalendar.getTime().getMinutes());
					GregorianCalendar mCalendar;
					mCalendar = new GregorianCalendar(y, m - 1, d, calculateCalendar.getTime().getHours(), calculateCalendar.getTime().getMinutes());
					if (mCalendar.getTimeInMillis() > System.currentTimeMillis() && alarm_of_onn[i] != 3) {
						Alarm mAlarm = new Alarm(this);
						mAlarm.setId(i);
						mAlarm.setType(i);
						mAlarm.setRepeat_t(0);
						mAlarm.setIs_debt(false);
						mAlarm.setTitle("" + prayerNames.get(i));
						mAlarm.setDate(mCalendar.getTimeInMillis());
						mAlarm.setDays(0);
						mAlarm.setEnabled(true);
						mAlarm.setOccurence(0);
						mAlarmListAdapter.add(mAlarm);
					}

					if (debt_notifi[i] != 0) {
						GregorianCalendar dCalendar;
						dCalendar = new GregorianCalendar(y, m - 1, d, calculateCalendar.getTime().getHours(), calculateCalendar.getTime().getMinutes() + debt_notifi[i]);

						if (dCalendar.getTimeInMillis() > System.currentTimeMillis() && debt_count[i] > 0) {
							Alarm mAlarm = new Alarm(this);
							mAlarm.setId(i);
							mAlarm.setType(i);
							mAlarm.setRepeat_t(debt_repeat[i]);
							mAlarm.setIs_debt(true);
							mAlarm.setTitle(getResources().getString(R.string.notification_debt) + " " + prayerNames.get(i));
							mAlarm.setDate(dCalendar.getTimeInMillis());
							mAlarm.setDays(0);
							mAlarm.setEnabled(true);
							mAlarm.setOccurence(0);
							mAlarmListAdapter.add(mAlarm);

						}
					}


					int hours = new Time(System.currentTimeMillis()).getHours();
					int minute = new Time(System.currentTimeMillis()).getMinutes();
					String[] separated = newTime.split(":");
					int h = Integer.valueOf(separated[0]);
					int M = Integer.valueOf(separated[1]);

					Log.d("HH", hours + "_" + h + "M:" + minute + "_" + M);
					if (k == 100 && dd == d) {
						if (h >= hours && M > minute) {
							k = j;
						} else if (h > hours && h != 0) {
							k = j;
						}
					}

				} catch (ParseException e) {
					e.printStackTrace();
				}
				j++;
			}

		}
	}

}
