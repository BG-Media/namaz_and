package com.bugingroup.namazmuftiyat.utils;

import android.content.Context;
import android.text.Html;
import android.util.Log;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.bugingroup.namazmuftiyat.R;
import com.bugingroup.namazmuftiyat.utils.volley.AppController;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.bugingroup.namazmuftiyat.MainActivity.lang;
import static com.bugingroup.namazmuftiyat.MainActivity.mosquesList;
import static com.bugingroup.namazmuftiyat.MainActivity.scheduleList;
import static com.bugingroup.namazmuftiyat.bookTaharatTerminDestroy.Comments.commentsAdapter;
import static com.bugingroup.namazmuftiyat.bookTaharatTerminDestroy.Comments.commentsList;
import static com.bugingroup.namazmuftiyat.useful.UsefulMaterial.usefulyAdapter;

/**
 * Created by User on 23.11.2016.
 */

public class Requests {



    public void getNamazBook(final Context context, final SimpleAdapter adapter, final ArrayList<HashMap<String,String>> arrayList,String url){
        arrayList.clear();
        Log.d("URL, ",URL_Constant.DOMAIN+lang+"/posts/"+url);
            StringRequest jsonReq = new StringRequest (Request.Method.GET,
                    URL_Constant.DOMAIN+lang+"/posts/"+url,new Response.Listener<String>() {

                @Override
                public void onResponse(String response) {
                    if(response!=null){
                        Log.d("ddddd",response);

                        try {
                           String newStr = URLDecoder.decode(URLEncoder.encode(response, "iso8859-1"),"UTF-8");

                            JSONObject jsonObject2 =  new JSONObject(newStr);
                            JSONArray serviceArray2 = jsonObject2.getJSONArray("results");
                            JSONObject service2 = serviceArray2.getJSONObject(0);
                            Log.d("ARABBBB",service2.getString("content_2")+"+++"+fixEncodingUnicode(service2.getString("content_2"))+"+++");


                            JSONObject jsonObject =  new JSONObject(response);
                            if(jsonObject.getBoolean("success")) {
                                JSONArray serviceArray = jsonObject.getJSONArray("results");
                                for (int i = 0; i < serviceArray.length(); i++) {
                                    JSONObject service = serviceArray.getJSONObject(i);
                                    HashMap<String, String> hashMap = new HashMap<>();
                                    hashMap.put("id", service.getString("id"));
                                    hashMap.put("title", service.getString("title"));
                                    hashMap.put("description", service.getString("description"));
                                    hashMap.put("content", service.getString("content"));
                                    hashMap.put("content_2", service.getString("content_2"));
                                    hashMap.put("audio", service.getString("audio"));
                                    byte ptext[] = new byte[0];
                                    try {
                                        ptext = service.getString("content_2").getBytes("ISO-8859-1");
                                        String value = new String(ptext, "UTF-8");
                                        Log.d("ARABBBB",service.get("content_2")+"**"+value);
                                    } catch (UnsupportedEncodingException e) {
                                        e.printStackTrace();
                                    }

                                    Log.d("ARABBBB",service.get("content_2")+"**"+fixEncodingUnicode(service.getString("content_2"))+"**");
                                    hashMap.put("views", service.getString("views"));
                                    hashMap.put("created_at", service.getString("created_at"));
                                    hashMap.put("comments", service.getString("comments"));
                                    arrayList.add(hashMap);
                                }
                                if (adapter != null) {
                                    adapter.notifyDataSetChanged();
                                }
                            }
                            //Log.d("test",response);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {

                    Toast.makeText(context,context.getResources().getString(R.string.volley_error),Toast.LENGTH_SHORT).show();

                    VolleyLog.d("Error: " + error.getMessage());
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                    headers.put("Content-Type", "application/json");
                    return headers;
                }
            };
            AppController.getInstance().addToRequestQueue(jsonReq);
    }

    public static String fixEncodingUnicode(String response) {
        String str = "";
        try {
            str = new String(response.getBytes("windows-1254"), "UTF-8");
        } catch (UnsupportedEncodingException e) {

            e.printStackTrace();
        }

        String decodedStr = Html.fromHtml(str).toString();
        return decodedStr;
    }
    public void getComments(final Context context, final ArrayList<HashMap<String,String>> arrayList,String id){
        commentsList.clear();
        StringRequest jsonReq = new StringRequest (Request.Method.GET,
                    URL_Constant.DOMAIN+lang+"/comments/"+id,new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    if(response!=null){
                        try {
                            JSONObject jsonObject =  new JSONObject(response);
                            if(jsonObject.getBoolean("success")) {
                                JSONArray serviceArray = jsonObject.getJSONArray("results");
                                for (int i = 0; i < serviceArray.length(); i++) {
                                    JSONObject service = serviceArray.getJSONObject(i);
                                    if(service.getString("parent_id").equals("0")) {
                                        HashMap<String, String> hashMap = new HashMap<>();
                                        hashMap.put("id", service.getString("id"));
                                        hashMap.put("text", service.getString("text"));
                                        hashMap.put("name", service.getString("name"));
                                        hashMap.put("email", service.getString("email"));
                                        hashMap.put("parent_id", service.getString("parent_id"));
                                        hashMap.put("created_at", service.getString("created_at"));
                                        arrayList.add(hashMap);
                                        for (int j = i; j < serviceArray.length(); j++) {
                                            JSONObject service2 = serviceArray.getJSONObject(j);
                                            if(service.getString("id").equals(service2.getString("parent_id"))) {
                                                HashMap<String, String> hashMap2 = new HashMap<>();
                                                hashMap2.put("id", service2.getString("id"));
                                                hashMap2.put("text", service2.getString("text"));
                                                hashMap2.put("name", service2.getString("name"));
                                                hashMap2.put("email", service2.getString("email"));
                                                hashMap2.put("parent_id", service2.getString("parent_id"));
                                                hashMap2.put("created_at", service2.getString("created_at"));
                                                arrayList.add(hashMap2);
                                            }
                                        }

                                    }

                                }
                                commentsAdapter.notifyDataSetChanged();
                            }
                            //Log.d("test",response);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {


                    Toast.makeText(context,context.getResources().getString(R.string.volley_error),Toast.LENGTH_SHORT).show();

                    VolleyLog.d("Error: " + error.getMessage());
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                    headers.put("Content-Type", "application/json");
                    return headers;
                }
            };
            AppController.getInstance().addToRequestQueue(jsonReq);
    }


    public void getUsefuly(final Context context, final ArrayList<HashMap<String,String>> arrayList){

        Log.d("ASASASASURL: ",URL_Constant.DOMAIN+lang+"/posts/15");
        StringRequest jsonReq = new StringRequest (Request.Method.GET,
                    URL_Constant.DOMAIN+lang+"/posts/15",new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    if(response!=null){
                        try {
                            JSONObject jsonObject =  new JSONObject(response);
                            if(jsonObject.getBoolean("success")) {
                                JSONArray serviceArray = jsonObject.getJSONArray("results");
                                for (int i = 0; i < serviceArray.length(); i++) {
                                    JSONObject service = serviceArray.getJSONObject(i);
                                    HashMap<String, String> hashMap = new HashMap<>();
                                    hashMap.put("id", service.getString("id"));
                                    hashMap.put("title", service.getString("title"));
                                    hashMap.put("description", service.getString("description"));
                                    hashMap.put("content", service.getString("content"));
                                    hashMap.put("views", service.getString("views"));
                                    hashMap.put("image", service.getString("image"));
                                    hashMap.put("created_at", service.getString("created_at"));
                                    hashMap.put("comments", service.getString("comments"));
                                    hashMap.put("audio", service.getString("audio"));
                                    arrayList.add(hashMap);
                                }
                                usefulyAdapter.notifyDataSetChanged();
                            }
                            //Log.d("test",response);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {


                    Toast.makeText(context,context.getResources().getString(R.string.volley_error),Toast.LENGTH_SHORT).show();

                    VolleyLog.d("Error: " + error.getMessage());
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                    headers.put("Content-Type", "application/json");
                    return headers;
                }
            };
            AppController.getInstance().addToRequestQueue(jsonReq);
    }

    public void getSub_categories(final Context context, final ArrayList<HashMap<String,String>> arrayList, String id, final SimpleAdapter adapter){
        arrayList.clear();
        StringRequest jsonReq = new StringRequest (Request.Method.GET,
                    URL_Constant.DOMAIN+lang+"/sub_categories/"+id,new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    if(response!=null){
                        try {
                            JSONObject jsonObject =  new JSONObject(response);
                            if(jsonObject.getBoolean("success")) {
                                JSONArray serviceArray = jsonObject.getJSONArray("results");
                                for (int i = 0; i < serviceArray.length(); i++) {
                                    JSONObject service = serviceArray.getJSONObject(i);
                                    HashMap<String, String> hashMap = new HashMap<>();
                                    hashMap.put("id", service.getString("id"));
                                    hashMap.put("title", service.getString("title"));
                                    arrayList.add(hashMap);
                                }
                                adapter.notifyDataSetChanged();
                            }
                            //Log.d("test",response);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(context,context.getResources().getString(R.string.volley_error),Toast.LENGTH_SHORT).show();
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                    headers.put("Content-Type", "application/json");
                    return headers;
                }
            };
            AppController.getInstance().addToRequestQueue(jsonReq);
    }


    public void getNamazSpoiled(final Context context, final TextView descText){
        StringRequest jsonReq = new StringRequest (Request.Method.GET,
                URL_Constant.URL_GET_NAMAZ_SPOILED,new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if(response!=null){
                    try {

                        JSONObject jsonObject =  new JSONObject(response);
                        JSONObject spoiledJObject = jsonObject.getJSONObject("result");
                        HashMap<String,String> hashMap = new HashMap<>();

                        hashMap.put("text_kz",spoiledJObject.getString("text_kz"));
                        hashMap.put("text_ru",spoiledJObject.getString("text_ru"));

                        descText.setText(spoiledJObject.getString("text_ru"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {


                Toast.makeText(context,context.getResources().getString(R.string.volley_error),Toast.LENGTH_SHORT).show();

            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json");
                return headers;
            }

//            @Override
//            protected Map<String, String> getParams() {
//                Map<String, String> params = new HashMap<String, String>();
//                return params;
//            }
        };
        AppController.getInstance().addToRequestQueue(jsonReq);
    }

    public void getTerms(final Context context, final TextView descText){
        StringRequest jsonReq = new StringRequest (Request.Method.GET,
                URL_Constant.URL_GET_TERMS,new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if(response!=null){
                    try {

                        JSONObject jsonObject =  new JSONObject(response);
                        JSONObject termJObject = jsonObject.getJSONObject("result");

                        HashMap<String,String> hashMap = new HashMap<>();

                        hashMap.put("text_kz",termJObject.getString("text_kz"));
                        hashMap.put("text_ru",termJObject.getString("text_ru"));

                        descText.setText(termJObject.getString("text_ru"));
                     } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {


                Toast.makeText(context,context.getResources().getString(R.string.volley_error),Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json");
                return headers;
            }

//            @Override
//            protected Map<String, String> getParams() {
//                Map<String, String> params = new HashMap<String, String>();
//                return params;
//            }
        };
        AppController.getInstance().addToRequestQueue(jsonReq);
    }




    public void getTaharat(final Context context, final TextView descText, final TextView titleText){
        StringRequest jsonReq = new StringRequest (Request.Method.GET,
                URL_Constant.URL_GET_TAHARAT,new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if(response!=null){
                    try {

                        JSONObject jsonObject =  new JSONObject(response);
                        JSONObject taharatJObject = jsonObject.getJSONObject("result");

                        HashMap<String,String> hashMap = new HashMap<>();

                        hashMap.put("title_kz",taharatJObject.getString("title_kz"));
                        hashMap.put("title_ru",taharatJObject.getString("title_ru"));
                        hashMap.put("text_kz",taharatJObject.getString("text_kz"));
                        hashMap.put("text_ru",taharatJObject.getString("text_ru"));

                        //taharatList.add(hashMap);

                        descText.setText(taharatJObject.getString("text_ru"));
                        titleText.setText(taharatJObject.getString("title_ru"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {


                Toast.makeText(context,context.getResources().getString(R.string.volley_error),Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json");
                return headers;
            }

//            @Override
//            protected Map<String, String> getParams() {
//                Map<String, String> params = new HashMap<String, String>();
//                return params;
//            }
        };
        AppController.getInstance().addToRequestQueue(jsonReq);
    }


    public void getMosques(final Context context, double lat, double longitude){
        mosquesList.clear();
            StringRequest jsonReq = new StringRequest (Request.Method.GET,
                    URL_Constant.URL_GET_MOSQUES_BY_COORDS+lat+"/"+longitude+"/10",new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    if(response!=null){
                        try {
                            //JSONObject jsonObject =  new JSONObject(response);
                            JSONArray serviceArray = new JSONArray(response);
                            for (int i = 0; i < serviceArray.length(); i++) {
                                JSONObject service = serviceArray.getJSONObject(i);
                                HashMap<String, Object> hashMap = new HashMap<>();

                                hashMap.put("image", "http://new2.muftyat.kz/uploads/" + service.getString("image"));
                                hashMap.put("title", service.getString("title"));
                                hashMap.put("short_description", service.getString("short_description"));
                                hashMap.put("address", service.getString("address"));

                                String string = service.getString("coords");
                                String[] parts = string.split(",");
                                String part1 = parts[0];
                                String part2 = parts[1];

                                //JSONArray coordsArray = service.getJSONArray("coords");
                                hashMap.put("lat", part1);
                                hashMap.put("long", part2);

                                mosquesList.add(hashMap);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {

                    Toast.makeText(context,context.getResources().getString(R.string.volley_error),Toast.LENGTH_SHORT).show();
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                    headers.put("Content-Type", "application/json");
                    return headers;
                }
            };
            if(!jsonReq.equals(null)) {
                AppController.getInstance().addToRequestQueue(jsonReq);
            }
    }


    public void getCities(final Context context, final ArrayList<HashMap<String,String>> arrayList,
                          final SimpleAdapter adapter) {
        if (arrayList.size() == 0) {
            StringRequest jsonReq = new StringRequest(Request.Method.GET,
                    URL_Constant.URL_GET_CITIES, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    if (response != null) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray serviceArray = jsonObject.getJSONArray("result");
                            for (int i = 0; i < serviceArray.length(); i++) {
                                JSONObject service = serviceArray.getJSONObject(i);
                                HashMap<String, String> hashMap = new HashMap<>();

                                hashMap.put("id", service.getString("id"));
                                hashMap.put("name", service.getString("name"));
                                hashMap.put("latitude", service.getString("latitude"));
                                hashMap.put("longitude", service.getString("longitude"));

                                arrayList.add(hashMap);
                            }

                            if (adapter != null) {
                                adapter.notifyDataSetChanged();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {

                    Toast.makeText(context,context.getResources().getString(R.string.volley_error),Toast.LENGTH_SHORT).show();
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                    headers.put("Content-Type", "application/json");
                    return headers;
                }

//            @Override
//            protected Map<String, String> getParams() {
//                Map<String, String> params = new HashMap<String, String>();
//                return params;
//            }
            };
            AppController.getInstance().addToRequestQueue(jsonReq);
        }
    }

    public void getSchedule(String period, double lat, double longitude){
        final String request = "{ \"period\":\""+period+"\", \"lat\":"+"\""+lat+"\",\"long\":\""+longitude+"\"}";
        JsonObjectRequest jsonObjReq = null;
        try {
            jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                    URL_Constant.URL_GET_SCHEDULE, new JSONObject(request),
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                JSONObject jsonObject = response.getJSONObject("input");
                                JSONArray jsonArray = response.getJSONArray("output");
                                for(int i = 0;i<jsonArray.length();i++){
                                    JSONObject c = jsonArray.getJSONObject(i);
                                    HashMap<String,String> hashMap = new HashMap<>();
                                    hashMap.put("date",c.getString("date"));
                                    hashMap.put("Fajr",c.getString("Fajr"));
                                    hashMap.put("Sunrise",c.getString("Sunrise"));
                                    hashMap.put("Dhuhr",c.getString("Dhuhr"));
                                    hashMap.put("Asr",c.getString("Asr"));
                                    hashMap.put("Maghrib",c.getString("Maghrib"));
                                    hashMap.put("Isha",c.getString("Isha"));

                                    scheduleList.add(hashMap);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) { ///minau error
//                    progressDialog.dismiss();
//                    if(settingsLng.equals("en")){
//                        Toast.makeText(getActivity().getApplicationContext(),"Can't execute the operation",Toast.LENGTH_SHORT).show();
//                    }else{
//                        Toast.makeText(getActivity().getApplicationContext(),"Не удалось выполнить операцию",Toast.LENGTH_SHORT).show();
//                    }
                }                                                    ///minau jueili jakwa
            }) {                                         ///minau jueili jakwa jai jakwa

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                    headers.put("Content-Type", "application/json");
                    return headers;
                }

                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();

                    return params;
                }

            };
        } catch (JSONException e) {
            e.printStackTrace();
        }

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(jsonObjReq,
                "TravelChefs");
    }
}
