package com.bugingroup.namazmuftiyat.bookTaharatTerminDestroy;

/**
 * Created by Nurik on 11.04.2017.
 */
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import org.json.JSONArray;

public class MPagerAdapter extends FragmentStatePagerAdapter {
    private int mNumOfTabs;
    JSONArray serviceArray;
    int[] ids;


    MPagerAdapter(FragmentManager fm, int NumOfTabs, JSONArray serviceArray, int[] ids) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
        this.serviceArray = serviceArray;
        this.ids = ids;
    }

    @Override
    public Fragment getItem(int position) {

        return new FragmentRakat(ids[position],serviceArray);
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}