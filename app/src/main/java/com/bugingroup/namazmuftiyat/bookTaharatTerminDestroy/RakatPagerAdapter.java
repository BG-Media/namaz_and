package com.bugingroup.namazmuftiyat.bookTaharatTerminDestroy;

/**
 * Created by Nurik on 11.04.2017.
 */
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import org.json.JSONArray;

public class RakatPagerAdapter extends FragmentStatePagerAdapter {
    private int mNumOfTabs;
    JSONArray serviceArray;

    RakatPagerAdapter(FragmentManager fm, int NumOfTabs, JSONArray serviceArray) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
        this.serviceArray = serviceArray;
    }

    @Override
    public Fragment getItem(int position) {

       return new FragmentRakatInfo(serviceArray,position+1);
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}