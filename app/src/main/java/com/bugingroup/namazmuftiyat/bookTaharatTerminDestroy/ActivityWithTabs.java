package com.bugingroup.namazmuftiyat.bookTaharatTerminDestroy;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.bugingroup.namazmuftiyat.R;
import com.bugingroup.namazmuftiyat.utils.Requests;
import com.bugingroup.namazmuftiyat.utils.URL_Constant;
import com.bugingroup.namazmuftiyat.utils.volley.AppController;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;

import static com.bugingroup.namazmuftiyat.MainActivity.lang;

/**
 * Created by Nurik on 11.04.2017.
 */

public class ActivityWithTabs extends AppCompatActivity {

    Intent in;

    @BindView(R.id.tabs)
    TabLayout tabLayout;
    LinearLayout mTabsLinearLayout;

    @BindView(R.id.pager)
    ViewPager viewPager;

    String [] titles;
    int  [] ids;
    Requests r = new Requests();
    String id;

    @BindString(R.string.download)
    String download;

    @BindString(R.string.volley_error)
    String volley_error;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tabs);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        in = getIntent();
        id = in.getStringExtra("id");
        getNamaz_Sub();
        getSupportActionBar().setTitle(in.getStringExtra("title"));




        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    public void getNamaz_Sub(){
        final ProgressDialog p = new ProgressDialog(ActivityWithTabs.this);
        p.setTitle(download);
        p.show();
        StringRequest jsonReq = new StringRequest (Request.Method.GET,
                URL_Constant.DOMAIN+lang+"/sub_categories/"+id,new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if(response!=null){
                    try {
                        JSONObject jsonObject =  new JSONObject(response);
                        if(jsonObject.getBoolean("success")) {
                            JSONArray serviceArray = jsonObject.getJSONArray("results");
                            titles = new String[serviceArray.length()];
                            ids = new int[serviceArray.length()];
                            for (int i = 0; i < serviceArray.length(); i++) {
                                JSONObject service = serviceArray.getJSONObject(i);
                                titles[i] = service.getString("title");
                                ids[i] = service.getInt("id");
                                tabLayout.addTab(tabLayout.newTab().setText(service.getString("title")));
                            }
                            viewPager.setAdapter(new MPagerAdapter(getSupportFragmentManager(), tabLayout.getTabCount(),serviceArray,ids));

                        }

                        //Log.d("test",response);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                p.dismiss();
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                Toast.makeText(ActivityWithTabs.this,volley_error,Toast.LENGTH_SHORT).show();
                p.dismiss();
                VolleyLog.d("Error: " + error.getMessage());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json");
                return headers;
            }
        };
        AppController.getInstance().addToRequestQueue(jsonReq);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
