package com.bugingroup.namazmuftiyat.bookTaharatTerminDestroy;

import android.content.Context;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.os.Handler;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.bugingroup.namazmuftiyat.R;
import com.bugingroup.namazmuftiyat.utils.volley.ImageVolleyRequest;

import java.util.HashMap;
import java.util.List;

/**
 * Created by Nurik on 11.05.2017.
 */

public class ExpandableListAdapter extends BaseExpandableListAdapter {

    private ImageLoader imageLoader;
    private Context _context;
    private List<String> _listDataHeader;
    private HashMap<String, List<String>> _listDataChild;
    private final Handler handler = new Handler();
    private MediaPlayer mediaPlayer ;

    public ExpandableListAdapter(Context context, List<String> listDataHeader,
                                 HashMap<String, List<String>> listChildData) {
        this._context = context;
        this._listDataHeader = listDataHeader;
        this._listDataChild = listChildData;
        imageLoader = ImageVolleyRequest.getInstance(_context)
                .getImageLoader();

    }

    @Override
    public Object getChild(int groupPosition, int childPosititon) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                .get(childPosititon);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {


        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.list_child_item, null);
        }

        TextView txtListChild = (TextView) convertView.findViewById(R.id.child_text);
        NetworkImageView image = (NetworkImageView) convertView.findViewById(R.id.image);
        Log.d("childPosition",childPosition+"*");
        if(childPosition==0) {
            txtListChild.setVisibility(View.VISIBLE);
            image.setVisibility(View.GONE);
            txtListChild.setText(_context.getResources().getString(R.string.dalel));
            txtListChild.setPadding(10,10,10,10);
            txtListChild.setTextColor(_context.getResources().getColor(R.color.colorPrimary));
        }else if(childPosition==1) {
            txtListChild.setVisibility(View.GONE);
            image.setVisibility(View.VISIBLE);
            String image_url = (String) getChild(groupPosition, childPosition);
            Log.d("image_url",image_url);
            image.setImageUrl(image_url, imageLoader);
        } else {
            String childText = (String) getChild(groupPosition, childPosition);
            txtListChild.setVisibility(View.VISIBLE);
            image.setVisibility(View.GONE);

            txtListChild.setPadding(8,0,8,0);
            childText= childText.replaceAll("\\\\r", "");
            childText= childText.replaceAll("\\\\n", "");
            childText= childText.replaceAll("\\\\t", "");
            txtListChild.setText(Html.fromHtml(childText));
            txtListChild.setTextColor(_context.getResources().getColor(R.color.black));
        } return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                .size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this._listDataHeader.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return this._listDataHeader.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        String headerTitle = (String) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.list_group, null);
        }

        TextView lblListHeader = (TextView) convertView
                .findViewById(R.id.title);
        lblListHeader.setTypeface(null, Typeface.BOLD);
        lblListHeader.setText(headerTitle);

        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}
