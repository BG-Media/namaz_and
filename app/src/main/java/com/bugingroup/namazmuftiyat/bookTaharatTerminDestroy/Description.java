package com.bugingroup.namazmuftiyat.bookTaharatTerminDestroy;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.bugingroup.namazmuftiyat.R;
import com.bugingroup.namazmuftiyat.utils.Requests;

import java.util.ArrayList;
import java.util.HashMap;

import static com.bugingroup.namazmuftiyat.MainActivity.whatPage;
import static com.bugingroup.namazmuftiyat.bookTaharatTerminDestroy.CommonList.bookList;

public class Description extends AppCompatActivity {

    TextView descText;
    TextView descTitleText;
    Intent in;
    Requests r;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_description);

        descText = (TextView) findViewById(R.id.description_text);
        descTitleText = (TextView) findViewById(R.id.description_title);

        in = getIntent();
        r = new Requests();

        if(whatPage.equals("taharat")){
            r.getTaharat(this,descText,descTitleText);
        }else if(whatPage.equals("destroy")){
            descTitleText.setVisibility(View.GONE);
            r.getNamazSpoiled(this,descText);
        }else if(whatPage.equals("terms")){
            descTitleText.setVisibility(View.GONE);
            r.getTerms(this,descText);
        }else if(whatPage.equals("common")){
            descTitleText.setVisibility(View.GONE);
            initDescription(bookList);
        }
    }


    private void initDescription(ArrayList<HashMap<String,String>> list){
        for (int i = 0;i<list.size();i++){
            if(in.getStringExtra("name").equals(list.get(i).get("title_ru"))){
                descText.setText(list.get(i).get("text_ru"));
            }
        }
    }
}
