package com.bugingroup.namazmuftiyat.bookTaharatTerminDestroy;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ExpandableListView;

import com.android.volley.toolbox.ImageLoader;
import com.bugingroup.namazmuftiyat.R;
import com.bugingroup.namazmuftiyat.utils.NonScrollExpandableListView;
import com.bugingroup.namazmuftiyat.utils.volley.ImageVolleyRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.bugingroup.namazmuftiyat.bookTaharatTerminDestroy.ActivityRakatTbs.Arab;
import static com.bugingroup.namazmuftiyat.bookTaharatTerminDestroy.ActivityRakatTbs.Lan;
import static com.bugingroup.namazmuftiyat.bookTaharatTerminDestroy.ActivityRakatTbs.Trans;

/**
 * Created by Nurik on 11.04.2017.
 */

@SuppressLint("ValidFragment")
public class FragmentRakatInfo extends Fragment {
    int id;
    JSONArray serviceArray;
    ExpandableListAdapter  listAdapter;


    HashMap<String, List<String>> expandableListDetail = new HashMap<String, List<String>>();
    List<String> expandableListTitle =  new ArrayList<String>();;

    @BindView(R.id.rakat_list)
    NonScrollExpandableListView listView;
    private final Handler handler = new Handler();
    ImageLoader imageLoader;
    public FragmentRakatInfo(JSONArray serviceArray, int i) {
        this.serviceArray = serviceArray;
        this.id = i;
    }

    @Override
    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container, final Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_rakat_description, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(final View view, @Nullable final Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        imageLoader = ImageVolleyRequest.getInstance(getActivity())
                .getImageLoader();


        try {
            for (int i = 0; i < serviceArray.length(); i++) {
                JSONObject service = serviceArray.getJSONObject(i);
                if (service.getInt("rakat") == id) {

                    List<String> child = new ArrayList<String>();
                    child.add(service.getString("content_2"));
                    child.add(service.getString("image"));

                 //   child.add(service.getString("audio"));
                    child.add(service.getString("description"));
                    JSONArray content = service.getJSONArray("content");
                    if(content.length()>0){
                        for (int j=0;j<content.length();j++) {
                            JSONObject contents = (JSONObject) content.get(j);
                            child.add(j+1+") "+contents.getString("title"));
                            if(Arab && !contents.getString("arab").equals("")) child.add(contents.getString("arab"));
                            if(Lan && !contents.getString("trans").equals("")) child.add(contents.getString("trans"));
                            if(Trans && !contents.getString("read").equals("")) child.add(contents.getString("read"));


                        }
                    }
                     Log.d("123123123",service.getString("title"));
                    expandableListTitle.add(service.getString("title"));
                    expandableListDetail.put(service.getString("title"), child);

                }
            }

            listAdapter = new ExpandableListAdapter(getActivity(), expandableListTitle, expandableListDetail);

            listView.setAdapter(listAdapter);
            Log.d("AAAAA2","title");
        } catch (JSONException e) {
            e.printStackTrace();
        }


        listView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                if(childPosition==0) {
                    final String selected = (String) listAdapter.getChild(
                            groupPosition, childPosition);
                    final Dialog dialog = new Dialog(getActivity());
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setContentView(R.layout.dalel);
                    Button close = (Button) dialog.findViewById(R.id.close);
                    close.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });
                    WebView webview = (WebView) dialog.findViewById(R.id.webview);
                    WebSettings settings = webview.getSettings();
                    settings.setDefaultTextEncodingName("utf-8");
                    webview.loadData(selected, "text/html; charset=utf-8", "utf-8");

                    dialog.show();
                }
                return false;
            }
        });

    }



}






