package com.bugingroup.namazmuftiyat.bookTaharatTerminDestroy;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.bugingroup.namazmuftiyat.R;
import com.bugingroup.namazmuftiyat.utils.CircleImageView;
import com.bugingroup.namazmuftiyat.utils.Requests;
import com.melnykov.fab.FloatingActionButton;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindString;
import butterknife.ButterKnife;

/**
 * Created by Nurik on 04.04.2017.
 */

public class Comments extends AppCompatActivity {


    @BindString(R.string.comment)
    String comment;

    ListView listview;
    public static ListAdapter commentsAdapter;
    public static ArrayList<HashMap<String,String>> commentsList = new ArrayList<>();
    HashMap<String,String> hashMap = new HashMap<>();
    Requests r = new Requests();
    Intent in;
    String post_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comments);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(comment);

        listview = (ListView) findViewById(R.id.listView);
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.attachToListView(listview);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Comments.this,SendComment.class).putExtra("post_id",post_id).putExtra("parent_id","0"));
            }
        });
        in = getIntent();
        post_id = in.getStringExtra("id");
        commentsAdapter = new ListAdapter(Comments.this,commentsList);
        listview.setAdapter(commentsAdapter);

    }

    public class ListAdapter extends BaseAdapter {

        ArrayList<HashMap<String, String>> commentsList;
        Activity activity;

        public ListAdapter(Activity activity, ArrayList<HashMap<String, String>> commentsList) {
            this.activity = activity;
            this.commentsList = commentsList;
        }


        @Override
        public int getCount() {
            return commentsList.size();
        }

        @Override
        public Object getItem(int location) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }


        class ViewHolder {
           TextView name;
            WebView text;
            CircleImageView avatar;
            LinearLayout relative;
            View divider;
            ImageButton answer;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            final ViewHolder viewHolder;
            if (convertView == null) {
                LayoutInflater inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.item_comments, parent, false);
                viewHolder = new ViewHolder();

                viewHolder.name = (TextView) convertView.findViewById(R.id.name);
                viewHolder.text = (WebView) convertView.findViewById(R.id.text);
                viewHolder.avatar = (CircleImageView) convertView.findViewById(R.id.avatar);
                viewHolder.relative = (LinearLayout) convertView.findViewById(R.id.relative);
                viewHolder.divider = (View) convertView.findViewById(R.id.divider);
                viewHolder.answer = (ImageButton) convertView.findViewById(R.id.answer);

                convertView.setTag(viewHolder);

            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }


            viewHolder.name.setText(commentsList.get(position).get("name"));
            viewHolder.text.loadData(commentsList.get(position).get("text"), "text/html; charset=UTF-8", null);



            if(!commentsList.get(position).get("parent_id").equals("0")) {
                LinearLayout.LayoutParams relativeParams = new LinearLayout.LayoutParams(
                        new LinearLayout.LayoutParams(
                                LinearLayout.LayoutParams.MATCH_PARENT,
                                LinearLayout.LayoutParams.WRAP_CONTENT));
                relativeParams.setMargins(80, 0, 0, 0);
                viewHolder.relative.setLayoutParams(relativeParams);
                viewHolder.relative.requestLayout();
            }else {
                LinearLayout.LayoutParams relativeParams = new LinearLayout.LayoutParams(
                        new LinearLayout.LayoutParams(
                                LinearLayout.LayoutParams.MATCH_PARENT,
                                LinearLayout.LayoutParams.WRAP_CONTENT));
                relativeParams.setMargins(0, 0, 0, 0);
                viewHolder.relative.setLayoutParams(relativeParams);
                viewHolder.relative.requestLayout();
            }

            viewHolder.answer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(Comments.this,SendComment.class).putExtra("post_id",post_id).putExtra("parent_id",commentsList.get(position).get("id")));
                }
            });

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });

            return convertView;
        }
    }


    @Override
    protected void onStart() {
        super.onStart();
        r.getComments(this,commentsList,post_id);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
