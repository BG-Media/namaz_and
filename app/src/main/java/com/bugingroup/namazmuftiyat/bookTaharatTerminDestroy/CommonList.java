package com.bugingroup.namazmuftiyat.bookTaharatTerminDestroy;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import com.bugingroup.namazmuftiyat.R;
import com.bugingroup.namazmuftiyat.utils.Requests;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindArray;
import butterknife.BindString;
import butterknife.ButterKnife;

import static com.bugingroup.namazmuftiyat.MainActivity.whatPage;

public class CommonList extends AppCompatActivity {

    ListView bookListView;
    public static SimpleAdapter bookAdapter;
    public static ArrayList<HashMap<String,String>> bookList = new ArrayList<>();
    Requests r = new Requests();

    @BindArray(R.array.namaz_male)
    String[] namaz_male;

    @BindString(R.string.namaz_book) String namaz_book;
    @BindString(R.string.termin) String termin;
    @BindString(R.string.taharat) String taharat;
    @BindString(R.string.azan) String azan;
    @BindString(R.string.sura_dua) String sura_dua;
    @BindString(R.string.learn_namaz) String learn_namaz;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_common_list);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        bookListView = (ListView) findViewById(R.id.list);

        bookAdapter = new SimpleAdapter(this,bookList,R.layout.common_item,
                new String[]{"title"},new int[]{R.id.common_item_text});
        bookListView.setAdapter(bookAdapter);
        if(whatPage.equals("book")){
            bookList.clear();
            getSupportActionBar().setTitle(namaz_book);
            r.getNamazBook(this,bookAdapter,bookList,"101");
        }else if(whatPage.equals("terms")){
            bookList.clear();
            getSupportActionBar().setTitle(termin);
            r.getNamazBook(this,bookAdapter,bookList,"19");
        }else if(whatPage.equals("taharat")){
            bookList.clear();
            getSupportActionBar().setTitle(taharat);
            r.getSub_categories(this,bookList,"24",bookAdapter);
        }else if(whatPage.equals("azan")){
            bookList.clear();
            getSupportActionBar().setTitle(azan);
            r.getSub_categories(this,bookList,"9",bookAdapter);
        }else if(whatPage.equals("sura_dua")){
            bookList.clear();
            getSupportActionBar().setTitle(sura_dua);
            r.getSub_categories(this,bookList,"18",bookAdapter);
        }else if(whatPage.equals("learn")){
            bookList.clear();
            getSupportActionBar().setTitle(learn_namaz);
            HashMap<String, String> hashMap = new HashMap<>();
            hashMap.put("id", "28");
            hashMap.put("title", namaz_male[0]);
            bookList.add(hashMap);

            hashMap = new HashMap<>();
            hashMap.put("id", "29");
            hashMap.put("title", namaz_male[1]);
            bookList.add(hashMap);

            bookAdapter.notifyDataSetChanged();
        }


        bookListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if(whatPage.equals("sura_dua") || whatPage.equals("taharat") || whatPage.equals("azan")){
                    getSupportActionBar().setTitle(bookList.get(i).get("title"));
                    r.getNamazBook(CommonList.this,bookAdapter,bookList,bookList.get(i).get("id"));
                    whatPage = "sura";
                }else if(whatPage.equals("learn")) {
                    Intent inDesc = new Intent(CommonList.this, ActivityWithTabs.class);
                    inDesc.putExtra("title", bookList.get(i).get("title"));
                    inDesc.putExtra("id", bookList.get(i).get("id"));
                    startActivity(inDesc);
                }else{
                    Intent inDesc = new Intent(CommonList.this, SingleBook.class);
                    inDesc.putExtra("position", i);
                    inDesc.putExtra("type", "book");
                    startActivity(inDesc);
                }
            }
        });
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
