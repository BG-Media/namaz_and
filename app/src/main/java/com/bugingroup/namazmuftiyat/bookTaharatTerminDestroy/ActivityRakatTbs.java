package com.bugingroup.namazmuftiyat.bookTaharatTerminDestroy;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.android.volley.toolbox.StringRequest;
import com.bugingroup.namazmuftiyat.R;
import com.bugingroup.namazmuftiyat.utils.NonSwipeableViewPager;
import com.bugingroup.namazmuftiyat.utils.Requests;
import com.bugingroup.namazmuftiyat.utils.URL_Constant;
import com.bugingroup.namazmuftiyat.utils.volley.AppController;
import com.bugingroup.namazmuftiyat.utils.volley.ImageVolleyRequest;
import com.google.android.youtube.player.YouTubePlayer;
import com.thefinestartist.ytpa.YouTubePlayerActivity;
import com.thefinestartist.ytpa.enums.Orientation;
import com.thefinestartist.ytpa.enums.Quality;
import com.thefinestartist.ytpa.utils.YouTubeThumbnail;
import com.thefinestartist.ytpa.utils.YouTubeUrlParser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.bugingroup.namazmuftiyat.MainActivity.lang;

/**
 * Created by Nurik on 11.04.2017.
 */

public class ActivityRakatTbs extends AppCompatActivity {

    Intent in;

    @BindView(R.id.tabs)
    TabLayout tabLayout;

    @BindView(R.id.pager)
    NonSwipeableViewPager viewPager;

    @BindView(R.id.thumb)
    NetworkImageView thumb;

    Requests r = new Requests();
    String id,title,video;
    int rakat_count;
    JSONArray serviceArray;

    @BindString(R.string.download)
    String download;

    @BindString(R.string.volley_error)
    String volley_error;

    @BindView(R.id.play_btn)
    ImageButton play_btn;

    @BindView(R.id.video_layout)
    RelativeLayout video_layout;

    @BindView(R.id.next_tab)
    TextView next_tab;

    int page=0;

    public static boolean Arab = true,Lan = true, Trans = true;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rakat_tabs);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        in = getIntent();
        id = in.getStringExtra("id");
        title = in.getStringExtra("title");
        video = in.getStringExtra("video");
        ImageLoader imageLoader = ImageVolleyRequest.getInstance(this)
                .getImageLoader();
        if (video.equals("")) {
            video_layout.setVisibility(View.GONE);
        } else {
            video_layout.setVisibility(View.VISIBLE);
            final String vidoeId = YouTubeUrlParser.getVideoId(video);
            thumb.setImageUrl(YouTubeThumbnail.getUrlFromVideoId(vidoeId, Quality.MEDIUM), imageLoader);
            play_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(ActivityRakatTbs.this, YouTubePlayerActivity.class);
                    intent.putExtra(YouTubePlayerActivity.EXTRA_VIDEO_ID, vidoeId);
                    intent.putExtra(YouTubePlayerActivity.EXTRA_PLAYER_STYLE, YouTubePlayer.PlayerStyle.DEFAULT);
                    intent.putExtra(YouTubePlayerActivity.EXTRA_ORIENTATION, Orientation.AUTO);
                    intent.putExtra(YouTubePlayerActivity.EXTRA_SHOW_AUDIO_UI, true);
                    intent.putExtra(YouTubePlayerActivity.EXTRA_HANDLE_ERROR, true);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                }
            });

        }

        rakat_count = Integer.valueOf(in.getStringExtra("rakat_count"));
        getSupportActionBar().setTitle(title);
        for(int i=1;i<=rakat_count;i++){
            tabLayout.addTab(tabLayout.newTab().setText(i+" Ракат"));
        }
        getPostsWithId();

        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                SetPage(tab.getPosition());
            }
            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }
            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });


    }
    @OnClick(R.id.next_tab)
    void nextTab(){
        if(page<rakat_count-1){
            SetPage(page+1);
        }
    }


    public void SetPage(int i){
        page= i;
        viewPager.reMeasureCurrentPage(i);
        viewPager.setCurrentItem(i);
        if(i==rakat_count-1){
            next_tab.setVisibility(View.GONE);
        }else {
            next_tab.setVisibility(View.VISIBLE);
        }
    }
    private void getPostsWithId() {
        final ProgressDialog p = new ProgressDialog(ActivityRakatTbs.this);
        p.setTitle(download);
        p.show();
        StringRequest jsonReq = new StringRequest (Request.Method.GET,
                URL_Constant.DOMAIN+lang+"/posts/"+id,new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if(response!=null){
                    try {
                        JSONObject jsonObject =  new JSONObject(response);
                        if(jsonObject.getBoolean("success")) {
                            serviceArray = jsonObject.getJSONArray("results");
                            viewPager.setAdapter(new RakatPagerAdapter(getSupportFragmentManager(), tabLayout.getTabCount(),serviceArray));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                p.dismiss();
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                Toast.makeText(ActivityRakatTbs.this,volley_error,Toast.LENGTH_SHORT).show();
                p.dismiss();
                VolleyLog.d("Error: " + error.getMessage());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json");
                return headers;
            }
        };
        AppController.getInstance().addToRequestQueue(jsonReq);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.filter:
                final Dialog dialog = new Dialog(ActivityRakatTbs.this);
                dialog.setContentView(R.layout.filter);
                dialog.setTitle("Фильтр");

                final CheckBox arab= (CheckBox) dialog.findViewById(R.id.arab);
                final CheckBox lan= (CheckBox) dialog.findViewById(R.id.lan);
                final CheckBox trans= (CheckBox) dialog.findViewById(R.id.trans);
                arab.setChecked(Arab);
                lan.setChecked(Lan);
                trans.setChecked(Trans);
                Button no = (Button) dialog.findViewById(R.id.no);
                no.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                Button ok = (Button) dialog.findViewById(R.id.yes);
                ok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Arab = arab.isChecked();
                        Lan = lan.isChecked();
                        Trans = trans.isChecked();
                        getPostsWithId();
                        dialog.dismiss();

                    }
                });
                dialog.show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
