package com.bugingroup.namazmuftiyat.bookTaharatTerminDestroy;

import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.bugingroup.namazmuftiyat.R;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import static com.bugingroup.namazmuftiyat.SubCategory.subList;
import static com.bugingroup.namazmuftiyat.bookTaharatTerminDestroy.CommonList.bookList;
import static com.bugingroup.namazmuftiyat.useful.UsefulMaterial.usefulyList;
import static com.bugingroup.namazmuftiyat.utils.URL_Constant.CSS_style;
import static com.bugingroup.namazmuftiyat.utils.URL_Constant.IMAGE;

public class SingleBook extends AppCompatActivity {

    TextView created_at;
    TextView views;
    TextView comments;
    TextView audio_time;
    WebView webView;
    LinearLayout comment_layout;
    RelativeLayout audio_layout;
    int pos;
    String type;
    ArrayList<HashMap<String,String>> list ;
    private ImageButton audio_btn;
    private MediaPlayer mediaPlayer;
    private SeekBar seekBar;

    private final Handler handler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_single);
        pos = getIntent().getIntExtra("position",0);
        type = getIntent().getStringExtra("type");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        if(type.equals("book")){
            list =  bookList;
        }else if(type.equals("usefuly")){
            list =  usefulyList;
        }else if(type.equals("subList")){
            list =  subList;
        }
        getSupportActionBar().setTitle(list.get(pos).get("title"));
        created_at = (TextView) findViewById(R.id.created_at);
        views = (TextView) findViewById(R.id.views);
        comments = (TextView) findViewById(R.id.comments);
        audio_time = (TextView) findViewById(R.id.audio_time);
        webView = (WebView) findViewById(R.id.webview);
//        webView.setInitialScale(150);
        WebSettings settings = webView.getSettings();
        settings.setDefaultTextEncodingName("utf-8");
        comment_layout = (LinearLayout) findViewById(R.id.comment_layout);
        audio_layout = (RelativeLayout) findViewById(R.id.audio_layout);
        if(list.get(pos).get("audio").equals("false") || list.get(pos).get("audio").equals("")){
            audio_layout.setVisibility(View.GONE);
        }else {
            audio_layout.setVisibility(View.VISIBLE);
        }

        comment_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SingleBook.this,Comments.class).putExtra("id",list.get(pos).get("id")));
            }
        });

        initDescription(list);

        audio_btn = (ImageButton) findViewById(R.id.audio_btn);
        audio_btn.setBackgroundResource(R.drawable.audio_play);
        audio_btn.setOnClickListener(new View.OnClickListener()
        {@Override public void onClick(View v) {
            buttonClick();
        }});
        try {
            String url = list.get(pos).get("audio"); // your URL here
            mediaPlayer = new MediaPlayer();
            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            mediaPlayer.setDataSource(url);
            mediaPlayer.prepare(); // might take long! (for buffering, etc)
        } catch (IOException e) {
            e.printStackTrace();
        }


        seekBar = (SeekBar) findViewById(R.id.SeekBar01);
        seekBar.setMax(mediaPlayer.getDuration());
        seekBar.setOnTouchListener(new View.OnTouchListener() {@Override public boolean onTouch(View v, MotionEvent event) {
            seekChange(v);
            return false; }
        });
        setProgressText();
    }

    private void initDescription(ArrayList<HashMap<String,String>> list){


        created_at.setText(list.get(pos).get("created_at"));
        views.setText(list.get(pos).get("views"));
        comments.setText(list.get(pos).get("comments"));


      if(!type.equals("usefuly")){
          String content =list.get(pos).get("description")+list.get(pos).get("content")+list.get(pos).get("content_2");
          content = content.replaceAll( "src=\"", "src=\""+IMAGE );
          Log.d("ASASASASIIII",content);
              String html = CSS_style+content;
            webView.loadData(html, "text/html;  charset=UTF-8", null);
        }else {
          Log.d("DFDFDFDFDF",list.get(pos).get("content")+"**");
          String content =list.get(pos).get("content");
          content = content.replaceAll( "src=\"", "src=\""+IMAGE );
          Log.d("ASASASASIIII",content);

          String html = CSS_style+content;
          webView.loadData(html, "text/html; charset=utf-8", "utf-8");
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    // This is event handler thumb moving event
    private void seekChange(View v){
        if(mediaPlayer.isPlaying()){
            SeekBar sb = (SeekBar)v;
            mediaPlayer.seekTo(sb.getProgress());
        }
    }

    private void buttonClick(){
        if (!mediaPlayer.isPlaying()) {
            audio_btn.setBackgroundResource(R.drawable.audio_pause);
            try{
                mediaPlayer.start();

                startPlayProgressUpdater();

            }catch (IllegalStateException e) {
                mediaPlayer.pause();
            }
        }else {
            audio_btn.setBackgroundResource(R.drawable.audio_play);
            mediaPlayer.pause();
        }
    }
    public void startPlayProgressUpdater() {
        seekBar.setProgress(mediaPlayer.getCurrentPosition());

        if (mediaPlayer.isPlaying()) {
            Runnable notification = new Runnable() {
                public void run() {
                    startPlayProgressUpdater();
                    setProgressText();
                }
            };
            handler.postDelayed(notification,1000);
        }else{
            mediaPlayer.pause();
            audio_btn.setBackgroundResource(R.drawable.audio_play);
            seekBar.setProgress(0);
        }
    }

    protected void setProgressText() {

        final int HOUR = 60*60*1000;
        final int MINUTE = 60*1000;
        final int SECOND = 1000;

        int durationInMillis = mediaPlayer.getDuration();
        int curVolume = mediaPlayer.getCurrentPosition();

        int durationHour = durationInMillis/HOUR;
        int durationMint = (durationInMillis%HOUR)/MINUTE;
        int durationSec = (durationInMillis%MINUTE)/SECOND;

        int currentHour = curVolume/HOUR;
        int currentMint = (curVolume%HOUR)/MINUTE;
        int currentSec = (curVolume%MINUTE)/SECOND;

        if(durationHour>0){
            audio_time.setText(String.format("%02d:%02d:%02d/%02d:%02d:%02d", currentHour,currentMint,currentSec, durationHour,durationMint,durationSec));
           }else{
            audio_time.setText(String.format("%02d:%02d/%02d:%02d",currentMint,currentSec, durationMint,durationSec));
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
       // mediaPlayer.stop();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mediaPlayer.pause();

    }
}
