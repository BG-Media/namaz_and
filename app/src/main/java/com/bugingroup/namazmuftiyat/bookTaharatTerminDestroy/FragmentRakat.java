package com.bugingroup.namazmuftiyat.bookTaharatTerminDestroy;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import com.bugingroup.namazmuftiyat.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Nurik on 11.04.2017.
 */

@SuppressLint("ValidFragment")
public class FragmentRakat extends Fragment {

    int id;
    JSONArray serviceArray;
    ArrayList<HashMap<String,String>> itemList = new ArrayList<>();

    @BindView(R.id.list)
    ListView listView;

    public FragmentRakat(int id, JSONArray serviceArray) {
        this.id = id;
        this.serviceArray = serviceArray;
    }

    @Override
    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container, final Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_common_list, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(final View view, @Nullable final Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        SimpleAdapter dapter = new SimpleAdapter(getActivity(),itemList,R.layout.common_item,
                new String[]{"title"},new int[]{R.id.common_item_text});
        listView.setAdapter(dapter);

        Log.d("AAAAA",serviceArray+"");
        try {
            for (int i=0;i<serviceArray.length();i++){
                JSONObject service = serviceArray.getJSONObject(i);
                if(service.getInt("id")==id){
                   JSONArray subSubCats = service.getJSONArray("subSubCats");
                    for(int j=0;j<subSubCats.length();j++){
                        JSONObject service2 = subSubCats.getJSONObject(j);
                        HashMap<String, String> hashMap = new HashMap<>();
                        hashMap.put("id", service2.getString("id"));
                        hashMap.put("title", service2.getString("title"));
                        hashMap.put("video", service2.getString("video"));
                        hashMap.put("rakat_count", service2.getString("rakat_count"));
                        itemList.add(hashMap);
                    }
                }
            }
            dapter.notifyDataSetChanged();
        } catch (JSONException e) {
        e.printStackTrace();
        }


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
               startActivity(new Intent(getActivity(),ActivityRakatTbs.class)
                       .putExtra("id",itemList.get(i).get("id"))
                       .putExtra("title",itemList.get(i).get("title"))
                       .putExtra("video",itemList.get(i).get("video"))
                       .putExtra("rakat_count",itemList.get(i).get("rakat_count"))
                       );
            }
        });
    }
}

