package com.bugingroup.namazmuftiyat.bookTaharatTerminDestroy;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bugingroup.namazmuftiyat.R;
import com.bugingroup.namazmuftiyat.utils.Requests;
import com.bugingroup.namazmuftiyat.utils.URL_Constant;
import com.bugingroup.namazmuftiyat.utils.volley.AppController;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindString;
import butterknife.ButterKnife;

import static com.bugingroup.namazmuftiyat.MainActivity.lang;

/**
 * Created by Nurik on 05.04.2017.
 */

public class SendComment extends AppCompatActivity {

    EditText name,email,text;
    Button send;
    Requests r;

    @BindString(R.string.send_comment) String send_comment;
    @BindString(R.string.valid_email) String valid_email;
    @BindString(R.string.valid_empty) String valid_empty;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_comments);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(send_comment);


        name = (EditText) findViewById(R.id.name);
        email = (EditText) findViewById(R.id.email);
        text = (EditText) findViewById(R.id.text);
        send = (Button) findViewById(R.id.send);
        r = new Requests();
        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(name.getText().length()>0 && email.getText().length()>0 && text.getText().length()>0){
                  if(URL_Constant.checkEmail(email)){
                      StringRequest postRequest = new StringRequest(Request.Method.POST, URL_Constant.DOMAIN+lang+"/addComment",
                              new Response.Listener<String>()
                              {
                                  @Override
                                  public void onResponse(String response) {
                                      // response
                                      finish();
                                  }
                              },
                              new Response.ErrorListener()
                              {
                                  @Override
                                  public void onErrorResponse(VolleyError error) {
                                      // error
                                      Log.d("Error.Response", error+"");
                                  }
                              }
                      ) {
                          @Override
                          protected Map<String, String> getParams()
                          {
                              Map<String, String>  params = new HashMap<String, String>();
                              params.put("name", name.getText().toString());
                              params.put("email",email.getText().toString());
                              params.put("text",text.getText().toString());
                              params.put("post_id",getIntent().getStringExtra("post_id"));
                              params.put("publish","1");
                              params.put("parent_id",getIntent().getStringExtra("parent_id"));

                              return params;
                          }
                      };
                      AppController.getInstance().addToRequestQueue(postRequest,
                              "TravelChefs");

                  }else {
                      Toast.makeText(SendComment.this, valid_email, Toast.LENGTH_SHORT).show();
                  }
                }else {
                    Toast.makeText(SendComment.this, valid_empty, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}