package com.bugingroup.namazmuftiyat;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.TextView;

import com.bugingroup.namazmuftiyat.utils.DatabaseHelper;
import com.bugingroup.namazmuftiyat.utils.Zakat;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Nurik on 17.04.2017.
 */

public class ActivityRekvizitPay extends AppCompatActivity {

    DatabaseHelper db;
    String price= "";

    @BindView(R.id.text)
    TextView text;

    @BindString(R.string.help)
    String help;
    @BindString(R.string.are_you_paid)
    String are_you_paid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rekvizit);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(help);
        db = new DatabaseHelper(getApplicationContext());
        price = getIntent().getStringExtra("price");
        text.setText(are_you_paid+price+" тг?");
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @OnClick(R.id.yes)
    void Yes(){
        Zakat zakat = new Zakat(price);
        db.createPay(zakat);
        db.closeDB();
        finish();
    }

    @OnClick(R.id.no)
    void No(){
        finish();
    }

}
