package com.bugingroup.namazmuftiyat.Fragment;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.bugingroup.namazmuftiyat.OpenPDF;
import com.bugingroup.namazmuftiyat.R;
import com.bugingroup.namazmuftiyat.utils.URL_Constant;
import com.bugingroup.namazmuftiyat.utils.volley.AppController;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.bugingroup.namazmuftiyat.MainActivity.ASRM;
import static com.bugingroup.namazmuftiyat.MainActivity.HIGHET;
import static com.bugingroup.namazmuftiyat.MainActivity.M1;
import static com.bugingroup.namazmuftiyat.MainActivity.M2;
import static com.bugingroup.namazmuftiyat.MainActivity.M3;
import static com.bugingroup.namazmuftiyat.MainActivity.M4;
import static com.bugingroup.namazmuftiyat.MainActivity.M5;
import static com.bugingroup.namazmuftiyat.MainActivity.M6;
import static com.bugingroup.namazmuftiyat.MainActivity.METHOD;

import static com.bugingroup.namazmuftiyat.GetNamazTime.LAT;
import static com.bugingroup.namazmuftiyat.GetNamazTime.LON;
/**
 * Created by nurbaqyt on 15.05.17.
 */

public class Fragment3 extends Fragment {

    @BindView(R.id.date)
    TextView date;
    @BindView(R.id.time_zone)
    TextView time_zone;
    @BindView(R.id.from)
    TextView from;
    @BindView(R.id.to)
    TextView to;


    int year ;
    int month;
    int day  ;
    int click=0;
    String MINUTES;
    int MONTH;

    boolean _areLecturesLoaded = false;


    @Override
    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container, final Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment3, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(final View view, @Nullable final Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


         MINUTES = M1+","+M2+","+M3+","+M4+","+M5+","+M6;
        Calendar c = Calendar.getInstance();
        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH)+1;
        day  = c.get(Calendar.DAY_OF_MONTH);

        date.setText(day+"."+month+"."+year);
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser && !_areLecturesLoaded ) {
            getUTC();
            _areLecturesLoaded = true;
        }
    }


    @OnClick(R.id.date)
    void DataClick(){
        click = 1;
        DatePickerDialog datePicker = new DatePickerDialog(getActivity(),datePickerListener,year,month,day);
        datePicker.setCancelable(false);
        datePicker.show();
    }
    @OnClick(R.id.from)
    void fromClick(){
        click = 2;
        DatePickerDialog datePicker = new DatePickerDialog(getActivity(),datePickerListener,year,month,day);
        datePicker.setCancelable(false);
        datePicker.show();
    }
    @OnClick(R.id.to)
    void toClick(){
        click = 3;
        DatePickerDialog datePicker = new DatePickerDialog(getActivity(),datePickerListener,year,month,day);
        datePicker.setCancelable(false);
        datePicker.show();
    }



    private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {
            year = selectedYear;
            month = selectedMonth + 1;
            day = selectedDay;
            switch (click){
                case 1: date.setText(day + "." + month + "." + year);break;
                case 2:  from.setText(day + "." + month + "." + year);break;
                case 3:  to.setText(day + "." + month + "." + year);break;
            }
        }
    };


    public void getUTC(){
        Log.d("AAAAAAA","http://api.geonames.org/timezoneJSON?lat="+LAT+"&lng="+LON+"&username=adiletmaks");
        StringRequest jsonReq = new StringRequest (Request.Method.GET,
                "http://api.geonames.org/timezoneJSON?lat="+LAT+"&lng="+LON+"&username=adiletmaks",new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if(response!=null){
                    try {
                        JSONObject object = new JSONObject(response);
                        int t  = object.getInt("rawOffset");
                        if(t<0)time_zone.setText("UTC "+t);
                        else time_zone.setText("UTC +"+t);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {


                Toast.makeText(getActivity(),getResources().getString(R.string.volley_error),Toast.LENGTH_SHORT).show();
                VolleyLog.d("Error: " + error.getMessage());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json");
                return headers;
            }

        };
        AppController.getInstance().addToRequestQueue(jsonReq);
    }


    @OnClick(R.id.on_month)
    void on_monthClick(){
        MONTH = month;
        getPDF();
    }
    @OnClick(R.id.on_year)
    void on_yearClick(){
        MONTH = 0;
        getPDF();
    }



    public void getPDF(){
        final ProgressDialog p = new ProgressDialog(getActivity());
        p.setTitle("");
        p.setTitle(getResources().getString(R.string.download));
        p.show();
        Log.d("API3",URL_Constant.URL_GET_TIME+LAT+"/"+LON+"/"+year);
        StringRequest jsonReq = new StringRequest (Request.Method.POST,
                URL_Constant.URL_GET_TIME+LAT+"/"+LON+"/"+year,new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                if(response!=null){
                    try {
                        Log.d("response",response);
                        p.dismiss();
                        JSONObject object = new JSONObject(response);
                        Intent intent = new Intent(getActivity(), OpenPDF.class);
                        intent.putExtra("pdf",object.getString("file"));
                        startActivity(intent);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getActivity(),getResources().getString(R.string.volley_error),Toast.LENGTH_SHORT).show();
                VolleyLog.d("Error: " + error.getMessage());
                p.dismiss();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("method",METHOD+"");
                params.put("asr",ASRM+"");
                params.put("higherLatitudes",HIGHET+"");
                params.put("minutes",MINUTES);
                params.put("month",MONTH+"");
                params.put("pdf","1");
                params.put("day",day+"");

                Log.d("aaaaaa",METHOD+"/"+ASRM+"/"+HIGHET+"/"+MINUTES+"/"+MONTH+"/"+day+"/");
                return params;
            }

        };
        AppController.getInstance().addToRequestQueue(jsonReq);
    }


}
