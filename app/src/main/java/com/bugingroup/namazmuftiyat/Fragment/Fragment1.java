package com.bugingroup.namazmuftiyat.Fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.bugingroup.namazmuftiyat.R;

import butterknife.BindArray;
import butterknife.BindView;
import butterknife.ButterKnife;

import static com.bugingroup.namazmuftiyat.MainActivity.ASRM;
import static com.bugingroup.namazmuftiyat.MainActivity.HIGHET;
import static com.bugingroup.namazmuftiyat.MainActivity.M1;
import static com.bugingroup.namazmuftiyat.MainActivity.M2;
import static com.bugingroup.namazmuftiyat.MainActivity.M3;
import static com.bugingroup.namazmuftiyat.MainActivity.M4;
import static com.bugingroup.namazmuftiyat.MainActivity.M5;
import static com.bugingroup.namazmuftiyat.MainActivity.M6;
import static com.bugingroup.namazmuftiyat.MainActivity.METHOD;

/**
 * Created by nurbaqyt on 15.05.17.
 */

public class Fragment1 extends Fragment {

    @BindView(R.id.method)
    Spinner method;

    @BindArray(R.array.method_array)
    String[] method_array;

    @BindView(R.id.method_asr)
    Spinner method_asr;

    @BindArray(R.array.asr_array)
    String[] asr_array;

    @BindView(R.id.higherLatitudes)
    Spinner higherLatitudes;

    @BindArray(R.array.higher_array)
    String[] higher_array;

    @BindView(R.id.fajir)
    Spinner fajir;
    @BindView(R.id.voshod)
    Spinner voshod;

    @BindView(R.id.zuhr)
    Spinner zuhr;

    @BindView(R.id.asr)
    Spinner asr;

    @BindView(R.id.magrib)
    Spinner magrib;

    @BindView(R.id.isha)
    Spinner isha;

    int [] Minute = new int[61];
    String [] minute = new String[61];



    @Override
    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container, final Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment1, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(final View view, @Nullable final Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        for (int i=0;i<61;i++){
            Minute[i] = i-30;
            minute[i] = i-30 + " мин.";
        }


        ArrayAdapter<String> methodArrayAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, method_array);
        methodArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        method.setAdapter(methodArrayAdapter);

        ArrayAdapter<String> asrArrayAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, asr_array);
        asrArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        method_asr.setAdapter(asrArrayAdapter);

        ArrayAdapter<String> higherArrayAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, higher_array);
        higherArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        higherLatitudes.setAdapter(higherArrayAdapter);

        ArrayAdapter<String> fajirArrayAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, minute);
        fajirArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        fajir.setAdapter(fajirArrayAdapter);
        fajir.setSelection(30);

        ArrayAdapter<String> voshodArrayAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, minute);
        voshodArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        voshod.setAdapter(fajirArrayAdapter);
        voshod.setSelection(30);

        ArrayAdapter<String> zuhrArrayAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, minute);
        zuhrArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        zuhr.setAdapter(fajirArrayAdapter);
        zuhr.setSelection(30);

        ArrayAdapter<String> aasrArrayAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, minute);
        aasrArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        asr.setAdapter(fajirArrayAdapter);
        asr.setSelection(30);

        ArrayAdapter<String> magribArrayAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, minute);
        magribArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        magrib.setAdapter(fajirArrayAdapter);
        magrib.setSelection(30);

        ArrayAdapter<String> ishaArrayAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, minute);
        ishaArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        isha.setAdapter(fajirArrayAdapter);
        isha.setSelection(30);

        ////////////////////////////////////////////////////
        method.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0: METHOD = 8; Spinnerenabled(false); break;
                    case 1: METHOD = 3; Spinnerenabled(true); break;
                    case 2: METHOD = 2; Spinnerenabled(true); break;
                    case 3: METHOD = 4; Spinnerenabled(true); break;
                    case 4: METHOD = 1; Spinnerenabled(true); break;
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        /////////////////////////////////////////////////////
        method_asr.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0: ASRM = 1; break;
                    case 1: ASRM = 0; break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        /////////////////////////////////////////////////////
        higherLatitudes.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0: HIGHET = 3; break;
                    case 1: HIGHET = 1; break;
                    case 2: HIGHET = 2; break;
                    case 3: HIGHET = 0; break;
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        /////////////////////////////////////////////////////
        fajir.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                M1 = Minute[position];
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        /////////////////////////////////////////////////////
        voshod.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                M2 = Minute[position];
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        /////////////////////////////////////////////////////
        zuhr.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                M3 = Minute[position];
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        /////////////////////////////////////////////////////
        asr.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                M4 = Minute[position];
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        /////////////////////////////////////////////////////
        magrib.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                M5 = Minute[position];
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        /////////////////////////////////////////////////////
        isha.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                M6 = Minute[position];
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

    }


    public void Spinnerenabled(boolean b){
        ((Spinner) method_asr).getSelectedView().setEnabled(b);
        method_asr.setEnabled(b);
        if(!b) {
            method_asr.setSelection(0);
            higherLatitudes.setSelection(0);
        }
        ((Spinner) higherLatitudes).getSelectedView().setEnabled(b);
        higherLatitudes.setEnabled(b);
    }
}
