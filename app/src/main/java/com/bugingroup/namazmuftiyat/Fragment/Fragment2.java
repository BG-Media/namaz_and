package com.bugingroup.namazmuftiyat.Fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.bugingroup.namazmuftiyat.MainActivity;
import com.bugingroup.namazmuftiyat.R;
import com.bugingroup.namazmuftiyat.utils.volley.AppController;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.bugingroup.namazmuftiyat.GetNamazTime.LAT;
import static com.bugingroup.namazmuftiyat.GetNamazTime.LON;
import static com.bugingroup.namazmuftiyat.SplashScreen.PREFS_NAME;

/**
 * Created by nurbaqyt on 15.05.17.
 */

public class Fragment2 extends Fragment {

    ArrayList<HashMap<String,String>> arrayList;
    SharedPreferences settings;
    String lang;

    @BindView(R.id.lat_et)
    EditText lat_et;

    @BindView(R.id.lon_et)
    EditText lon_et;

    @BindView(R.id.cordinat)
    LinearLayout cordinat;
    @BindView(R.id.relativeLayout_city)
    RelativeLayout relativeLayout_city;

    @BindView(R.id.setcordinat)
    CheckBox setcordinat;

    @BindView(R.id.city)
    TextView city;

    ListAdapter adapter;

    @BindView(R.id.mapView)
    MapView mMapView;

    private GoogleMap googleMap;
    @Override
    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container, final Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment2, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(final View view, @Nullable final Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        settings = getActivity().getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        lang = settings.getString("language","ru");

        if(setcordinat.isChecked()){
            cordinat.setVisibility(View.VISIBLE);
        }else {
            cordinat.setVisibility(View.GONE);
        }

        setcordinat.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    cordinat.setVisibility(View.VISIBLE);
                }else {
                    cordinat.setVisibility(View.GONE);
                }
            }
        });

        getCity();
        mMapView.onCreate(savedInstanceState);
        googleMap = mMapView.getMap();
        //mMapView.onResume();
        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }

        googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                setMapMarker(latLng);
            }
        });


//        city.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//
//
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//
//            }
//        });
        LatLng latitube = new LatLng(MainActivity.latitude,MainActivity.longitude);
        setMapMarker(latitube);



        relativeLayout_city.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder adb = new AlertDialog.Builder(getActivity());
                LayoutInflater inflater = getActivity().getLayoutInflater();
                View dialogView = inflater.inflate(R.layout.alert_listview, null);
                adb.setView(dialogView);
                adb.setNegativeButton("Кері", null);
                final DialogInterface dialog;

                ListView listView = (ListView)dialogView.findViewById(R.id.listView);
                listView.setAdapter(adapter);
                adb.create();
                dialog = adb.show();

                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        String[] parts = arrayList.get(position).get("coords").split("\\,");
                        double lat = Double.parseDouble(parts[0]);
                        double lon = Double.parseDouble(parts[1]);
                        dialog.dismiss();
                        LatLng latitube = new LatLng(lat,lon);
                        setMapMarker(latitube);
                    }
                });
            }
        });

        lat_et.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(lat_et.getText().toString().length()>0 && lon_et.getText().toString().length()>0){
                    double lat = Double.parseDouble(lat_et.getText().toString());
                    double lon = Double.parseDouble(lon_et.getText().toString());
                    LatLng latitube = new LatLng(lat,lon);
                    setMapMarker(latitube);
                }
            }
            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        lon_et.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if( lat_et.getText().toString().length()>0 && lon_et.getText().toString().length()>0){
                    double lat = Double.parseDouble(lat_et.getText().toString());
                    double lon = Double.parseDouble(lon_et.getText().toString());
                    LatLng latitube = new LatLng(lat,lon);
                    setMapMarker(latitube);
                }
            }
            @Override
            public void afterTextChanged(Editable s) {
            }
        });


    }

    private void setMapMarker(final LatLng latlng ) {
        LAT = latlng.latitude;
        LON = latlng.longitude;
        Log.d("AAAAAAA1111", LAT+" "+LON);
        String cityName = getCityName(latlng);
        if(cityName =="Қаланы таңдау") {
            Toast.makeText(getActivity(), "Жақындатып таңдаңыз!", Toast.LENGTH_SHORT).show();
            city.setText("");
            city.setHint(cityName);
        }
        else {
            city.setText(cityName);
        }
        mMapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap mMap) {
                googleMap = mMap;
                googleMap.clear();
                googleMap.addMarker(new MarkerOptions().position(latlng).title(""));
                CameraPosition cameraPosition = new CameraPosition.Builder().target(latlng).zoom(9).build();
                googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
            }
        });
    }

    public String getCityName(LatLng latlng ){
        Geocoder gcd = new Geocoder(getActivity(), Locale.getDefault());
        List<android.location.Address> addresses = null;
        try {
            addresses = gcd.getFromLocation(latlng.latitude, latlng.longitude, 1);
        } catch (IOException e) {
            e.printStackTrace();
        }
        String cityName = addresses.get(0).getLocality();
        if(cityName==null){
            cityName = "Қаланы таңдау";
        } else{
            switch (cityName){
                case "Astana":
                    cityName="Астана";
                    break;
                case "Туркестан":
                    cityName="Түркістан";
                    break;
                case "Almaty":
                    cityName="Алматы";
                    break;
                case "Karagandy":
                    cityName="Қарағанды";
                    break;
                case "Караганды":
                    cityName="Қарағанды";
                    break;
                case "Aktobe":
                    cityName="Ақтөбе";
                    break;
                case "Актобе":
                    cityName="Ақтөбе";
                    break;
                case "Atyrau":
                    cityName="Атырау";
                    break;
                case "Aktau":
                    cityName="Ақтау";
                    break;
                case "Актау":
                    cityName="Ақтау";
                    break;
                case "Kokshetau":
                    cityName="Көкшетау";
                    break;
                case "Кокшетау":
                    cityName="Көкшетау";
                    break;
                case "Petropavlsk":
                    cityName="Петропавл";
                    break;
                case "Shymkent":
                    cityName="Шымкент";
                    break;
                case "Taraz":
                    cityName="Тараз";
                    break;
                case "Pavlodar":
                    cityName="Павлодар";
                    break;
                case "Ust'-Kamenogorsk":
                    cityName="Өскемен";
                    break;
                case "Усть-Каменогорск":
                    cityName="Өскемен";
                    break;
                case "Semey":
                    cityName="Семей";
                    break;
                case "Уральск":
                    cityName="Орал";
                    break;
                case "Uralsk":
                    cityName="Орал";
                    break;
                case "Костанай":
                    cityName="Қостанай";
                    break;
                case "Kostanay":
                    cityName="Қостанай";
                    break;
                case "Kyzylorda":
                    cityName="Қызылорда";
                    break;
                case "Кызылорда":
                    cityName="Қызылорда";
                    break;

            }
        }
            return cityName;
    }

    public void getCity(){
        arrayList = new ArrayList<>();
        StringRequest jsonReq = new StringRequest (Request.Method.GET,
                "http://namaz.muftyat.kz/namaz_times/regions.json",new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if(response!=null){
                    try {
                        JSONArray jsonArray = new JSONArray(response);
                        for (int i=0;i<jsonArray.length();i++){
                            JSONObject jsonObject = (JSONObject) jsonArray.get(i);
                            if(jsonObject.getInt("parent_id")!=0) {
                                HashMap<String, String> hashMap = new HashMap<>();
                                hashMap.put("id", jsonObject.getString("id"));
                                hashMap.put("coords", jsonObject.getString("coords"));
                                hashMap.put("title",lang.equals("ru") ?jsonObject.getString("title_ru") :jsonObject.getString("title_kk"));
                                arrayList.add(hashMap);
                            }
                        }
                        adapter = new ListAdapter(getActivity(),arrayList);
                       // city.setAdapter(adapter);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {


                Toast.makeText(getActivity(),getResources().getString(R.string.volley_error),Toast.LENGTH_SHORT).show();
                VolleyLog.d("Error: " + error.getMessage());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json");
                return headers;
            }

        };
        AppController.getInstance().addToRequestQueue(jsonReq);
    }



    public class ListAdapter extends BaseAdapter {

        ArrayList<HashMap<String, String>> arrayList;
        Activity activity;

        public ListAdapter(Activity activity, ArrayList<HashMap<String, String>> arrayList) {
            this.activity = activity;
            this.arrayList = arrayList;
        }


        @Override
        public int getCount() {
            return arrayList.size();
        }

        @Override
        public Object getItem(int location) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }


        class ViewHolder {
            TextView city;

        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            final ViewHolder viewHolder;
            if (convertView == null) {
                LayoutInflater inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.item_city, parent, false);
                viewHolder = new ViewHolder();

                viewHolder.city = (TextView) convertView.findViewById(R.id.city);

                convertView.setTag(viewHolder);

            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }

            viewHolder.city.setText(arrayList.get(position).get("title"));

            return convertView;
        }
    }
    @Override
    public void onResume() {
        super.onResume();
        mMapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mMapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }


}
