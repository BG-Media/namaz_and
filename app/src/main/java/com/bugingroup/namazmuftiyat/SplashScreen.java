package com.bugingroup.namazmuftiyat;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;

import java.util.Locale;

/**
 * Created by Nurik on 13.04.2017.
 */

public class SplashScreen extends Activity {
    public static final String PREFS_NAME = "MyPrefsFile";
    Handler hand = new Handler();
    private static final long INTERVAL = 1500;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        hand.postDelayed(run1, INTERVAL);
    }
    Runnable run1 = new Runnable() {
        @Override
        public void run() {
            SharedPreferences settings = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
            ChangeLanguage(settings.getString("language","kk"));
        }
    };

    public void ChangeLanguage(String loc){
        Locale locale = new Locale(loc);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config,
                getBaseContext().getResources().getDisplayMetrics());
        startActivity(new Intent(SplashScreen.this,MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
        finish();
    }
}
