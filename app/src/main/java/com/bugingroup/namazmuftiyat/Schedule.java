package com.bugingroup.namazmuftiyat;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.bugingroup.namazmuftiyat.utils.CalculatePrayTime;
import com.bugingroup.namazmuftiyat.utils.NonScrollListView;

import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import io.blackbox_vision.materialcalendarview.view.CalendarView;

import static com.bugingroup.namazmuftiyat.MainActivity.ASRM;
import static com.bugingroup.namazmuftiyat.MainActivity.HIGHET;
import static com.bugingroup.namazmuftiyat.MainActivity.M1;
import static com.bugingroup.namazmuftiyat.MainActivity.M2;
import static com.bugingroup.namazmuftiyat.MainActivity.M3;
import static com.bugingroup.namazmuftiyat.MainActivity.M4;
import static com.bugingroup.namazmuftiyat.MainActivity.M5;
import static com.bugingroup.namazmuftiyat.MainActivity.M6;
import static com.bugingroup.namazmuftiyat.MainActivity.METHOD;
import static com.bugingroup.namazmuftiyat.MainActivity.addingTimeArr;
import static com.bugingroup.namazmuftiyat.MainActivity.latitude;
import static com.bugingroup.namazmuftiyat.MainActivity.longitude;
import static com.bugingroup.namazmuftiyat.MainActivity.timezone;

public class Schedule extends AppCompatActivity {


    @BindView(R.id.calendar_view)
    CalendarView calendarView;

    ArrayList<HashMap<String,String>> namaztimeList = new ArrayList<>() ;
    ListAdapter adapter;
    @BindView(R.id.namaz_time)
    NonScrollListView listView;
    int k=100;
    int YEAR,MONTH,DAY,dd;

    @BindString(R.string.namaz_time) String namaz_time;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_schedule);
        ButterKnife.bind(this);
        getSupportActionBar().setTitle(namaz_time);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        listView.setEnabled(false);


        Calendar cal = Calendar.getInstance();
        Date now = new Date();
        cal.setTime(now);

        YEAR = cal.get(Calendar.YEAR);
        MONTH = cal.get(Calendar.MONTH)+1;
        DAY = cal.get(Calendar.DATE);
        dd = cal.get(Calendar.DATE);

        getTime(YEAR,MONTH,DAY);

        calendarView.update(Calendar.getInstance(Locale.getDefault()));
        calendarView.setOnDateClickListener(new CalendarView.OnDateClickListener() {
            @Override
            public void onDateClick(@NonNull Date date) {
                Calendar cal = Calendar.getInstance();
                cal.setTime(date);

                YEAR = cal.get(Calendar.YEAR);
                MONTH = cal.get(Calendar.MONTH)+1;
                DAY = cal.get(Calendar.DATE);
                if(dd!=DAY){
                    k=100;
                }
                getTime(YEAR,MONTH,DAY);
            }
        });


    }



    public void getTime(int y, int m ,int d) {
        namaztimeList .clear();
        // Retrive lat, lng using location API

        CalculatePrayTime prayers = new CalculatePrayTime();
        prayers.setTimeFormat(prayers.Time24);
        prayers.setCalcMethod(METHOD);
        prayers.setAsrJuristic(ASRM);
        prayers.setAdjustHighLats(HIGHET);
        int[] offsets = new int[]{M1, M2, M3, M4,0, M5, M6}; // {Fajr,Sunrise,Dhuhr,Asr,Sunset,Maghrib,Isha}
        if(METHOD==8){
            if (latitude < 48) {
                offsets  = new int[]{M1, M2-3, M3+3, M4+3,0, M5+3, M6};
            } else {
                offsets  = new int[]{M1, M2-5, M3+5, M4+5,0, M5+5, M6};
            }
        }
        prayers.tune(offsets);

        SimpleDateFormat df = new SimpleDateFormat("HH:mm");
        Calendar calculateCalendar = Calendar.getInstance();

        Calendar cal = Calendar.getInstance();
        Date now = new Date();
        cal.setTime(now);


        ArrayList prayerTimes = prayers.getDatePrayerTimes(y, m, d, latitude, longitude, timezone);
        ArrayList prayerNames = prayers.getTimeNames();

        for (int i = 0; i < prayerTimes.size(); i++) {
            try {
                Date dateForCalculation = df.parse(prayerTimes.get(i) + "");
                calculateCalendar.setTime(dateForCalculation);

                if ((cal.get(Calendar.DAY_OF_MONTH) >= 22 && cal.get(Calendar.MONTH) == 5 && latitude >= 48) ||
                        (cal.get(Calendar.MONTH) == 6 && latitude >= 48) ||
                        (cal.get(Calendar.DAY_OF_MONTH) <= 22 && cal.get(Calendar.MONTH) == 7 && latitude >= 48)) {
                    addingTimeArr[i] = addingTimeArr[i] + 2;
                }

                calculateCalendar.add(Calendar.MINUTE, addingTimeArr[i]);
                String newTime = df.format(calculateCalendar.getTime());

                HashMap<String, String> hashMap = new HashMap<>();
                hashMap.put("name", prayerNames.get(i) + "");
                hashMap.put("time", newTime);
                namaztimeList.add(hashMap);


                int hours = new Time(System.currentTimeMillis()).getHours();
                int minute = new Time(System.currentTimeMillis()).getMinutes();
                String[] separated = newTime.split(":");
                int h = Integer.valueOf(separated[0]);
                int M = Integer.valueOf(separated[1]);
                Log.d("HHH",dd+"**"+ y+"**"+m+"**"+d+"**"+latitude+"**"+longitude+"**"+timezone);
                if (k == 100 && dd == d) {
                    if (h == hours && M > minute) {
                        Log.d("HHH1", h + "**" + hours);
                        k = i;
                    } else if (h > hours && h != 0) {
                        k = i;
                        Log.d("HHH2", h + "**" + hours);
                    }
                }

            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        adapter=new ListAdapter(Schedule.this, namaztimeList, k);
        listView.setAdapter(adapter);

    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
        public class ListAdapter extends BaseAdapter {

            ArrayList<HashMap<String, String>> commentsList;
            Activity activity;
            int k;
            public ListAdapter(Activity activity, ArrayList<HashMap<String, String>> commentsList, int k) {
                this.activity = activity;
                this.commentsList = commentsList;
                this.k = k;
            }


            @Override
            public int getCount() {
                return commentsList.size();
            }

            @Override
            public Object getItem(int location) {
                return null;
            }

            @Override
            public long getItemId(int position) {
                return position;
            }


            class ViewHolder {
                TextView name;
                TextView time;
                TextView current;

            }

            @Override
            public View getView(final int position, View convertView, ViewGroup parent) {
                final ViewHolder viewHolder;
                if (convertView == null) {
                    LayoutInflater inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    convertView = inflater.inflate(R.layout.item_namaz_time, parent, false);
                    viewHolder = new ViewHolder();

                    viewHolder.current = (TextView) convertView.findViewById(R.id.current);
                    viewHolder.name = (TextView) convertView.findViewById(R.id.name);
                    viewHolder.time = (TextView) convertView.findViewById(R.id.time);

                    convertView.setTag(viewHolder);

                } else {
                    viewHolder = (ViewHolder) convertView.getTag();
                }

                if(position==k){
                    viewHolder.current.setVisibility(View.VISIBLE);
                }else {
                    viewHolder.current.setVisibility(View.GONE);
                }
                viewHolder.name.setText(commentsList.get(position).get("name"));
                viewHolder.time.setText(commentsList.get(position).get("time"));




                convertView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                    }
                });

                return convertView;
            }
        }

}
