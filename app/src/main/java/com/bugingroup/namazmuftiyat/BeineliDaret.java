package com.bugingroup.namazmuftiyat;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.android.volley.toolbox.StringRequest;
import com.bugingroup.namazmuftiyat.bookTaharatTerminDestroy.ActivityRakatTbs;
import com.bugingroup.namazmuftiyat.bookTaharatTerminDestroy.ExpandableListAdapter;
import com.bugingroup.namazmuftiyat.utils.NonScrollExpandableListView;
import com.bugingroup.namazmuftiyat.utils.Requests;
import com.bugingroup.namazmuftiyat.utils.URL_Constant;
import com.bugingroup.namazmuftiyat.utils.volley.AppController;
import com.bugingroup.namazmuftiyat.utils.volley.ImageVolleyRequest;
import com.google.android.youtube.player.YouTubePlayer;
import com.thefinestartist.ytpa.YouTubePlayerActivity;
import com.thefinestartist.ytpa.enums.Orientation;
import com.thefinestartist.ytpa.enums.Quality;
import com.thefinestartist.ytpa.utils.YouTubeThumbnail;
import com.thefinestartist.ytpa.utils.YouTubeUrlParser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;

import static com.bugingroup.namazmuftiyat.MainActivity.lang;
import static com.bugingroup.namazmuftiyat.utils.URL_Constant.CSS_style;
import static com.bugingroup.namazmuftiyat.utils.URL_Constant.IMAGE;

/**
 * Created by nurbaqyt on 26.07.17.
 */

public class BeineliDaret extends AppCompatActivity {

    @BindView(R.id.play_btn)
    ImageButton play_btn;

    @BindView(R.id.thumb)
    NetworkImageView thumb;

    Requests r = new Requests();

 
    @BindString(R.string.video)
    String video;

    @BindString(R.string.download)
    String download;

    @BindString(R.string.volley_error)
    String volley_error;


    HashMap<String, List<String>> expandableListDetail = new HashMap<String, List<String>>();
    List<String> expandableListTitle =  new ArrayList<String>();;

    @BindView(R.id.rakat_list)
    NonScrollExpandableListView listView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_beineli_daret);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getIntent().getStringExtra("title"));



        getPostsWithId(getIntent().getStringExtra("id"));

        ImageLoader imageLoader = ImageVolleyRequest.getInstance(this)
                .getImageLoader();


            final String vidoeId = YouTubeUrlParser.getVideoId(video);
            thumb.setImageUrl(YouTubeThumbnail.getUrlFromVideoId(vidoeId, Quality.MEDIUM), imageLoader);
            play_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(BeineliDaret.this, YouTubePlayerActivity.class);
                    intent.putExtra(YouTubePlayerActivity.EXTRA_VIDEO_ID, vidoeId);
                    intent.putExtra(YouTubePlayerActivity.EXTRA_PLAYER_STYLE, YouTubePlayer.PlayerStyle.DEFAULT);
                    intent.putExtra(YouTubePlayerActivity.EXTRA_ORIENTATION, Orientation.AUTO);
                    intent.putExtra(YouTubePlayerActivity.EXTRA_SHOW_AUDIO_UI, true);
                    intent.putExtra(YouTubePlayerActivity.EXTRA_HANDLE_ERROR, true);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                }
            });



    }


    private void getPostsWithId(String id) {
        final ProgressDialog p = new ProgressDialog(BeineliDaret.this);
        p.setTitle(download);
        p.show();
        StringRequest jsonReq = new StringRequest (Request.Method.GET,
                URL_Constant.DOMAIN+lang+"/posts/"+id,new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if(response!=null){
                    try {
                        JSONObject jsonObject =  new JSONObject(response);
                        if(jsonObject.getBoolean("success")) {
                            JSONArray serviceArray = jsonObject.getJSONArray("results");

                            try {
                                for (int i = 0; i < serviceArray.length(); i++) {
                                    JSONObject service = serviceArray.getJSONObject(i);

                                        List<String> child = new ArrayList<String>();
                                    String childText = service.getString("content");
                                    childText = childText.replaceAll( "src=\"", "src=\""+IMAGE );
                                    String html = CSS_style+childText;
                                    child.add(html);

                                        Log.d("123123123",service.getString("title"));
                                        expandableListTitle.add(service.getString("title"));
                                        expandableListDetail.put(service.getString("title"), child);

                                }

                                BeineliDaretListAdapter listAdapter = new BeineliDaretListAdapter(BeineliDaret.this, expandableListTitle, expandableListDetail);

                                listView.setAdapter(listAdapter);
                                Log.d("AAAAA2","title");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                p.dismiss();
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                Toast.makeText(BeineliDaret.this,volley_error,Toast.LENGTH_SHORT).show();
                p.dismiss();
                VolleyLog.d("Error: " + error.getMessage());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json");
                return headers;
            }
        };
        AppController.getInstance().addToRequestQueue(jsonReq);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
