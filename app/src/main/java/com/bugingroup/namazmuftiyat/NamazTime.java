package com.bugingroup.namazmuftiyat;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.bugingroup.namazmuftiyat.utils.CalculatePrayTime;
import com.bugingroup.namazmuftiyat.utils.NonScrollListView;

import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

import butterknife.BindArray;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.bugingroup.namazmuftiyat.MainActivity.ASRM;
import static com.bugingroup.namazmuftiyat.MainActivity.HIGHET;
import static com.bugingroup.namazmuftiyat.MainActivity.M1;
import static com.bugingroup.namazmuftiyat.MainActivity.M2;
import static com.bugingroup.namazmuftiyat.MainActivity.M3;
import static com.bugingroup.namazmuftiyat.MainActivity.M4;
import static com.bugingroup.namazmuftiyat.MainActivity.M5;
import static com.bugingroup.namazmuftiyat.MainActivity.M6;
import static com.bugingroup.namazmuftiyat.MainActivity.METHOD;
import static com.bugingroup.namazmuftiyat.MainActivity.addingTimeArr;
import static com.bugingroup.namazmuftiyat.MainActivity.currentCityName;
import static com.bugingroup.namazmuftiyat.MainActivity.latitude;
import static com.bugingroup.namazmuftiyat.MainActivity.longitude;
import static com.bugingroup.namazmuftiyat.MainActivity.timezone;
import static com.bugingroup.namazmuftiyat.SplashScreen.PREFS_NAME;

/**
 * Created by Nurik on 13.04.2017.
 */

public class NamazTime extends AppCompatActivity {

    @BindView(R.id.city_title)
    TextView city_title;

    @BindView(R.id.date)
    TextView date_tv;

    @BindView(R.id.namaz_title)
    TextView namaz_title;

    @BindView(R.id.minus_time)
    TextView minus_time;

    @BindView(R.id.digitalclock)
    TextView digitalclock;


    @BindArray(R.array.namaz_name)
            String []namaz_name;

    ArrayList<HashMap<String,String>> namaztimeList ;

    @BindView(R.id.namaz_time)
    NonScrollListView listView;
    int k=100;
    int YEAR,MONTH,DAY,dd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_namaz_time);
        ButterKnife.bind(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        k=100;
        SharedPreferences settings = getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        currentCityName = settings.getString("city", "Алматы");
        city_title.setText(currentCityName);
        Calendar cal = Calendar.getInstance();
        Date now = new Date();
        cal.setTime(now);

        YEAR = cal.get(Calendar.YEAR);
        MONTH = cal.get(Calendar.MONTH)+1;
        DAY = cal.get(Calendar.DATE);
        dd = cal.get(Calendar.DATE);
        Log.d("MONTH",YEAR+"*"+MONTH+"*"+DAY);
        getYearMonthDay(YEAR,MONTH,DAY);
    }

    @OnClick(R.id.date)
    void OnDateClick(){
        Intent in = new Intent(getApplicationContext(),Schedule.class);
        startActivity(in);
    }

    @OnClick(R.id.next)
    void NextClick(){
        DAY++;
        if(dd!=DAY){
            k=100;
        }
        getYearMonthDay(YEAR,MONTH,DAY);
    }
    @OnClick(R.id.preview)
    void previewClick(){
        DAY--;
        if(dd!=DAY){
            k=100;
        }
        getYearMonthDay(YEAR,MONTH,DAY);
    }

    public void getYearMonthDay(int y, int m, int d){

        try {
            String dateStr = d+"/"+m+"/"+y;

            SimpleDateFormat curFormater = new SimpleDateFormat("dd/MM/yyyy");
            Date dateObj = null;

            dateObj = curFormater.parse(dateStr);

            SimpleDateFormat postFormater = new SimpleDateFormat("dd MMMM, yyyy", Locale.getDefault());
            String newDateStr = postFormater.format(dateObj);
            date_tv.setText(newDateStr+" г.");
//            getTime(y,m,d);
            getTime(y,m,d);
            Log.d("HHH",y+"-"+m+"-"+d);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    };


    // onClick method for the button
    public void getTime(int y, int m ,int d) {
        namaztimeList = new ArrayList<>();
        // Retrive lat, lng using location API

        CalculatePrayTime prayers = new CalculatePrayTime();
        prayers.setTimeFormat(prayers.Time24);
        prayers.setCalcMethod(METHOD);
        prayers.setAsrJuristic(ASRM);
        prayers.setAdjustHighLats(HIGHET);
        ArrayList<String> timeNames = new ArrayList<String>();
        timeNames.add(namaz_name[0]);
        timeNames.add(namaz_name[1]);
        timeNames.add(namaz_name[2]);
        timeNames.add(namaz_name[3]);
        timeNames.add(namaz_name[4]);
        timeNames.add(namaz_name[4]);
        timeNames.add(namaz_name[5]);
        prayers.setTimeNames(timeNames);

        int[] offsets  = new int[]{M1, M2, M3, M4,0, M5, M6};

        if(METHOD==8){
            if (latitude < 48) {
                offsets  = new int[]{M1, M2-3, M3+3, M4+3,0, M5+3, M6};
            } else {
                offsets  = new int[]{M1, M2-5, M3+5, M4+5,0, M5+5, M6};
            }
        }
        prayers.tune(offsets);

        SimpleDateFormat df = new SimpleDateFormat("HH:mm");
        Calendar calculateCalendar = Calendar.getInstance();

        Calendar cal = Calendar.getInstance();
        Date now = new Date();
        cal.setTime(now);


        Log.d("HHH", y+"**"+m+"**"+d+"**"+latitude+"**"+longitude+"**"+timezone);
        ArrayList prayerTimes = prayers.getDatePrayerTimes(y,m,d, latitude, longitude, timezone);
        ArrayList prayerNames = prayers.getTimeNames();

        int j=0;
        for (int i = 0; i < prayerTimes.size(); i++) {
            if (i != 4) {
                Log.d("SSSSttttt", prayerTimes.size() + "***" + i + "**" + prayerTimes.get(i)+ "**" + prayerTimes.size());
                try {
                    Date dateForCalculation = df.parse(prayerTimes.get(i) + "");
                    calculateCalendar.setTime(dateForCalculation);

//                if((cal.get(Calendar.DAY_OF_MONTH) >= 22 && cal.get(Calendar.MONTH) == 5 && latitude >=48) ||
//                        (cal.get(Calendar.MONTH) ==6 && latitude >=48) ||
//                        (cal.get(Calendar.DAY_OF_MONTH) <= 22 && cal.get(Calendar.MONTH) == 7 && latitude >=48)){
//                    addingTimeArr[i] = addingTimeArr[i]+2;
//                }

                    calculateCalendar.add(Calendar.MINUTE, addingTimeArr[i]);
                    String newTime = df.format(calculateCalendar.getTime());

                    HashMap<String, String> hashMap = new HashMap<>();
                    hashMap.put("name", prayerNames.get(i) + "");
                    hashMap.put("time", newTime);
                    namaztimeList.add(hashMap);


                    int hours = new Time(System.currentTimeMillis()).getHours();
                    int minute = new Time(System.currentTimeMillis()).getMinutes();
                    String[] separated = newTime.split(":");
                    int h = Integer.valueOf(separated[0]);
                    int M = Integer.valueOf(separated[1]);

                    Log.d("HH", hours + "_" + h + "M:" + minute + "_" + M);
                    if (k == 100 && dd == d) {
                        if (h >= hours && M > minute) {
                            k = j;
                        } else if (h > hours && h != 0) {
                            k = j;
                        }
                    }

                } catch (ParseException e) {
                    e.printStackTrace();
                }
                j++;
            }


            listView.setAdapter(new ListAdapter(NamazTime.this, namaztimeList, k));
            if (k != 100) {
                namaz_title.setText(namaztimeList.get(k).get("name"));
                digitalclock.setText(namaztimeList.get(k).get("time"));
                String[] separated = namaztimeList.get(k).get("time").split(":");
                int h = Integer.valueOf(separated[0]);
                int M = Integer.valueOf(separated[1]);
                int S = h * 3600 + M * 60;
                int second = cal.get(Calendar.HOUR) * 3600 + cal.get(Calendar.MINUTE) * 60 + cal.get(Calendar.SECOND);
                Log.d("AAAAA", S + "*" + second + "==" + (S - second));
                if (S - second > 43200)
                    reverseTimer(S - second - 43200, minus_time);
                else
                    reverseTimer(S - second, minus_time);
            }
        }
    }


    @OnClick(R.id.back)
    void onBackClick(){
        finish();
    }

    @OnClick(R.id.setting)
    void settingClick(){
        Intent in = new Intent(getApplicationContext(),ActivitySetting.class);
        startActivity(in);
    }


    public class ListAdapter extends BaseAdapter {

        ArrayList<HashMap<String, String>> commentsList;
        Activity activity;
        int k;
        public ListAdapter(Activity activity, ArrayList<HashMap<String, String>> commentsList, int k) {
            this.activity = activity;
            this.commentsList = commentsList;
            this.k = k;
        }


        @Override
        public int getCount() {
            return commentsList.size();
        }

        @Override
        public Object getItem(int location) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }


        class ViewHolder {
            TextView name;
            TextView time;
            TextView current;

        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            final ViewHolder viewHolder;
            if (convertView == null) {
                LayoutInflater inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.item_namaz_time, parent, false);
                viewHolder = new ViewHolder();

                viewHolder.current = (TextView) convertView.findViewById(R.id.current);
                viewHolder.name = (TextView) convertView.findViewById(R.id.name);
                viewHolder.time = (TextView) convertView.findViewById(R.id.time);

                convertView.setTag(viewHolder);

            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }

            if(position==k){

                viewHolder.current.setVisibility(View.VISIBLE);
            }else {
                viewHolder.current.setVisibility(View.GONE);
            }
            viewHolder.name.setText(commentsList.get(position).get("name"));
            viewHolder.time.setText(commentsList.get(position).get("time"));




            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });

            return convertView;
        }
    }

    public void reverseTimer(int Seconds,final TextView tv){

        new CountDownTimer(Seconds* 1000+1000, 1000) {

            public void onTick(long millisUntilFinished) {
                int seconds = (int) (millisUntilFinished / 1000);
                int hour=0;
                int minutes= 0;
                if(seconds>3600) {
                    hour = seconds / 3600;
                    minutes = (seconds-hour*3600) / 60;
                }else{
                 minutes = seconds / 60;
                }
                seconds = seconds % 60;

                tv.setText("(-" + String.format("%02d", hour)
                        + ":" + String.format("%02d", minutes)
                        + ":" + String.format("%02d", seconds)+")");
            }

            public void onFinish() {

            }
        }.start();
    }


}
