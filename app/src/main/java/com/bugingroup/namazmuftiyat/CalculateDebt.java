package com.bugingroup.namazmuftiyat;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.bugingroup.namazmuftiyat.utils.DatabaseHelper;
import com.google.android.youtube.player.YouTubePlayer;
import com.thefinestartist.ytpa.YouTubePlayerActivity;
import com.thefinestartist.ytpa.enums.Orientation;
import com.thefinestartist.ytpa.utils.YouTubeUrlParser;

import org.w3c.dom.CDATASection;

import java.util.Date;
import java.util.concurrent.TimeUnit;

import butterknife.BindArray;
import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Nurik on 10.04.2017.
 */

public class CalculateDebt extends AppCompatActivity {

    @BindView(R.id.birthday)
    EditText birthday;

    @BindView(R.id.start_namaz)
    EditText start_namaz;

    @BindView(R.id.male)
    Spinner male;

    @BindView(R.id.year)
    EditText year_et;

    @BindArray(R.array.array_male)
    String[] array_male;

    int DAY_RESULT=0,Year=0,Month=0,Day=0;

    @BindView(R.id.debt_year)
    TextView debt_year;

    @BindView(R.id.debt_month)
    TextView debt_month;

    @BindView(R.id.debt_day)
    TextView debt_day;

    TextView DiaMsg,DiaTitle;
    Dialog dialog;
    Button btnOk;

    DatabaseHelper db;

    @BindString(R.string.cal_debt) String cal_debt;
    @BindString(R.string.year) String year_string;
    @BindString(R.string.month) String month_string;
    @BindString(R.string.day) String day_string;

    @BindString(R.string.birthday) String birthday_string;
    @BindString(R.string.male) String male_string;
    @BindString(R.string.year_start_namaz) String year_start_namaz;
    @BindString(R.string.start_namaz) String start_namaz_string;

    @BindString(R.string.title_birthday) String title_birthday_string;
    @BindString(R.string.title_male) String title_male_string;
    @BindString(R.string.title_year_start_namaz) String title_year_start_namaz;
    @BindString(R.string.title_start_namaz) String title_start_namaz_string;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_debt_calc);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(cal_debt);

        male.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, array_male));
        male.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(Year!=0) {
                    if (position == 1) {
                        int YEAR_M = Year+12;
                        year_et.setText(YEAR_M+"");
                    } else  if (position == 2){
                        int YEAR_M = Year+9;
                        year_et.setText(YEAR_M+"");
                    }
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        dialog = new Dialog(CalculateDebt.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.info_dialog_debt);
        DiaMsg = (TextView) dialog.findViewById(R.id.DiaMsg);
        DiaTitle = (TextView) dialog.findViewById(R.id.DiaTitle);
        btnOk = (Button) dialog.findViewById(R.id.btnOk);
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

    }

    @OnClick(R.id.birthday)
    void BirthdayClick(){
        DatePickerDialog datePickerDialog = new DatePickerDialog(CalculateDebt.this,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        Year=year;
                        Month=monthOfYear + 1;
                        Day=dayOfMonth;
                        birthday.setText(dayOfMonth + "." + (monthOfYear + 1) + "." + year);
                    }
                }, 1980, 0, 1);
        datePickerDialog.show();
    }

    @OnClick(R.id.start_namaz)
    void start_namazClick(){
        DatePickerDialog datePickerDialog = new DatePickerDialog(CalculateDebt.this,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        start_namaz.setText(dayOfMonth + "." + (monthOfYear + 1) + "." + year);

                        Date d1 = new Date(Integer.valueOf(year_et.getText().toString()),Month,Day);
                        Date d2 = new Date(year,monthOfYear+1,dayOfMonth);
                        long seconds = (d2.getTime()-d1.getTime())/1000;
                        int day = (int) TimeUnit.SECONDS.toDays(seconds);
                        System.out.println("Duration2: " +day/365);
                        debt_year.setText((int)day/365+" "+year_string);
                        debt_month.setText((int)day/30+" "+month_string);
                        debt_day.setText(day+" "+day_string);
                        DAY_RESULT = day;
                    }
                }, Integer.valueOf(year_et.getText().toString()), Month-1, Day);
        datePickerDialog.show();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @OnClick(R.id.to_result)
    void to_resultClick(){
       startActivity(new Intent(CalculateDebt.this,ResultDebt.class).putExtra("debt",false).putExtra("day",DAY_RESULT));
    }


    @OnClick(R.id.btn_brithday)
    void btn_brithday(){
        DiaTitle.setText(title_birthday_string);
        DiaMsg.setText(birthday_string);
        dialog.show();
    }



    @OnClick(R.id.btn_male)
    void btn_male(){
        DiaTitle.setText(title_male_string);
        DiaMsg.setText(male_string);
        dialog.show();
    }



    @OnClick(R.id.btn_year)
    void btn_year(){
        DiaTitle.setText(title_year_start_namaz);
        DiaMsg.setText(year_start_namaz);
        dialog.show();
    }



    @OnClick(R.id.btn_start_namaz)
    void btn_start_namaz(){
        DiaTitle.setText(title_start_namaz_string);
        DiaMsg.setText(start_namaz_string);
        dialog.show();
    }


    @OnClick(R.id.guide)
    void GuideClick(){
        final String vidoeId = YouTubeUrlParser.getVideoId("https://www.youtube.com/watch?v=1kiTN_jTkWo&feature=youtu.be");
        Intent intent = new Intent(CalculateDebt.this, YouTubePlayerActivity.class);
        intent.putExtra(YouTubePlayerActivity.EXTRA_VIDEO_ID, vidoeId);
        intent.putExtra(YouTubePlayerActivity.EXTRA_PLAYER_STYLE, YouTubePlayer.PlayerStyle.DEFAULT);
        intent.putExtra(YouTubePlayerActivity.EXTRA_ORIENTATION, Orientation.AUTO);
        intent.putExtra(YouTubePlayerActivity.EXTRA_SHOW_AUDIO_UI, true);
        intent.putExtra(YouTubePlayerActivity.EXTRA_HANDLE_ERROR, true);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);

    }
}