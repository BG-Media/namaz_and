package com.bugingroup.namazmuftiyat;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.bugingroup.namazmuftiyat.utils.DatabaseHelper;
import com.bugingroup.namazmuftiyat.utils.Zakat;

import java.util.List;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Nurik on 17.04.2017.
 */

public class PayHelp extends AppCompatActivity {

    @BindView(R.id.price)
    EditText price;
    @BindView(R.id.input_p)
    TextView input_p;

//    DatabaseHelper db;
//
//    @BindView(R.id.listView)
//    ListView listView;

    @BindString(R.string.help)
    String help;
    @BindString(R.string.valid_empty)
    String valid_empty;
    @BindString(R.string.input_price_donation)
    String input_price_donation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help_pay);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(help);
//        db = new DatabaseHelper(getApplicationContext());
//
//        listView = (ListView) findViewById(R.id.listView);


        input_p.setText(input_price_donation);
    }

    @OnClick(R.id.pay)
    void Payclick(){
        if(price.getText().toString().length()>0)
        startActivity(new Intent(PayHelp.this,ActivityPay.class).putExtra("price",price.getText().toString()));
        else Toast.makeText(this, valid_empty, Toast.LENGTH_SHORT).show();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }




//    public class ListAdapter extends BaseAdapter {
//
//        Activity activity;
//        List<Zakat> allToDos;
//
//        public ListAdapter(Activity activity,  List<Zakat> allToDos) {
//            this.activity = activity;
//            this.allToDos = allToDos;
//        }
//
//
//        @Override
//        public int getCount() {
//            return allToDos.size();
//        }
//
//        @Override
//        public Object getItem(int location) {
//            return null;
//        }
//
//        @Override
//        public long getItemId(int position) {
//            return position;
//        }
//
//
//        class ViewHolder {
//            TextView price,time,date;
//
//        }
//
//        @Override
//        public View getView(final int position, View convertView, ViewGroup parent) {
//            final ViewHolder viewHolder;
//            if (convertView == null) {
//                LayoutInflater inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//                convertView = inflater.inflate(R.layout.item_pay, parent, false);
//                viewHolder = new ViewHolder();
//
//                viewHolder.price = (TextView) convertView.findViewById(R.id.price);
//                viewHolder.time = (TextView) convertView.findViewById(R.id.time);
//                viewHolder.date = (TextView) convertView.findViewById(R.id.date);
//
//
//                convertView.setTag(viewHolder);
//
//            } else {
//                viewHolder = (ViewHolder) convertView.getTag();
//            }
//
//
//            final Zakat zakat = allToDos.get(position);
//
//            viewHolder.price.setText(zakat.getPrice());
//            viewHolder.time .setText(zakat.getCreate_time());
//            viewHolder.date .setText(zakat.getCreate_date());
//
//
//            return convertView;
//        }
//    }
//
//
//    @Override
//    protected void onStart() {
//        super.onStart();
//        List<Zakat> allToDos = db.getAllPay();
//
//        listView = (ListView) findViewById(R.id.listView);
//        listView.setAdapter(new ListAdapter(PayHelp.this,allToDos));
//    }
}
