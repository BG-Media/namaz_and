package com.bugingroup.namazmuftiyat.kibla;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ImageView;

import com.bugingroup.namazmuftiyat.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import static com.bugingroup.namazmuftiyat.MainActivity.latitude;
import static com.bugingroup.namazmuftiyat.MainActivity.longitude;


public class KiblaActivity extends AppCompatActivity  implements OnMapReadyCallback {

    private static final String TAG = "CompassActivity";

    private Kibla kibla;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kibla);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        kibla = new Kibla(this);
        kibla.arrowView = (ImageView) findViewById(R.id.main_image_hands);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG, "start compass");
        kibla.start();
    }

    @Override
    protected void onPause() {
        super.onPause();
        kibla.stop();
    }

    @Override
    protected void onResume() {
        super.onResume();
        kibla.start();
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG, "stop compass");
        kibla.stop();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {


        LatLng yourlocation = new LatLng(latitude, longitude);
        LatLng mekkah = new LatLng(21.4224604,39.8261414);
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(yourlocation,10));
        PolylineOptions options = new PolylineOptions().width(5).color(Color.GREEN).geodesic(true);
        options.add(yourlocation);
        options.add(mekkah);
        googleMap.addPolyline(options);

        BitmapDescriptor icon = BitmapDescriptorFactory.fromResource(R.drawable.mekkah);

        MarkerOptions markerOptions = new MarkerOptions().position(mekkah)
                .title("الكعبة")
                .snippet("Kaaba")
                .icon(icon);
        googleMap.addMarker(markerOptions);

    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_compass, menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//
//        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }
//
//        return super.onOptionsItemSelected(item);
//    }
}