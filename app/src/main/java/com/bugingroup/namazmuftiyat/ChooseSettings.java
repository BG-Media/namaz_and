package com.bugingroup.namazmuftiyat;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.bugingroup.namazmuftiyat.utils.Requests;

import java.util.ArrayList;
import java.util.HashMap;

import static com.bugingroup.namazmuftiyat.MainActivity.addingTimeArr;
import static com.bugingroup.namazmuftiyat.TimeSettings.whatSettings;

public class ChooseSettings extends AppCompatActivity {

    ListView chooseListView;
    SimpleAdapter chooseAdapter;
    ArrayList<HashMap<String,String>> arrayList = new ArrayList<>();
    Requests r = new Requests();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_setttings);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        chooseListView = (ListView) findViewById(R.id.choose_settings_list);

        if(whatSettings.equals("city")){
            chooseAdapter = new SimpleAdapter(this,arrayList,R.layout.choose_item,
                    new String[]{"id","name"},new int[]{R.id.choose_item_id,R.id.choose_item_title});
            chooseListView.setAdapter(chooseAdapter);
            r.getCities(this,arrayList,chooseAdapter);
            chooseListView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    TextView textView = (TextView) view.findViewById(R.id.choose_item_title);
                    Intent in = new Intent(getApplicationContext(),TimeSettings.class);
                    in.putExtra("city",textView.getText().toString());
                    startActivity(in);
                }
            });

        }else if(whatSettings.equals("correction")){

            chooseAdapter = new SimpleAdapter(this,arrayList,R.layout.choose_item,
                    new String[]{"minutes","value"},new int[]{R.id.choose_item_title,R.id.choose_item_id});
            chooseListView.setAdapter(chooseAdapter);

            for(int i = 30;i>=1;i--){
                HashMap<String,String> hashMap = new HashMap<>();
                hashMap.put("minutes",-i+" минут");
                hashMap.put("value",-i+"");
                arrayList.add(hashMap);
            }

            for(int i = 0;i<=30;i++){
                HashMap<String,String> hashMap = new HashMap<>();
                hashMap.put("minutes",i+" минут");
                hashMap.put("value",i+"");
                arrayList.add(hashMap);
            }
            chooseAdapter.notifyDataSetChanged();

            chooseListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    TextView textView = (TextView) view.findViewById(R.id.choose_item_id);
                    Intent in = new Intent(getApplicationContext(),TimeSettings.class);
                    in.putExtra("value",Integer.parseInt(textView.getText().toString()));
              //      addingTimeArr[whatAzan] = Integer.parseInt(textView.getText().toString());
                    Toast.makeText(getApplicationContext(),"Время скорректировано",Toast.LENGTH_SHORT).show();
                    startActivity(in);
                }
            });
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
