package com.bugingroup.namazmuftiyat;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.bugingroup.namazmuftiyat.utils.DatabaseHelper;
import com.bugingroup.namazmuftiyat.utils.Zakat;
import com.melnykov.fab.FloatingActionButton;

import java.util.List;

import butterknife.BindString;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Nurik on 26.04.2017.
 */

public class ZakatList extends AppCompatActivity {


    ListView listview;

    DatabaseHelper db;
    @BindString(R.string.zakat)
    String Zakat;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.zakatlist);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(Zakat);
        db = new DatabaseHelper(getApplicationContext());
        listview = (ListView) findViewById(R.id.listView);

        db.closeDB();

    }

    @OnClick(R.id.calculate_zakat)
    void calculate_zakat(){
        startActivity(new Intent(ZakatList.this,CalculateZakat.class).putExtra("id",0));
    }

    public class ListAdapter extends BaseAdapter {

        Activity activity;
        List<Zakat> allToDos;

        public ListAdapter(Activity activity,  List<Zakat> allToDos) {
            this.activity = activity;
            this.allToDos = allToDos;
        }


        @Override
        public int getCount() {
            return allToDos.size();
        }

        @Override
        public Object getItem(int location) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }


        class ViewHolder {
            TextView text;
            ImageButton delete;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            final ViewHolder viewHolder;
            if (convertView == null) {
                LayoutInflater inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.item_zakat, parent, false);
                viewHolder = new ViewHolder();

                viewHolder.text = (TextView) convertView.findViewById(R.id.text);
                viewHolder.delete = (ImageButton) convertView.findViewById(R.id.delete);

                convertView.setTag(viewHolder);

            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }


            final Zakat zakat = allToDos.get(position);

            viewHolder.text.setText(Zakat+" № "+zakat.getId());
            viewHolder.delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    db.deleteToDoTag(zakat.getId());
                    allToDos.remove(position);
                    notifyDataSetChanged();
                }
            });
            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(ZakatList.this,CalculateZakat.class).putExtra("id",zakat.getId()));
                }
            });

            return convertView;
        }
    }


    @Override
    protected void onStart() {
        super.onStart();
        List<Zakat> allToDos = db.getAllzakat();
        listview.setAdapter(new ListAdapter(ZakatList.this,allToDos));
        db.closeDB();
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; go home
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
